open STools
open BTools

(* full proof *)
type ('a, 'd) proof =
  | End
  | Seq of 'a list * ('a, 'd) proof
  | Sum of 'd * ('a, 'd) proof list

let rec proof_normalize = function
  | End -> End
  | Seq ([], p) -> proof_normalize p
  | Seq (al, p) ->
  ( match proof_normalize p with
    | Seq (al', p') -> Seq(al@al', p')
    | p -> Seq(al, p)
  )
  | Sum (d, pl) ->
    Sum(d, List.map proof_normalize pl)

(* partial proof (cost annoted) *)
type ('a, 'd, 's, 'c) pproof =
  | PEnd
  | PNxt of 's
  | PSeq of 'a list * 'c * 's * ('a, 'd, 's, 'c) pproof
  | PSum of 'd * 'c * ('s * ('a, 'd, 's, 'c) pproof) list
  | PMin of 's * ('a, 'd, 's, 'c) pproof list

let rec pproof_normalize ( + ) = function
  | PEnd -> PEnd
  | PNxt s -> PNxt s
  | PSeq (al0, c0, s0, pp) ->
  (  match pproof_normalize ( + ) pp with
    | PSeq (al1, c1, s1, pp) ->
      PSeq (al0@al1, c0 + c1, s1, pp)
    | pp' -> PSeq(al0, c0, s0, pp')
  )
  | PSum (d, c, ppl) ->
    let ppl' = List.map
      (fun (s, pp) -> (s, pproof_normalize ( + ) pp))
      ppl
    in
    PSum(d, c, ppl')
  | PMin(s, ppl) ->
    match ppl with
    | [] -> assert false
    | [pp] -> pproof_normalize ( + ) pp
    | _ -> PMin (s, List.map (pproof_normalize ( + )) ppl)

type ('a, 'd) aproof =
  | AEnd
  | ANxt
  | ASeq of 'a list
  | ASum of 'd
  | AMin

let aproof_of_pproof = function
  | PEnd               -> AEnd
  | PNxt _             -> ANxt
  | PSeq (al, _, _, _) -> ASeq al
  | PSum (d , _, _)    -> ASum d
  | PMin (_ , _)       -> AMin

let rec proof_of_pproof :
    ('a, 'd, 's, 'c) pproof -> ('a, 'd) proof =
  function
  | PEnd -> End
  | PNxt s -> assert false
  | PSeq (al, _, _, pp) ->
    Seq(al, proof_of_pproof pp)
  | PSum (d, _, ppl) ->
    Sum(d, List.map (fun (_, pp) -> proof_of_pproof pp) ppl)
  | PMin(_, _) -> assert false

(* [LATER] implement PMin *)
let rec cost_of_pproof
  (zero : unit -> 'c)
  ((+):'c -> 'c -> 'c)
  (cost_of_pnxt:'s -> 'c) :
    ('a, 'd, 's, 'c) pproof -> 'c =
  function
  | PEnd -> zero()
  | PNxt s -> cost_of_pnxt s
  | PSeq (al, c, _, pp) -> c + (cost_of_pproof zero (+) cost_of_pnxt pp)
  | PSum (d, c, ppl) ->
    List.fold_left (fun c (_, pp) ->
      c + (cost_of_pproof zero (+) cost_of_pnxt pp)) c ppl
  | PMin(_, ppl) -> assert false

module ToS =
struct
  include ToS

  let rec proof (sa:'a t) (sd:'d t) : ('a, 'd) proof t = function
    | End -> "End"
    | Seq (al, p) -> ("Seq "^(((list sa) * (proof sa sd)) (al, p)))
    | Sum (d, pl) -> ("Sum "^((sd * (list (proof sa sd))) (d, pl)))

  let rec pproof (sa:'a t) (sd:'d t) (ss:'s t) (sc:'c t) : ('a, 'd, 's, 'c) pproof t = function
    | PEnd -> "PEnd"
    | PNxt s -> "PNxt "^(ss s)
    | PSeq (al, c, s, pp) ->
      "PSeq ("^(list sa al)^", "^(sc c)^", "^(ss s)^", "^(pproof sa sd ss sc pp)^")"
    | PSum (d, c, sppl) ->
      "PSum "^((trio sd sc (list(ss*(pproof sa sd ss sc)))) (d, c, sppl))
    | PMin (s, ppl) ->
      "PMin "^(pair ss (list(pproof sa sd ss sc)) (s, ppl))

  let aproof (sa:'a t) (sd:'d t) : ('a, 'd) aproof t = function
    | AEnd -> "AEnd"
    | ANxt -> "ANxt"
    | ASeq al -> "ASeq "^(list sa al)
    | ASum d  -> "ASum "^(sd d)
    | AMin -> "AMin"
end

module type MSig =
sig
(* A.1] type of elements *)
  type s (* state : the type of intermediate states *)
    (* [FIXME] standard operation on state elements must be consistent
        e.g. Stdlib.compare, Hashtbl.hash *)
  type a (* non branching actions : the type of actions *)
  type d (* branching actions, aka divider actions *)
  type c (* cost : the type of cost *)

(* A.2] type of proofs *)
  type p  = (a, d)        proof
  type pp = (a, d, s, c) pproof
  type ap = (a, d)       aproof

(* B] type of operators *)
  val lower : s -> c
  val upper : s -> p * c
  val next  : s -> pp
  (* do not use empty transition from Min to Min *)

  val cost_add : c -> c -> c
    (* [cost_add c1 c2 = c3] st. [c3] = [c1] + [c2] *)
  val cost_sub : c -> c -> c
    (* [cost_sub c1 c2 = c3] st. [c3] = [c1] - [c2] *)
  val cost_cmp : c -> c -> int
    (* comparison operator on [c] *)
  val cost_zero : unit -> c
    (* returns a neutral cost element *)

(* C] action application operators *)
  val do_a : s -> a -> c * s
  val do_d : s -> d -> c * (s list)

(* D] [DEBUG] printing facility *)
  val ss : s -> string (* state *)
  val sa : a -> string (* action *)
  val sd : d -> string (* divider *)
  val sc : c -> string (* cost *)

(* E] [ESTIMATION] estimate the number of combination left to deal with *)
  val estim : s -> BNat.nat
end

module type Sig =
sig
  module H : MSig

  type t

  val newman : unit -> t

  val solve : t -> H.s -> H.c * H.p
  (* val dump_stats : t -> Tree.stree *)
end

module Make(H0:MSig) : Sig
  with module H = H0
  =
struct
  module H = H0

  type ident = int

  type alive =
    | NotVisited
    | Visited
    | Solved

  type tnxt = {
    tnxt_proof : H.p;
  }

  type tseq = {
    tseq_next  : ident;
    tseq_actions : H.a list;
    tseq_actions_cost : H.c;
  }

  module CostCmp : Map.OrderedType
    with type t = H.c =
  struct
    type t = H.c
    let compare = H.cost_cmp
  end

  module CostPQMax = PriorityQueue.MakeMax(CostCmp)
  module CostPQMin = PriorityQueue.MakeMin(CostCmp)

  type tmin = {
    mutable tmin_upper  : ident;
      (* child-node which has the lowest upper bound *)
            tmin_queue  : ident CostPQMin.t;
              (* parameter : node(ident).lower *)
  }

  type tsum = {
            tsum_divider : H.d;
            tsum_ccost   : H.c; (* constant cost for this divider *)
            tsum_queue   : (ident * int) CostPQMax.t;
              (* parameter : node(ident).diff *)
    mutable tsum_solved  : (ident * int) list;
  }

  (* Q? : possible state collision with [TMin], [TSeq], [TSum]
      A! : First encountered node takes priority other subsequent ones
   *)

  type sstate =
    | TEnd
    | TNxt of tnxt
    | TSeq of tseq
    | TSum of tsum
    | TMin of tmin

  type snode = {
            ident : ident; (* unique identifier *)
            state : H.s;
    mutable alive : alive; (* Visited | NotVisited | Solved *)
    mutable lower : H.c;
    mutable upper : H.c;
    mutable diff  : H.c;
    mutable sstate: sstate;

    (* E] Estimation *)
    mutable reached : bool ref;
      (* This field is used when traversing the search-tree to
         compute reachable nodes (and terminal-nodes) *)
  }

  type t = {
            memory : (ident, snode) Hashtbl.t;
  (* The [t.unique] table ensures that each intermediate state
       has at most one identifier.
     NB : Because of evaluation and solving an identifier may
       may represent several states, which therefore have the
       exact same set of optimal solution
   *)
            unique : (H.s * H.ap, ident) Hashtbl.t;
    mutable index : int;
  (* Store a mapping from [TNxt] to [BNat.nat]
     each number is the current upper bound on the number of sub-cases to deal with (for each TNxt)
     whem summing them up we obtain an upper-bound on the number of remaining sub-cases.
     Such upper bound takes into account the memoization strategy
   *)
            estim : (ident, (bool ref) * BNat.nat) Hashtbl.t;
  (* [bool ref] is used to determine which TNxt node are reachable *)
  }

  let newman () = {
    memory = Hashtbl.create 10_000;
    unique = Hashtbl.create 10_000;
    index  = 0;
    estim  = Hashtbl.create 10_000;
  }

  module ToS =
  struct
    include ToS

    let proof : H.p t = proof H.sa H.sd
    let pproof : H.pp t = pproof H.sa H.sd H.ss H.sc
    let aproof : H.ap t = aproof H.sa H.sd

    let ident (ident:ident) : string = "<"^(int ident)^">"

    let alive : alive t = function
      | NotVisited -> "NotVisited"
      | Visited    -> "Visited"
      | Solved     -> "Solved"

    let tnxt (tnxt:tnxt) : string =
      "tnxt:{proof="^(proof tnxt.tnxt_proof)^"}"

    let tseq (tseq:tseq) : string =
      "tseq:{next="^(ident tseq.tseq_next)^"; actions="^(list H.sa tseq.tseq_actions)^"; actions_cost="^(H.sc tseq.tseq_actions_cost)^"}"

    let tmin (tmin:tmin) : string =
      "tmin:{upper="^(ident tmin.tmin_upper)^"; queue="^(ignore tmin.tmin_queue)^"}"

    let tsum (tsum:tsum) : string =
      "tsum:{divider="^(H.sd tsum.tsum_divider)^"; ccost="^(H.sc tsum.tsum_ccost)^"; queue="^(ignore tsum.tsum_queue)^"; solved="^(list(ident*int) tsum.tsum_solved)^"}"

    let sstate : sstate t = function
      | TEnd -> "TEnd"
      | TNxt ss -> "TNxt "^(tnxt ss)
      | TSeq ss -> "TSeq "^(tseq ss)
      | TSum ss -> "TSum "^(tsum ss)
      | TMin ss -> "TMin "^(tmin ss)

    let snode (snode:snode) : string =
      "{ident="^(ident snode.ident)^
      "; state="^(H.ss snode.state)^
      "; alive="^(alive snode.alive)^
      "; lower="^(H.sc snode.lower)^
      "; upper="^(H.sc snode.upper)^
      "; diff="^(H.sc snode.diff)^
      "; sstate="^(sstate snode.sstate)^
      "}"

    (* [LATER]
    type t = {
              memory : (ident, snode) Hashtbl.t;
              unique : (H.s * H.ap, ident) Hashtbl.t;
      mutable index : int;

    }

    let newman () = {
      memory = Hashtbl.create 10_000;
      unique = Hashtbl.create 10_000;
      index  = 0;
    }
  *)
  end

  let zero3 () =
    H.(cost_zero(), cost_zero(), cost_zero())

  let add3 (l0, d0, u0) (l1, d1, u1) =
    H.(cost_add l0 l1,
       cost_add d0 d1,
       cost_add u0 u1)

  let sub3 (l0, d0, u0) (l1, d1, u1) =
    H.(cost_sub l0 l1,
       cost_sub d0 d1,
       cost_sub u0 u1)

  let do_list_a (al:H.a list) (s:H.s) : H.c * H.s =
    List.fold_left (fun (c, s) a ->
      let c', s' = H.do_a s a in
      (H.cost_add c c', s')
    ) (H.cost_zero(), s) al

  let tmin_add (t:t) (snode:snode) (tmin:tmin) (snode':snode) : unit =
    assert(snode.sstate = TMin tmin);
    assert(H.cost_cmp snode'.lower snode'.upper <= 0);
    (* 1) try to improve the upper bound [snode.upper] *)
    let cmpU = H.cost_cmp snode'.upper snode.upper in
    (if (cmpU < 0) || (snode'.alive = Solved && cmpU <= 0)
      (* i.e. snode'.upper < snode.upper *)
    then (
      (* we store the improved upper bound *)
      tmin.tmin_upper <- snode'.ident;
      snode.upper <- snode'.upper;
      (* removes branches with lower bound higher than current upper bound *)
      CostPQMin.shrink tmin.tmin_queue snode'.upper;
    ));
    (* 2) try to discard [snode'] if [snode'.lower] > [snode.upper] *)
    let cmpL = H.cost_cmp snode'.lower snode.upper in
    if cmpL > 0 || (cmpU > 0 && cmpL >= 0)
      (* i.e.
        if snode'.upper > snode.upper && snode'.lower >= snode.upper then discard
        if                               snode'.lower >  snode.upper then discard
        else keep
       *)
    then ()
    else (
      CostPQMin.add tmin.tmin_queue snode'.lower snode'.ident
    )

  let rec tmin_find_best (t:t) (snode:snode) (tmin:tmin) : H.c * ident * snode =
    assert(snode.sstate = TMin tmin);
    let lower', ident' = CostPQMin.pull_first tmin.tmin_queue in
    let snode' = Hashtbl.find t.memory ident' in
    let cmp = H.cost_cmp lower' snode'.lower in
    assert(cmp <= 0); (* lower bound can only increase *)
    (* by the way we try to update the upper bound *)
    if cmp = 0
    then (lower', ident', snode')
    else (
      tmin_add t snode tmin snode';
      tmin_find_best t snode tmin
    )

  let rec best_solution (t:t) (ident:ident) : H.c * H.p =
    let snode = Hashtbl.find t.memory ident in
    (* print_newline(); *)
    (* print_string "[DBBC.best_solution] snode.ident="; print_int snode.ident; print_newline(); *)
    let c, p = match snode.sstate with
      | TEnd -> (H.cost_zero(), End)
      | TNxt tnxt -> (snode.upper, tnxt.tnxt_proof)
      | TSeq tseq -> (
        (* print_string "[DBBC.best_solution] sstate:TSeq"; print_newline(); *)
        let c, p = best_solution t tseq.tseq_next in
        let c' = H.cost_add tseq.tseq_actions_cost c in
        let p' = Seq (tseq.tseq_actions, p) in
        (c', p')
      )
      | TSum tsum -> (
        (* print_string "[DBBC.best_solution] sstate:TSum"; print_newline(); *)
        let n =
          CostPQMax.length tsum.tsum_queue
          + List.length tsum.tsum_solved
        in
        let opa = Array.make n None in
        let cost = ref tsum.tsum_ccost in
        let recfun (ident, key) : unit =
          let c, p = best_solution t ident in
          cost := H.cost_add !cost c;
          opa.(key) <- Some p
        in
        CostPQMax.iter (fun _ -> recfun) tsum.tsum_queue;
        List.iter recfun tsum.tsum_solved;
        let sons = MyArray.unop opa |> Array.to_list in
        (!cost, Sum (tsum.tsum_divider, sons))
      )
      | TMin tmin -> (
        (* print_string "[DBBC.best_solution] sstate:TMin"; print_newline(); *)
        let c, p = best_solution t tmin.tmin_upper in
        assert(H.cost_cmp c snode.upper <= 0);
        (c, p)
      )
    in
    (if H.cost_cmp c snode.upper > 0
    (* i.e. the 'best solution' has a higher cost than expected (inconsistent database) *)
    then (
      print_string "[DBBC.best_solution] upper bound inconsistency with [best_solution]"; print_newline();
      print_string "\t(best_solution) snode.ident="; print_int snode.ident; print_newline();
      print_string (H.ss snode.state); print_newline();
      print_string ("\tupper-bound:"^(H.sc snode.upper)); print_newline();
      print_string ("\tbest-solution:"^(H.sc c)); print_newline();
      failwith "[DBBC.best_solution] upper bound inconsistency with [best_solution]"
    ));
    (c, p)

  let rec add_pproof (t:t) (s:H.s) ?sednxt (pp:H.pp) : snode =
    (* print_string "add_pproof t s ?sednxt pp"; print_newline(); *)
    let ap = aproof_of_pproof pp in
    (* We check if this intermediate state has already
         been encountered *)
    match Hashtbl.find_opt t.unique (s, ap) with
    | Some ident -> (
      (* print_string "\told state"; print_newline(); *)
      (* Yes, this intermediate state has already been seen *)
      Hashtbl.find t.memory ident
    )
    | None -> (
      (* No, this intermediate is not recorded yet in the memory *)
      (* print_string "\tnew state"; print_newline(); *)
      let ident : ident = match sednxt with
        | Some ident -> (
          assert(Hashtbl.mem t.memory ident);
          Hashtbl.remove t.memory ident;
          ident
        )
        | None -> (
          let ident = t.index in
          assert(not(Hashtbl.mem t.memory ident));
          t.index <- succ t.index;
          (* print_string "t.index : "; print_int t.index; print_newline(); *)
          ident
        )
      in
      Hashtbl.add t.unique (s, ap) ident;
      let snode : snode = match_pproof t ident s pp in
      Hashtbl.add t.memory ident snode;
      snode
    )
  and    match_pproof (t:t) (ident:ident) (state:H.s) (pp:H.pp) : snode =
    (* print_string "math_pproof t (ident:"; print_int ident; print_string ") state pp"; print_newline(); *)
    match pp with
    (* The programm should go only once in this branch (if there is a unique terminal state) *)
    | PEnd -> {
      ident; state;
      alive = Solved;
      lower = H.cost_zero();
      upper = H.cost_zero();
      diff  = H.cost_zero();
      sstate = TEnd;
      reached = ref true;
    }
    (* Leaf node of the partial proof, indicates where the computation has been stopped *)
    | PNxt state' -> (
      assert(state = state');
      let reached = ref true in
      Hashtbl.add t.estim ident (reached, H.estim state);
      let tnxt_proof, upper = H.upper state in
      let lower = H.lower state in
      {
        ident; state;
        alive = NotVisited;
        lower; upper;
        diff  = H.cost_sub upper lower;
        sstate = TNxt {tnxt_proof};
        reached;
      }
    )
    | PSeq (tseq_actions, tseq_actions_cost, state', nxt) ->
    (
      (* Cheking State Consistency *)
      let cost'', state'' = do_list_a tseq_actions state in
      if not (state'' = state')
      then (
        print_string "[DBBC.match_pproof] PSeq:(state'' = state')"; print_newline();
        assert false
      );
      assert(H.cost_cmp cost'' tseq_actions_cost = 0);
      (* i.e., assert(cost'' = tseq_actions_cost) *)
      let snode' = add_pproof t state' nxt in
      {
        ident; state; alive = Visited;
        lower = H.cost_add tseq_actions_cost snode'.lower;
        upper = H.cost_add tseq_actions_cost snode'.upper;
        diff  = snode'.diff;
        sstate = TSeq {
          tseq_next = snode'.ident;
          tseq_actions; tseq_actions_cost;
        };
        reached = ref true;
      }
    )
    | PSum (tsum_divider, tsum_ccost, nxl) ->
    (
      let ssl = List.map (fun (s, p) -> add_pproof t s p) nxl in
      let (lower, upper, diff) as total =
        ssl
        |> List.map
          (fun ss -> (ss.lower, ss.diff, ss.upper))
        |> List.fold_left add3 (zero3())
      in
      let tsum_queue = CostPQMax.empty() in
      List.iteri
        (fun i ss -> CostPQMax.add tsum_queue ss.diff (ss.ident, i))
        ssl;
      {
        ident; state; alive = Visited;
        lower; upper; diff;
        sstate = TSum {
          tsum_divider; tsum_ccost; tsum_queue;
          tsum_solved = [];
        };
        reached = ref true;
      }
    )
    | PMin (state', nxl) ->
    (
      (* Cheking State Consistency *)
      assert(state = state');
      let len_nxl = List.length nxl in
      assert(len_nxl >= 1);
      if len_nxl = 1
      then (
        match_pproof t ident state (List.hd nxl)
      )
      else (
        (* recursively add sub-pproofs *)
        let ssl = List.map (add_pproof t state) nxl in
        (* find upper bound *)
        let (tmin_upper, upper) =
             ssl
          |> MyList.opmin ~cmp:(fun ssx ssy -> H.cost_cmp ssx.upper ssy.upper)
          |> Tools.unop
          |> (fun (_, ss) -> (ss.ident, ss.upper))
        in
        (* fill up [tmin.tmin_queue] with sub-search-tree *)
        let tmin_queue = CostPQMin.empty() in
        List.iter
          (fun ss ->
            (* discarding already irrelevant branches *)
            let cmp = H.cost_cmp ss.lower upper in
            if cmp > 0 || (cmp = 0 && ss.ident <> tmin_upper)
            (* i.e. [ss.lower] > [upper] || ([ss.lower] = [upper] && [ss.ident] <> [tmin_upper]) *)
            then ()
            else (CostPQMin.add tmin_queue ss.lower ss.ident)
          )
          ssl;
        let lower : H.c = CostPQMin.first tmin_queue |> fst in
        let diff : H.c = H.cost_sub upper lower in
        {
          ident; state; alive = Visited;
          lower; upper; diff;
          sstate = TMin {
            tmin_upper; tmin_queue
          };
          reached = ref true;
        }
      )
    )

  let rec find_next (t:t) (ident:ident) : snode =
    (* print_string "[DBBC.find_next] ident:"; print_int ident; print_string ""; print_newline(); *)
    let snode = Hashtbl.find t.memory ident in
    if snode.alive = Solved then snode
    else
    (
      let prev_upper = snode.upper in
      (* we keep the value for monotony checking at the end *)
      let snode' = match snode.sstate with
      | TEnd -> snode
      | TNxt tnxt -> (
        Hashtbl.remove t.estim ident;
        (* print_string ("[DBBC.find_next:TNxt] next:ident:"^(ToS.int ident)); print_newline(); *)
        let cmp = H.cost_cmp snode.lower snode.upper in
        assert(cmp <= 0);
        if cmp = 0
        then (
          snode.alive <- Solved;
          snode
        ) else (
          let pp = H.next snode.state in
          let prev_upper = snode.upper in
          let snode' = add_pproof t snode.state ~sednxt:ident pp in
          (if not (H.cost_cmp snode'.upper prev_upper <= 0)
            then failwith "[DBBC.find_next:TNxt] upper bound is not monotonous");
          (* i.e. the new 'best solution' has a higher cost than expected (upper-bound is not monotonous) *)
          (* (* [DEBUG] *)
          let c, p = best_solution t snode'.ident in
          (if not (H.cost_cmp c snode'.upper <= 0)
            then failwith "[DBBC.find_next:TNxt] upper bound is not monotonous (with best_solution)");
          (* i.e. the new 'best solution' has a higher cost than expected (upper-bound is not monotonous) *)
           *)
          snode'
        )
      )
      | TSeq tseq -> (
        let snode' = find_next t tseq.tseq_next in
        snode.sstate <- TSeq {tseq with tseq_next = snode'.ident};
        snode.lower <- H.cost_add snode'.lower tseq.tseq_actions_cost;
        snode.upper <- H.cost_add snode'.upper tseq.tseq_actions_cost;
        snode.diff  <- snode'.diff;
        if snode'.alive = Solved
        then snode.alive <- Solved;
        snode
      )
      | TSum tsum -> (
        let total3 = (snode.lower, snode.diff  , snode.upper ) in
        let (diff', (ident', key')) = CostPQMax.pull_first tsum.tsum_queue in
        let best = Hashtbl.find t.memory ident' in
        let best3  = (best.lower  , best.diff  , best.upper  ) in
        let other3 = sub3 total3 best3 in
        let snode' = find_next t ident' in
        if snode'.alive = Solved
        then (
          tsum.tsum_solved <-
            (ident', key') :: tsum.tsum_solved;
          if CostPQMax.length tsum.tsum_queue = 0
          then snode.alive <- Solved;
          snode
        )
        else (
          CostPQMax.add tsum.tsum_queue snode'.diff (snode'.ident, key');
          let best3' = (snode'.lower, snode'.diff, snode'.upper) in
          let (lower', diff', upper') = add3 other3 best3' in
          snode.lower <- lower';
          snode.upper <- upper';
          snode.diff  <- diff';
          snode
        )
      )
      | TMin tmin -> (
        assert(CostPQMin.length tmin.tmin_queue >= 1);
        (* [TODO] adapt it to TSum *)
        (* 1) find the best element *)
        let _, best_ident, _ = tmin_find_best t snode tmin in
        (* 2) recursive call to [find_next] *)
        let snode' = find_next t best_ident in
        (* 3) put its replacement back into the queue *)
        tmin_add t snode tmin snode';
        (* 4) Compute lower-bound by finding the new best element
              AND putting it back into the queue *)
        let lower'', ident'', snode'' = tmin_find_best t snode tmin in
        tmin_add t snode tmin snode'';
        (* 5) If there is a single element left into the queue
              Return this element (calling ancestor redirection)
              Replace current node with a stub (non-calling ancestors and future ancestors)
         *)
        if CostPQMin.length tmin.tmin_queue = 1
        then (
          (if snode''.alive = Solved then snode.alive <- Solved);
          assert(H.cost_cmp snode.lower snode''.lower <= 0);
          (* i.e., snode.lower <= snode'.lower (lower increases) *)
          assert(H.cost_cmp snode.upper snode''.upper >= 0);
          (* i.e., snode.upper >= snode'.upper (upper decreases) *)
          snode.lower <- snode''.lower;
          snode.upper <- snode''.upper;
          snode.diff  <- snode''.diff;
          snode.sstate <- TSeq {
            tseq_next = snode''.ident;
            tseq_actions = [];
            tseq_actions_cost = H.cost_zero();
          };
          snode''
        )
        else snode
      )
    in
    assert(H.cost_cmp snode'.upper prev_upper <= 0);
    (* check that upper bound has not increased *)
    (* (* [DEBUG] : post-consistency checking *)
    let c, p = best_solution t ident in
    assert(H.cost_cmp c snode'.upper <= 0);
     *)
    snode'
  )

  let total_estim (t:t) : int * BNat.nat =
    let nb = ref 0 in
    let total = ref (BNat.zero()) in
    Hashtbl.iter (fun ident (_, value) ->
      incr nb;
      total := BNat.add !total value
    ) t.estim;
    (!nb, !total)

  let rec update_reach (t:t) (ident:ident) : unit =
    let snode = Hashtbl.find t.memory ident in
    if !(snode.reached) then ()
    else (
      snode.reached := true;
      match snode.sstate with
      | TEnd -> ()
      | TNxt _ -> ()
      | TSeq tseq -> (
        update_reach t tseq.tseq_next
      )
      | TSum tsum -> (
        List.iter (fun (ident, _) -> update_reach t ident) tsum.tsum_solved;
        CostPQMax.iter (fun _ (ident, _) -> update_reach t ident) tsum.tsum_queue;
      )
      | TMin tmin -> (
        CostPQMin.iter (fun _ ident -> update_reach t ident) tmin.tmin_queue;
      )
    )

  let reach_estim (t:t) (nl:snode list) : int * int * BNat.nat =
    (* reset all [reached] field to [false] *)
    Hashtbl.iter (fun ident snode ->
      snode.reached := false
    ) t.memory;
    List.iter (fun (snode:snode) ->
      update_reach t snode.ident
    ) nl;
    let nbnxt = ref 0 in
    let total = ref (BNat.zero()) in
    Hashtbl.iter (fun ident (reached, value) ->
      if !reached then (
        incr nbnxt;
        total := BNat.add !total value
      )
    ) t.estim;
    let nbnode = ref 0 in
    Hashtbl.iter (fun ident snode ->
      if !(snode.reached) then incr nbnode
    ) t.memory;
    (!nbnxt, !nbnode, !total)

  let solve ?(verbose=true) ?(wait_time=10.) (t:t) (s:H.s) : H.c * H.p =
    let root = ref (add_pproof t s (PNxt s)) in
    let niter = ref 0 in
    let last_time = ref (Tools.utime()) in
    while !root.alive <> Solved
    do
      incr niter;
      root := find_next t !root.ident;
      let new_time = Tools.utime() in
      if !niter = 1 || (new_time >= !last_time +. wait_time)
      then (
        let time_stop = Tools.utime() in
        last_time := new_time;
        print_newline();
        print_string ToS.("[DBBC.solver] niter:"^(int !niter)); print_newline();
        print_string ToS.("[DBBC.solver] upper:"^(H.sc !root.upper)); print_newline();
        print_string ToS.("[DBBC.solver] lower:"^(H.sc !root.lower)); print_newline();
        let nbnxt, estim = total_estim t in
        print_string ToS.("[DBBC.solver] [A] nb.  tnxt:"^(int nbnxt)); print_newline();
        print_string ToS.("[DBBC.solver] [A] estim:"^(BNat.to_string estim)); print_newline();
        print_string ToS.("[DBBC.solver] [A] nb. snode:"^(int (Hashtbl.length t.memory))); print_newline();
        let nbnxt, nbnode, estim = reach_estim t [!root] in
        print_string ToS.("[DBBC.solver] [B] nb.  tnxt:"^(int nbnxt)); print_newline();
        print_string ToS.("[DBBC.solver] [B] estim:"^(BNat.to_string estim)); print_newline();
        print_string ToS.("[DBBC.solver] [B] nb. snode:"^(int nbnode)); print_newline();
        let time_delta = Tools.utime() -. time_stop in
        print_string ToS.("[DBBC.solver] showing statistiques:"^(float time_delta)^" s."); print_newline();
      )
    done;
    best_solution t !root.ident

  let solve t s = solve t s
end
