open STools

module Channel =
struct

  type data = {
    mutable free : bool;
    mutable pos  : int;
    mutable meta : (string -> unit) option;
  }

  let data () =
    {free = true; pos = 0; meta = None}

  type file_channel = {
    mutable pos : int;
    mutable buf : char;
            cha : Stdlib.in_channel;
  }

  type file = unit
  type barray = unit

  module BA = Internal_BArray

  type channel_type =
    | File of file_channel
    | BArray of int ref * BA.t
    | Spy of (bool -> unit) * channel
  and  channel = data * channel_type

  type t = channel

  let get_pos ((data, _):channel) = data.pos

  let set_meta ((data, _):channel) anno : unit =
    data.meta <- Some anno

  let prn_meta ((data, _):channel) str : unit =
    match data.meta with
    | None -> ()
    | Some meta -> meta str

  let lock ((data, _):channel) : unit =
    assert(data.free);
    data.free <- false

  let unlock ((data, _):channel) : unit =
    assert(not data.free);
    data.free <- true

  let ppos ?(s="") cha =
    print_string s;
    print_string " [";
    print_int (get_pos cha);
    print_string "] ";
    print_newline()

  let mpos ?(s="") cha =
    let pos = string_of_int(get_pos cha) in
    prn_meta cha (s^" ["^pos^"] ")

  let open_spy f cha : channel =
    lock cha;
    (data(), Spy (f, cha))
  let close_spy ((data, cha) as dc)  =
    lock dc;
    match cha with
    | Spy (_, cha) -> unlock cha
    | _ -> assert false

  let open_flush ?(cha=stdout) cha0 =
    open_spy
    (fun b ->
      output_string cha (SUtils.string_of_bool b);
      Stdlib.flush cha
    )
    cha0

  let close_flush cha = close_spy cha

  let open_file target : channel = (data(), File {
    pos = 0;
    buf = '\000';
    cha = Stdlib.open_in_bin target;
  })

  let close_file (((data, ic) as dc):channel) =
    lock dc;
    match ic with
    | File fic -> (
      close_in fic.cha;
    )
    | _ -> assert false

  let open_barray (ba:BA.t) : channel =
    (data(), BArray (ref 0, ba))

  let close_barray ((data, ic):channel) =
    assert(data.free);
    match ic with
    | BArray _ -> (
      data.free <- false
    )
    | _ -> assert false

  exception End_of_OfBStream of string
end

open Channel

type 'a t = channel -> 'a

let float _ = assert false

let rec bool (data, ic) : bool =
  let rec aux0 (cha:channel_type) : bool =
    match cha with
    | File fic -> (
      if fic.pos = 0 then (
        try
          fic.buf <- Stdlib.input_char fic.cha;
        with
          | End_of_file ->
            raise (Channel.End_of_OfBStream "File")
        (* print_newline(); print_int (Char.code fic.buf); print_newline(); *)
      );
      let b = Internal_BChar.unsafe_get fic.buf fic.pos in
      (* print_string (SUtils.string_of_bool b); Stdlib.flush stdout; *)
      fic.pos <- (succ fic.pos) mod 8;
      b
    )
    | BArray (pos, ba) -> (
      assert(!pos >= 0);
      if !pos >= Internal_BArray.length ba
      then raise (Channel.End_of_OfBStream "BArray")
      else
      (
        let b = Internal_BArray.unsafe_get ba !pos in
        incr pos;
        b
      )
    )
    | Spy (f, cha) -> (
      let b = aux1 cha in
      f b;
      b
    )
  and     aux1 ((data, cha):channel) : bool =
    assert(not data.free);
    data.pos <- succ data.pos;
    aux0 cha
  in
  assert(data.free);
  data.pos <- succ data.pos;
  aux0 ic

let sized_bool_list s ic : bool list =
  List.init s (fun _ -> bool ic)

let unary ic : int =
  let rec aux carry = if bool ic
    then carry
    else (aux (carry+1))
  in aux 0

let sized_int (s:int) ic : int =
  let rec aux carry i = if i < s
    then (
      let carry' = carry +
        ((if bool ic then 1 else 0) lsl i)
      in
      aux carry' (i+1)
    )
    else carry
  in aux 0 0

let int_aux (s:int) ic : int =
  (sized_int (pred s) ic)
    lor (1 lsl (pred s))

let int_legacy ic : int =
  let size = unary ic in
  if size = 0
  then 0
  else (int_aux size ic)

let rec int_vanilla_aux ?(carry=0) ?(exp=1) cha : int =
  if bool cha
  then carry + exp
  else int_vanilla_aux
    ~carry:(carry + (if bool cha then exp else 0))
    ~exp:(exp lsl 1)
     cha

let int_vanilla cha : int =
  if bool cha
  then 0
  else int_vanilla_aux cha

let int = int_vanilla

(* more compact representation of big integers [WIP] *)
let log2_int ic : int =
  let size = int ic in
  if size = 0
  then 0
  else (
    (sized_int (size-1) ic) lor (1 lsl (size-1))
  )

(* DEBUG *)
let debug_int ic : int =
  mpos ~s:"int" ic;
  let x = int ic in
  mpos ~s:("int! : "^(string_of_int x)) ic;
  x

let int = if Tools.(mode = Debug) then debug_int else int

let profil_int ic : int =
  let pos0 = get_pos ic in
  let x = int ic in
  let pos1 = get_pos ic in
  prn_meta ic (
    ">> "^
    (string_of_int x)^","^
    (string_of_int(pos1-pos0))
  );
  x

let int = if Tools.(mode = Profil) then profil_int else int

let unary_le top cha =
  let rec aux carry top cha =
    if top = 0 then carry else
      match bool cha with
      | false -> aux (succ carry) (pred top) cha
      | true  -> carry
  in aux 0 top cha

let char ic : char =
  let c = ref '\000' in
  for i = 0 to 7
  do
    c := Internal_BChar.unsafe_set !c i (bool ic)
  done;
  !c

let bytes ic : Bytes.t =
  let n = int ic in
  let bytes = Bytes.create n in
  for i = 0 to n - 1
  do
    Bytes.unsafe_set bytes i (char ic)
  done;
  bytes

let string ic : string =
  Bytes.unsafe_to_string (bytes ic)

let barray ic : Internal_BArray.t =
  (* ppos ~s:"\nbarray.size" ic; *) (* DEBUG *)
  let n = int ic in
  (* ppos ~s:("\nbarray.size = "^(string_of_int n)^"\nbarray.data") ic; *)
  (* DEBUG *)
  let b = Internal_BArray.init n (fun _ -> bool ic) in
  (* ppos ~s:"\nbarray.data!" ic; *)
  b

let option (br:'a t) ic : 'a option =
  match bool ic with
  | false -> None
  | true  -> Some(br ic)

let int_option cha =
  Tools.int_option_of_int(int cha)

let int_bool_result cha =
  Tools.int_bool_result_of_int(int cha)

let sized_list (br:'a t) n ic : 'a list =
  List.init n (fun _ -> br ic)

let int_le top cha =
  assert(0<=top);
  match top with
  | 0 -> 0
  | 1 -> if bool cha then 1 else 0
  | _ -> (
    let n0 = Tools.math_log2 top in
    let b0 = Tools.blist_of_int top
      |> List.rev in
    assert(List.hd b0 = true);
    let b0 = List.tl b0 in
    let n = unary_le top cha in
    if n = n0 then (
      let rec aux carry = function
        | [] -> List.rev carry
        | true::b0 -> (
          match bool cha with
          | true  -> aux (true::carry) b0
          | false -> (
            let b = sized_list bool (List.length b0) cha in
            List.rev_append (false::b) carry
          )
        )
        | false::b0 -> (
          aux (false::carry) b0
        )
      in
      let b = aux [] b0 in
      Tools.int_of_bin (List.rev b)
    ) else (
      int_aux n cha
    )
  )

let list (br:'a t) ic : 'a list =
  let n = int ic in
  List.init n (fun _ -> br ic)

let bool_list ic : bool list = list bool ic

let array (br:'a t) ic : 'a array =
  let n = int ic in
  Array.init n (fun _ -> br ic)

let unit ic = ()

let pair (brA:'a t) (brB:'b t) ic : 'a * 'b =
  let a = brA ic in
  let b = brB ic in
  (a, b)

let ( * ) = pair

let trio (brA:'a t) (brB:'b t) (brC:'c t) ic : 'a * 'b * 'c =
  let a = brA ic in
  let b = brB ic in
  let c = brC ic in
  (a, b, c)

let none_list (br:'a option t) ic : 'a list =
  let rec aux carry = match br ic with
    | None -> List.rev carry
    | Some a -> aux (a::carry)
  in aux []

let bool_option_list ic : bool option list =
  let rec aux carry =
    let b1 = bool ic in
    let b2 = bool ic in
    if b1 then
      if b2 then (List.rev carry) else (aux (None::carry))
    else (aux ((Some b2)::carry))
  in aux []

let btree (br:'a t) ic : 'a Tree.btree =
  let rec aux () = if bool ic
    then (Tree.BLeaf(br ic))
    else (
      let t0 = aux () in
      let t1 = aux () in
      Tree.BNode(t0, t1)
    )
  in aux ()

let btree_code (t:'a Tree.btree) ic : 'a =
  let rec aux bt =
    match bt with
    | Tree.BLeaf x -> x
    | Tree.BNode(t0, t1) -> if bool ic
      then aux t1
      else aux t0
  in
  (* ppos ~s:"\nbtree_code" ic; *)
  let a = aux t in
  (* ppos ~s:"\nbtree_code!" ic; *)
  a

let huffman_btree (br:'a t) ic : 'a Tree.btree =
  let ht = btree br ic in
  let hc = btree_code ht in
  btree hc ic

let huffman (br:'a t) cha : 'a t =
  match bool cha with
  | true -> (
    (fun _ -> assert false)
  )
  | false -> (
    let ht = btree br cha in
    (*
    print_string "pprint huffman's btree"; print_newline();
    STree.pprint
      [BTreeUtils.to_stree (fun _ -> Tree.Leaf "") ht];
     *)
    btree_code ht
  )

let huffman2 ?(post=None) (br:'a t) cha : 'a t =
  let ht = btree (option br) cha in
  let post = match post with
    | None -> br
    | Some post -> post ()
  in
  let decode cha =
    match btree_code ht cha with
    | Some x -> x
    | None -> post cha
  in
  decode

let dag_core
  (find : int -> 'link option)
  (add  : int -> 'link -> unit)
  (recfun : channel ->
    (int -> 'link * int) ->
     int -> 'link * int
  )
  (br_ident : int -> int t)
  (cha : channel) : int -> 'link * int =
  let rec rec_link (nnode:int) : 'link * int =
    match bool cha with
    | true -> (
      let ident = br_ident nnode cha in
      match find ident with
      | None -> assert false
      | Some link -> (link, nnode)
    )
    | false -> (
      let link, nnode' = recfun cha rec_link (succ nnode) in
      add nnode link;
      (link, nnode')
    )
  in rec_link

let ctx_dag_core
  (find : 'ctx -> int -> 'link option)
  (add  : 'ctx -> int -> 'link -> unit)
  (recfun : channel ->
    ('ctx -> int -> 'link * int) ->
     'ctx -> int -> 'link * int
  )
  (br_ident : 'ctx -> int -> int t)
  (cha : channel) : 'ctx -> int -> 'link * int =
  let rec rec_link (ctx:'ctx) (nnode:int) : 'link * int =
    match bool cha with
    | true -> (
      let ident = br_ident ctx nnode cha in
      match find ctx ident with
      | None -> assert false
      | Some link -> (link, nnode)
    )
    | false -> (
      let link, nnode' =
        recfun cha rec_link ctx (succ nnode)
      in
      add ctx nnode link;
      (link, nnode')
    )
  in rec_link

let hctx_dag_core
  (find : 'ctx -> int -> 'link option)
  (add  : 'ctx -> int -> 'link -> unit)
  (recfun : channel ->
    ('ctx -> 'link) ->
     'ctx -> 'link
  )
  (br_ident : 'ctx -> int -> int t)
  (curr_nnode : 'ctx -> int)
  (incr_nnode : 'ctx -> int)
  (cha : channel) : 'ctx -> 'link =
  let rec rec_link (ctx:'ctx) : 'link =
    match bool cha with
    | true -> (
      prn_meta cha ">DAG> 0";
      let nnode = curr_nnode ctx in
      let ident = br_ident ctx nnode cha in
      match find ctx ident with
      | None -> assert false
      | Some link -> link
    )
    | false -> (
      prn_meta cha ">DAG> 1";
      let nnode = incr_nnode ctx in
      let link = recfun cha rec_link ctx in
      add ctx nnode link;
      link
    )
  in rec_link

let hctx2_dag_core
  (find : 'ctx -> int -> 'link option)
  (add  : 'ctx -> int -> 'link -> unit)
  (recfun : channel ->
    ('ctx -> 'link) ->
     'ctx -> 'link
  )
  (br_ident : 'ctx -> int -> int option t)
  (curr_nnode : 'ctx -> int)
  (incr_nnode : 'ctx -> int)
  (cha : channel) : 'ctx -> 'link =
  let rec rec_link (ctx:'ctx) : 'link =
    let nnode = curr_nnode ctx in
    match br_ident ctx nnode cha with
    | Some ident -> (
      prn_meta cha ">DAG> 0";
      match find ctx ident with
      | None -> assert false
      | Some link -> link
    )
    | None -> (
      prn_meta cha ">DAG> 1";
      let nnode = incr_nnode ctx in
      let link = recfun cha rec_link ctx in
      add ctx nnode link;
      link
    )
  in rec_link

let hctx3_dag_core
  (find : 'ctx -> int -> 'link option)
  (add  : 'ctx -> int -> 'link -> unit)
  (recfun : channel ->
    ('ctx -> 'link) ->
     'ctx -> 'link
  )
  (br_ident : 'ctx -> int -> (int, bool) result t)
  (curr_nnode : 'ctx -> int)
  (incr_nnode : 'ctx -> int)
  (cha : channel) : 'ctx -> 'link =
  let rec rec_link (ctx:'ctx) : 'link =
    let nnode = curr_nnode ctx in
    match br_ident ctx nnode cha with
    | Ok ident    -> (
      prn_meta cha ">DAG> 0";
      match find ctx ident with
      | None -> assert false
      | Some link -> link
    )
    | Error true  -> (
      prn_meta cha ">DAG> 1";
      let nnode = incr_nnode ctx in
      let link = recfun cha rec_link ctx in
      add ctx nnode link;
      link
    )
    | Error false -> (
      prn_meta cha ">DAG> 2";
      recfun cha rec_link ctx
    )
  in rec_link
