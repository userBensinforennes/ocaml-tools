open GraphGenLA.Type
open GraphGenLA.Utils

(* Section 0. Vertex-Separator *)

let rec iter_delete (carry:(int * 'e) list) (node:int) (list_node:(int * 'e) list) =
  match list_node with
    | [] -> List.rev carry
    | h::t -> (if (fst h = node) then iter_delete (h::carry) node t
                    else iter_delete carry node t)

(*[delete_node_nodes] deletes a node from a [(int * 'e) list],
  representing adajacency list*)
let delete_node_nodes (list_nodes:(int * 'e) list) (node:int) : (int * 'e) list =
  iter_delete [] node list_nodes

(*[delete_node] create a new graph equal to graph without node*)
(* use [graph_rm_vertex] instead *)
let delete_node (graph:('v, 'e) graph) (node:int) : ('v, 'e) graph =
  let new_graph = Array.make (Array.length graph) graph.(0) in
  for i = 0 to (Array.length graph) do
    let edges = delete_node_nodes graph.(i).edges node in
    new_graph.(i) <- {index = i; tag = graph.(i).tag ; edges = edges} ;
  done;
  new_graph.(node) <- {index = node ; tag = graph.(node).tag ; edges = []};
  new_graph
