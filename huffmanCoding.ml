open Tree
open STools
(*
type 'a btree =
  | BLeaf of 'a
  | BNode of ('a btree) * ('a btree)
 *)

module PQ = PriorityQueue.MakeMin
  (struct
    type t = int * int
    let compare = Stdlib.compare
  end)

let huffmanize ?(pa=None) (wl:('a * int) list) : 'a btree option =
  if List.length wl = 0 then None else (
    let pq = PQ.empty() in
    List.iter (fun (x, w) -> PQ.add pq (w, 0) (BLeaf x)) wl;
    let rec aux i =
      (match pa with
      | Some pa -> (
        print_string "iteration: "; print_int i; print_newline();
        OfSTree.pprint
          [PQ.to_stree ToSTree.(int * int) (BTreeUtils.to_stree pa) pq];
        print_string "PQ.length pq = "; print_int (PQ.length pq); print_newline();
      )
      | None -> ());
      assert(PQ.length pq >= 1);
      if PQ.length pq = 1
      then (snd(PQ.pull_first pq))
      else (
        let (w1, h1), x1 = PQ.pull_first pq in
        let (w2, h2), x2 = PQ.pull_first pq in
        PQ.add pq (w1+w2, succ (max h1 h2)) (BNode(x1, x2));
        aux (succ i)
      )
    in
    let btree = aux 0 in
    (* post-process checking [begin] *)
    let ht = BTreeUtils.reverse btree in
    List.iter (fun (a, _) ->
      if not (Hashtbl.mem ht a)
      then
      (
        (match pa with
          | Some pa -> (
            print_string "wl:\n";
            OfSTree.pprint [ToSTree.(list (pair pa int) wl)];
            print_string "btree:\n";
            OfSTree.pprint [(BTreeUtils.to_stree pa) btree];
            print_string "a:\n";
            OfSTree.pprint [pa a];
          )
          | None -> ()
        );
        assert false
      )
    ) wl;
    (* post-process checking [end] *)
    Some btree
  )

let single_huffman (wl:(bool * int) list) : bool list =
  let pq = PQ.empty() in
  List.iter (fun (x, w) ->
    PQ.add pq (w, 0) (if x then (Some []) else None)) wl;
  let rec aux () =
    if PQ.length pq = 1
    then (match snd(PQ.pull_first pq) with
      | Some code -> code
      | None -> assert false
    ) else (
      let (w1, h1), x1 = PQ.pull_first pq in
      let (w2, h2), x2 = PQ.pull_first pq in
      let x12 = match x1, x2 with
        | None, None -> None
        | Some l, None -> Some(false::l)
        | None, Some l -> Some(true ::l)
        | Some _, Some _ -> assert false
      in
      PQ.add pq (w1+w2, succ(max h1 h2)) x12;
      aux()
    )
  in aux()

let huffmanize2 ?(pa=None) (wl:('a * int) list) :
  ('a option btree) * int * 'a list =
  let listSome, listNone = List.partition
    (fun (_, n) -> assert(n>=0); n > 1) wl in
  let sumNone = List.fold_left (fun c (_, n) -> c + n) 0 listNone in
  let listNone = Tools.map fst listNone in
  let listH = (None, sumNone)::(Tools.map (fun (x, n) -> (Some x, n)) listSome) in
  match huffmanize ~pa listH with
  | None -> assert false
    (* [huffmanize] returns None if and only if the input list is empty, however it containts [(None, _)] *)
  | Some btree -> (btree, sumNone, listNone)

(* [huffmanize3 ~pa ~cmp wl =
  (posNone option, chcb = (pred-delta-nbit, alist) list, btree, sumNone, listNone)
  with [posNone option] indicating the position of the None in
  [hchb]
       [chcb] stands for Canonical Huffman Code's Book (CHCB)
          - [pred-delta-nbit] represents the difference in bit-length compared with the previous step of [chcb]
          - [alist] stores element of type 'a by increasing order according to cmp
       [btree] a btree semantically equivalent to the previously described CHCB
 *)
let huffmanize3 ?(pa=None) ?(cmp=Stdlib.compare)
    (wl:('a * int) list) :
  int option * (int * ('a list)) list * int * 'a list =
  let bt, sumNone, listNone = huffmanize2 ~pa wl in
  let height = BTreeUtils.height bt in
  let store : 'a list array = Array.make (succ height) [] in
  let posNone = ref None in
  BTreeUtils.iter (fun code op ->
    let len = List.length code in
    match op with
    | Some a -> (
      store.(len) <- a::store.(len)
    )
    | None -> posNone := Some len
  ) bt;
  let rec aux i bitlen carry =
    if i < Array.length store then (
      let alist = Array.unsafe_get store i in
      if alist = [] then (
        aux (succ i) bitlen carry
      ) else (
        let alist = List.sort cmp alist in
        let delta = i-bitlen in
        aux (succ i) (succ i) ((delta, alist)::carry)
      )
    ) else (List.rev carry)
  in
  let chcb = aux 0 0 [] in
  (!posNone, chcb, sumNone, listNone)

module BA   = Internal_BArray
module BNat = Internal_BArray_Nat

let code_of_bitlen
    (bllist:(int * 'a) list) : (BA.t * 'a) list =
  match bllist with
  | [] -> []
  | (bl, a)::tail -> (
    let rec aux code bitlen carry = function
      | [] -> List.rev carry
      | (bl, a)::tail -> (
        let code =
          BNat.(shift_lefti (succ code) (bl-bitlen))
        in
        aux code bl ((code, a)::carry) tail
      )
    in
    let code = BNat.sized_zero bl in
    aux code bl [(code, a)] tail
  )

let btree_of_code
    (clist:(BA.t * 'a) list) : 'a btree option =
  match clist with
  | [] -> None
  | liste -> (
    let rec aux i = function
      | [] -> assert false
      | [x] -> BLeaf (snd x)
      | liste -> (
        let liste0, liste1 = List.partition
          (fun x -> BA.get (fst x) i) liste in
        assert(liste0<>[]);
        assert(liste1<>[]);
        BNode (aux(succ i) liste0, aux(succ i) liste1)
      )
    in Some(aux 0 liste)
  )

let bllist_of_chcb (oppos:int option)
  (chcb:(int*('a list)) list) : (int * 'a option) list =
  if chcb = [] then [] else
  let pos = match oppos with
    | Some x -> x
    | None -> -1
  in
  let rec aux0 bl carry = function
    | []   -> carry
    | t::h ->
      aux0 bl ((bl, Some t)::carry) h
  in
  let rec aux1 bitlen carry = function
    | [] -> List.rev carry
    | (dt, al)::tail -> (
      let bitlen = bitlen + dt in
      let carry = if bitlen = pos
        then ((bitlen, None)::carry)
        else carry
      in
      aux1 (succ bitlen) (aux0 bitlen carry al) tail
    )
  in aux1 0 [] chcb

let btree_of_chcb oppos chcb =
  bllist_of_chcb oppos chcb
  |> code_of_bitlen
  |> btree_of_code
  |> Tools.unop

let of_htbl (h:('a, 'b)Hashtbl.t) : ('a * 'b) list =
  let stack = ref [] in
  Hashtbl.iter (fun k v -> stack := (k, v) :: !stack) h;
  !stack

let to_htbl ?(size=10000) (l:('a * 'b)list) :
    ('a, 'b)Hashtbl.t =
  let h = Hashtbl.create size in
  List.iter (fun (k, v) -> Hashtbl.add h k v) l;
  h

let htbl_of_chcb oppos chcb =
  bllist_of_chcb oppos chcb
  |> code_of_bitlen
  |> to_htbl
