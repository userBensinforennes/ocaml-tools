(* All Right Reserved

   Copyright (c) 2020 Joan Thibault
*)
open STools
open BTools

(* Data Structure *)

type graph = {
  nodes   : int list;
  cliques : int list list;
(* each [int list] is [SetList.sorted] *)
}

module ToS =
struct
  include ToS
  let graph graph =
    "{nodes="^(list int graph.nodes)^"; cliques="^(list(list int)graph.cliques)^"}"
end

let print_graph g =
  print_endline (ToS.graph g)

(* Tools *)

let check (graph:graph) =
(* returns true iff the graph is properly formed *)
  let check_clique clique =
    SetList.sorted clique &&
    SetList.subset_of clique graph.nodes
  in
  (SetList.sorted graph.nodes) &&
  (List.for_all check_clique graph.cliques)

let reduce (graph:graph) =
  {
    nodes = SetList.sort graph.nodes;
    cliques = List.map SetList.sort graph.cliques;
  }

(* neighbourhood related tools *)

let voisins (graph:graph) (set:int list) : int list =
  List.fold_left (fun voisins clique ->
    if SetList.nointer set clique
    then voisins
    else SetList.union voisins clique
  ) set graph.cliques

let voisins_strict graph set =
  SetList.minus (voisins graph set) set

(* Conversion *)

let internal_to_GraphLA (len:int) (graph:int list list) : int list array =
  match graph with
  | [] -> [| |]
  | _ -> (
    let edges = Array.make len [] in
    List.iter (fun clique ->
      List.iter (fun node ->
        assert(0 <= node && node < len);
        edges.(node) <- SetList.union edges.(node) clique
      ) clique
    ) graph;
    edges
  )

let to_GraphLA (graph:graph) : GraphLA.graph =
  match graph.nodes with
  | [] -> GraphLA.{nodes = []; edges = [||]}
  | nodes -> (
    let maxi = MyList.last nodes |> snd in
    let edges = internal_to_GraphLA (succ maxi) graph.cliques in
    GraphLA.{nodes; edges}
  )
