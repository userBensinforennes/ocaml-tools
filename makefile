## Declare Source Folders
SRCS=-Is ounits

## Declare Librairies
LIBS=-libs $(BOREAL_OPAM_ROOT)/lib/ocaml/unix

## Compute Proc Number
NPROC=$(shell nproc)

## Locate [ocamlbuild]
#OcamlBuild="/home/my_name/.opam/system/bin/ocamlbuild"
OcamlBuild=${BOREAL_OPAM_ROOT}/bin/ocamlbuild
#OcamlBuild="ocamlbuild"

## Assemble [OcamlBuild] command
OB=$(OcamlBuild) -r -j $(NPROC) $(LIBS) $(SRCS)

## Declare post-processing command [POST]
POST=(mv *.native *.byte bin/ &> /dev/null) || true
.PHONY: all bin tests noreg

all : ocamltools
	$(POST)

ounits: pre
	$(OB) \
		ounits/GGLA_domination_ounit.native \
		ounits/GGLA_domination_ounit.d.byte \
		ounits/myArray_ounit.native \
		ounits/bicimal_ounit.native \
		ounits/graphHFT_ounit_data.native \
		ounits/graphHFT_ounit.native \
		ounits/graphHFT_ounit.d.byte \
		ounits/myList_ounit.native \
		ounits/myList_ounit.d.byte \
		ounits/bnat_ounit.native \
		ounits/setList_ounit.native
	rm -rf ounits_bin/ &> /dev/null || true
	mkdir ounits_bin/ || true
	mv *.native *.byte ounits_bin/ &> /dev/null || true
	./ounits_bin.sh

pre:
	rm -rf bin
	mkdir bin
	./format.sh

ocamltools : pre
	$(OB) \
		DBBC.native \
		binUtils.native \
		boundedMemoSearch.native \
		bTools.native \
		bTreeUtils.native \
		extra.native \
		graphAA.native \
		graphGenH.native \
		graphGenLA.native \
		GGLA_cliques_detection.native \
		GGLA_domination.native \
		GGLA_vertex_separator_linear.native \
		GGLA_vertex_separator.native \
		graphHFT.native \
		graphLA.native \
		graphLL.native \
		graphWLA.native \
		h2Table.native \
		huffmanCoding.native \
		huffmanIO.native \
		internal_BArray.native \
		internal_BArray_Nat.native \
		internal_BChar.native \
		internal_IoB.native \
		internal_OfB.native \
		internal_OfBStream.native \
		internal_ToB.native \
		internal_ToBStream.native \
		intHeap.native \
		iter.native \
		memoTable.native \
		myArray.native \
		myList.native \
		o3Extra.native \
		o3.native \
		o3Utils.native \
		poly.native \
		priorityQueue.native \
		setList.native \
		sTools.native \
		tools.native \
		tree.native \
		unionFind.native

clean:
	$(OcamlBuild) -clean
	rm -rf _build
	rm -f bin/*.native bin/*.byte *.native *.d.byte
