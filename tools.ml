type mode =
  | Normal
  | Perf
  | Profil
  | Debug

let mode = Normal

type ('a, 'b) ab = A of 'a | B of 'b

type prio_t =
  | ASAP
  | ALAP
  | ALL

let string_of_prio = function
  | ASAP -> "ASAP"
  | ALAP -> "ALAP"
  | ALL  -> "ALL"

let prio_of_string = function
  | "ASAP" -> ASAP
  | "ALAP" -> ALAP
  | "ALL"  -> ALL
  | _ -> assert false

(* [BrokenWeakRef string] exception should be raised when an ad-hoc weak pointer is broken *)
exception BrokenWeakRef of string

let opfun f = function
  | Some x -> Some(f x)
  | None   -> None

let isSome  = function Some _ -> true  | None -> false
let isNone  = function Some _ -> false | None -> true
let isOk    = function Ok _ -> true  | Error _ -> false
let isError = function Ok _ -> false | Error _ -> true

let map f l = List.rev (List.rev_map f l)

let mapreduce map reduce init liste = List.fold_left (fun x y -> reduce x (map y)) init liste

let cmp a b =
  if (a = b)
    then 0
  else if (a < b)
    then 1
    else (-1)

let cswap c (a, b) = if c then (b, a) else (a, b)
let op2 op (a, b) = (op a, op b)
let opop (opa, opb) (a, b) = (opa a, opb b)

let cond_fun = function
  | true  -> (fun f x -> f x)
  | false  -> (fun f x ->   x)

let op_fun f = function
  | None    -> None
  | Some x  -> Some(f x)

let subset special default =
  let rec aux = function
    | _, 0         -> []
    | 0, size      -> special::(aux((-1), (size-1)))
    | idx, size    -> default::(aux((idx-1), (size-1)))
  in (fun idx size -> assert((idx>=0)&&(size>=0)&&(idx<size)); aux(idx, size))

let subset_t special default =
  let rec aux = function
    | _, 0         -> []
    | 0, size      -> special@(aux((-1), (size-1)))
    | idx, size    -> default@(aux((idx-1), (size-1)))
  in (fun idx size -> assert((idx>=0)&&(size>=0)&&(idx<size)); aux(idx, size))

let opfold_left f x = function
  | Some y -> Some(f x y)
  | None   -> Some x

let opfold_right f = function
  | Some x -> (fun y -> Some(f x y))
  | None   -> (fun y -> Some y)

let opopfold f x y = match x, y with
  | None  , None   -> None
  | Some z, None
  | None  , Some z -> Some z
  | Some x, Some y -> Some (f x y)

let opmax x = opfold_left max x
let opmin x = opfold_left min x

let opopmin x = opopfold min x

let mm x (mini, maxi) = (min mini x, max maxi x)

let opmm x = function
  | Some mmi -> Some(mm x mmi)
  | None     -> Some(x, x)

let rec math_pow = function
  | 0 -> (function 0 -> 1 | _ -> 0)
  | 1 -> (fun _ -> 1)
  | 2 -> (fun x -> 1 lsl x)
  | x -> (function
    | 0  -> 1
    | 1  -> x
    | n when n mod 2 = 0  -> math_pow (x*x) (n/2)
    | n            -> x*(math_pow (x*x) (n/2))
  )

let quick_pow (unit : 'a) (( * ) : 'a -> 'a -> 'a) (x : 'a) (k : int) : 'a =
  let rec aux = function
    | 0 -> (fun _ -> unit)
    | 1 -> (fun x -> x   )
    | n when n mod 2 = 0 -> (fun x ->       aux (n/2) (x*x)   )
    | n                  -> (fun x -> x * ( aux (n/2) (x*x) ) )
  in
  aux k x

let math_log x =
  let rec aux = function
  (*  | n when n <= 0  -> assert false *)
    | 1 -> 0
    | n when n < x  -> 1
    | n        -> 1+(aux (n/x))
  in (fun x -> assert(x > 0); aux x)

let math_log2 x =
  let rec aux i x = if x = 0
    then i
    else aux (i+1) (x/2)
  in aux 0 x

let math_log_up x y =
  assert(x > 0);
  assert(y > 0);
  let z = math_log x y in
  let z' = if y > (math_pow x z)
    then (z+1)
    else z
  in
  assert(z' >= 0);
  if z = 0
    then (assert(y = 1))
    else (assert(math_pow x (z'-1) < y && y <= math_pow x z'));
  z'

let check ?(debug_only=false) func objet =
  if mode = Debug || (not debug_only)
  then (
    assert(func objet);
    objet
  )
  else objet

let check_print_print = ref true

let check_print ?(debug_only=false) func print objet =
  if mode = Debug || (not debug_only)
  then (
    if func objet
    then objet
    else (
      (if !check_print_print then print objet);
      assert false
    )
  )
  else objet

let blist_of_int x : bool list =
  let rec aux carry x =
    if x = 0
    then (List.rev carry)
    else aux ((x mod 2 = 1)::carry) (x/2)
  in
  assert(x>=0);
  aux [] x

let bin_of_int x : bool array =
  blist_of_int x
  |> Array.of_list

exception NoneError

let unop = function
  | None -> raise NoneError
  | Some x -> x

let bin_of_char c =
  let rec aux carry x = function
    | 0 -> List.rev carry
    | n -> aux ((x mod 2 = 1)::carry) (x/2) (n-1)
  in aux [] (Char.code c) 8

(* int_of_bin [b0; b1; ...; bn] = sum_i (bi x 2^i) *)
let int_of_bin bl =
  let rec aux carry = function
    | [] -> carry
    | h::t -> aux (carry*2+(if h then 1 else 0)) t
  in aux 0 (List.rev bl)

let char_of_bin bl = Char.chr ( int_of_bin bl )

(* with [p] a predicate and [a] an array *)
let array_index (p : 'a -> bool) (a : 'a array) : int option =
  let n = Array.length a in
  let rec aux x =
      if x < n
      then (if p a.(x) then Some x else aux (x+1))
      else None
  in aux 0

(* assumes [Array.length array > 0] *)
let tree_of_array (cons : 'a -> 'a -> 'a) (array : 'a array) =
  let n = Array.length array in
  assert(n > 0);
  let rec aux i j =
    assert (i < j);
    if (j-i) = 1
    then array.(i)
    else(
      let c = (i+j)/2 in
      cons (aux i c) (aux c j)
      )
  in aux 0 n

let tree_of_list cons liste = tree_of_array cons (Array.of_list liste)

let rec power_2_above x n =
  if x >= n then x
  else if x * 2 > Sys.max_array_length then x
  else power_2_above (x * 2) n

let inv_perm (perm: int array) : int array =
  let n = Array.length perm in
  let perm' = Array.make n (-1) in
  for i = 0 to n - 1
  do
    let x = perm.(i) in
    (* sanity check *)
    assert(0<= x && x < n);
    perm'.(x) <- i
  done;
  (* sanity check *)
  Array.iter (fun x -> assert(x>=0)) perm';
  perm'

(*
  we define a mod b = r
  as the smallest non-negative element of the equivalent class of a in R/bZ (with R being the set of real numbers and Z the set of signed integers)
*)

let int_mod x y =
(* according to our definition a mod b = a mod (-b) *)
  let y = abs y in
  let m = x mod y in
  if m < 0 then (m+y) else m

let int_div x y =
  (x - (int_mod x y))/y

let int_quomod x y =
  let q, r = (int_div x y, int_mod x y) in
  (q, r)

let cnt_init size = Hashtbl.create size
let cnt_click (h:('a, int) Hashtbl.t) (x:'a) : unit =
  try
    let n = Hashtbl.find h  x in
    Hashtbl.replace h x (succ n)
  with
    Not_found -> Hashtbl.add h x 1

let cnt_end (h:('a, int) Hashtbl.t) : ('a * int) list =
  let l = ref [] in
  Hashtbl.iter (fun k n -> l:= (n, k) :: !l) h;
  !l
    |> List.sort Stdlib.compare
    |> map (fun (n, k) -> (k, n))

let cnt size =
  let cnt = cnt_init size in
  (cnt, cnt_click cnt, fun () -> cnt_end cnt)

let time txt f x =
  print_string txt; print_newline();
  let init = Sys.time() in
  let y = f x in
  print_string txt; print_string "! : ";
  print_float (Sys.time() -. init); print_newline();
  y

let int_of_int_option (opx:int option) : int =
  match opx with
  | None -> 0
  | Some x -> succ x

let int_option_of_int (x':int) : int option =
  match x' with
  | 0 -> None
  | sx -> Some(pred sx)

let int_of_int_bool_result (r:(int, bool) result) : int =
  match r with
  | Error false -> 0
  | Error true  -> 1
  | Ok x        -> (
    assert(x>=0);
    (x+2)
  )

let int_bool_result_of_int (r:int) : (int, bool) result =
  assert(r>=0);
  match r with
  | 0 -> Error false
  | 1 -> Error true
  | x -> Ok (x-2)

let pick1in2 l =
  let rec aux carry = function
    | [] -> List.rev carry
    | [x] -> List.rev (x::carry)
    | x::_::tail -> aux (x::carry) tail
  in aux [] l

exception IntError of int

let tee (pipe:'a -> unit) (a:'a) : 'a =
  pipe a;
  a

let tee_stream stream =
  let rec aux x =
    try
    (
      let char = Stream.next stream in
      print_char char; flush stdout;
      Some char
    )
    with _ -> None
  in Stream.from aux

let input_lines cha =
  let rec aux carry =
    try
      aux ((input_line cha)::carry)
    with End_of_file -> List.rev carry
  in aux []

let utime () = (Unix.times ()).Unix.tms_utime

let time_print = ref true

let time_start s =
  if !time_print
  then (
    print_string ("Computing "^s^"... ");
    let start = utime() in
    (fun () ->
      let delta = utime() -. start in
      print_float delta; print_string " s."; print_newline();
      delta
    )
  )
  else (
    let start = utime() in
    (fun () -> utime() -. start)
  )

let unop_succ : int option -> int =
  function
  | Some i -> succ i
  | None   -> 0

let internal_print_stat (stats:Gc.stat) = Gc.(
  print_string "minor (bytes)           : "; print_float (8. *. stats.minor_words); print_newline();
  print_string "promoted (bytes)        : "; print_float (8. *. stats.promoted_words); print_newline();
  print_string "major (bytes)           : "; print_float (8. *. stats.major_words); print_newline();
  print_string "minor collections (int) : "; print_int stats.minor_collections; print_newline();
  print_string "major collections (int) : "; print_int stats.major_collections; print_newline();
  print_string "heap (bytes)            : "; print_int (8*stats.heap_words); print_newline();
  print_string "heap chunks (int)       : "; print_int stats.heap_chunks; print_newline();
  print_string "live (bytes)            : "; print_int (8*stats.live_words); print_newline();
  print_string "live blocks (int)       : "; print_int stats.live_blocks; print_newline();
  print_string "free (bytes)            : "; print_int (8*stats.free_words); print_newline();
  print_string "free blocks (int)       : "; print_int stats.free_blocks; print_newline();
  print_string "largest free (bytes)    : "; print_int (8*stats.largest_free); print_newline();
  print_string "fragments (int)         : "; print_int stats.fragments; print_newline();
  print_string "fragments (bytes)       : "; print_int (8*stats.fragments); print_newline();
  print_string "compactions (int)       : "; print_int stats.compactions; print_newline();
  print_string "max heap (bytes)        : "; print_int (8*stats.top_heap_words); print_newline();
  print_string "stack (bytes)           : "; print_int (8*stats.stack_size); print_newline();
  ())

let internal_print_quick_stat (stats:Gc.stat) = Gc.(
  print_string "minor (bytes)           : "; print_float (8. *. stats.minor_words); print_newline();
  print_string "promoted (bytes)        : "; print_float (8. *. stats.promoted_words); print_newline();
  print_string "major (bytes)           : "; print_float (8. *. stats.major_words); print_newline();
  print_string "minor collections (int) : "; print_int stats.minor_collections; print_newline();
  print_string "major collections (int) : "; print_int stats.major_collections; print_newline();
  print_string "heap (bytes)            : "; print_int (8*stats.heap_words); print_newline();
  print_string "heap chunks (int)       : "; print_int stats.heap_chunks; print_newline();
  print_string "compactions (int)       : "; print_int stats.compactions; print_newline();
  print_string "max heap (bytes)        : "; print_int (8*stats.top_heap_words); print_newline();
  print_string "stack (bytes)           : "; print_int (8*stats.stack_size); print_newline();
  ())

let print_stats () : unit = internal_print_stat (Gc.stat())
let print_quick_stats () : unit = internal_print_quick_stat (Gc.quick_stat())
