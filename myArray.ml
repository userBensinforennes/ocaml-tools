open Extra

let map_of_list (f:'a -> 'b) (l:'a list) : 'b array =
  match l with
  | [] -> [||]
  | hd::tl -> (
    let len = List.length l in
    let arr = Array.make len (f hd) in
    let rec loop i = function
      | [] -> ()
      | hd::tl -> (
        Array.unsafe_set arr i (f hd);
        loop (succ i) tl
      )
    in
    loop 1 tl;
    arr
  )

let map_of_rev_list (f:'a -> 'b) (l:'a list) : 'b array =
  match l with
  | [] -> [||]
  | hd::tl -> (
    let len = List.length l in
    let arr = Array.make len (f hd) in
    let rec loop i = function
      | [] -> ()
      | hd::tl -> (
        Array.unsafe_set arr i (f hd);
        loop (pred i) tl
      )
    in
    loop (pred(pred len)) tl;
    arr
  )

let rev (a:'a array) : 'a array =
  let n = Array.length a in
  Array.init n (fun i -> a.(n-1-i))

let count f a =
  let cnt = ref 0 in
  Array.iteri (fun i x -> if f x then incr cnt) a;
  !cnt

let counti f a =
  let cnt = ref 0 in
  Array.iteri (fun i x -> if f i x then incr cnt) a;
  !cnt

let count_true = count (fun x -> x)
let count_false = count (fun x -> not x)

let count_Some  l = count Tools.isSome  l
let count_None  l = count Tools.isNone  l
let count_Ok    l = count Tools.isOk    l
let count_Error l = count Tools.isError l

let list_of_indexes (p:'a -> bool) (a:'a array) : int list =
  let len = Array.length a in
  let rec loop (carry:int list) (i:int) =
    if i < len
    then if p (Array.unsafe_get a i)
      then loop (i::carry) (succ i)
      else loop     carry  (succ i)
    else (List.rev carry)
  in loop [] 0

let list_of_indexes_true (a:'a array) : int list =
  let len = Array.length a in
  let rec loop (carry:int list) (i:int) =
    if i < len
    then if Array.unsafe_get a i
      then loop (i::carry) (succ i)
      else loop     carry  (succ i)
    else (List.rev carry)
  in loop [] 0

let list_of_indexesi (p:int -> 'a -> bool) (a:'a array) : int list =
  let len = Array.length a in
  let rec loop (carry:int list) (i:int) =
    if i < len
    then if p i (Array.unsafe_get a i)
      then loop (i::carry) (succ i)
      else loop     carry  (succ i)
    else (List.rev carry)
  in loop [] 0

let indexes f a : int array =
  list_of_indexes f a |> Array.of_list
let indexes_true a : int array =
  list_of_indexes_true a |> Array.of_list
let indexesi f a : int array =
  list_of_indexesi f a |> Array.of_list

let of_indexes (len:int) (l:int list) : bool array =
  let a = Array.make len false in
  List.iter (fun i -> a.(i) <- true) l;
  a

let flatten (a:'a array array) : 'a array =
  Array.of_list (List.flatten (Tools.map Array.to_list (Array.to_list a)))

let unop (opa:'a option array) : 'a array =
  opa
  |> Array.to_list
  |> MyList.unop
  |> Array.of_list

let map_unop (opa:'a option array) : 'a array =
  Array.map Tools.unop opa

(* [unop_rename option_array = (array, rename)]
  \forall i,
  match option_array.(i) with
  | Some v -> v = array.(rename.(j))
  | None   -> rename.(j) = -1
 *)
let unop_rename (opa:'a option array) : ('a array) * int array =
  let oplen = Array.length opa in
  let len = count_Some opa in
  (* we initialize an intermediary graph with None *)
  (* it has the correct number of vertices *)
  let a = Array.make len None in
  (* we initialize [rename.(i) = -1] *)
  let rename = Array.make oplen (-1) in
  let index = ref 0 in
  for i = 0 to oplen - 1
  do
    match opa.(i) with
    | Some v -> (
      let j = !index in
      incr index;
      a.(j) <- Some v;
      rename.(i) <- j;
    )
    | None -> ()
  done;
  assert(!index = len);
  (map_unop a, rename)

(*  if [i] >= [Array.length a]
      then [find_prev p a i = find_last p a]
    if [i] <= 0
      then find_prev p a i = None

    ensures:
      \forall p a i.
        find_prev p a i = None <=>
          \forall j < i. p a.(j) = false
      \forall p a i j.
        find_prev p a i = Some j <=>
          j < i /\ (\forall k. j < j < i => p a.(k) = false)
    ensures(equivalent definition):
      \forall p a i j.
        find_prev p a i = Some j => j < i /\ p a.(j) = true
      \forall p a i.
        let j := Tools.unop_succ (find_prev p a i) in
        \forall k. j <= k < i => p a.(k) = false
 *)
(* (* straight recursion *)
let rec find_prev
    (p:'a -> bool)
    (a:'a array)
    (i:int) : int option =
       if i <= 0
    then None
  else if i >= Array.length a
    then find_prev p a (Array.length a)
  else if p a.(pred i)
    then Some (pred i)
    else find_prev p a (pred i)
 *)
   (* wrapped recursion *)
let find_prev (p:'a -> bool) (a:'a array) (i:int) : int option =
  let rec loop i =
    if i >= 0
    then if p a.(i)
      then Some i
      else loop (pred i)
    else None
  in
  loop (min i (Array.length a))

let find_prev_Some (a:'a option array) (i:int) : (int * 'a) option =
  match find_prev Tools.isSome a i with
  | None -> None
  | Some j -> (
    let v = Tools.unop a.(j) in
    Some(j, v)
  )

(*  [bool_rename p aa = int * iaa]
    where:
      p : 'a -> bool
      aa : 'a array
      iaa : int option array

    ensures:
      \forall i. iaa.(i) = None -> p aa.(i) = false
      \forall i. iaa.(i) = Some j ->
        p aa.(i) = true /\
        j = Tools.unop_suc
          (find_prev iaa
          match find_prev_some iaa i with
          | None -> j = 0
          | Some (_, k) -> j = k + 1
 *)

let bool_rename (p:'a -> bool) (aa:'a array) : int option array* int =
  let len = Array.length aa in
  let rename = Array.make len None in
  let rec loop i j =
         if i < len
    then if p aa.(i)
      then (
        rename.(i) <- Some j;
        loop (succ i) (succ j)
      )
      else
       (loop (succ i) j)
    else j
  in
  (rename, loop 0 0)

let cumul_bool (p:'a -> bool) (aa:'a array) : int array * int =
  let len = Array.length aa in
  let cum = Array.make len (-1) in
  let rec loop i s =
    if i < len
    then if p aa.(i)
      then (cum.(i) <- succ s; loop (succ i) (succ s))
      else (cum.(i) <-      s; loop (succ i)       s )
    else s
  in
  (cum, loop 0 0)

let cumul_bool_list (p:'a -> bool) (l:'a list) : int array * int =
  l |> Array.of_list
    |> cumul_bool p

let for_all2 p a1 a2 =
  let len = Array.length a1 in
  if len <> Array.length a2
    then (raise (Invalid_argument "MyArray.for_all2"));
  let rec aux a1 a2 i =
    (i >= len) || (
      (p (Array.unsafe_get a1 i)
         (Array.unsafe_get a2 i))
      && (aux a1 a2 (succ i))
    )
  in aux a1 a2 0

let for_all_true a = Array.for_all (fun v -> v) a

let map_find_first2 (opfun:'a -> 'b -> 'c option)
  (aa:'a array) (bb:'b array) : 'c option =
  let len = Array.length aa in
  if len <> Array.length bb
    then invalid_arg "MyArray.map_find_first2 invalid argument";
  let rec aux i =
    if i < len
      then match opfun
        (Array.unsafe_get aa i)
        (Array.unsafe_get bb i) with
        | Some some -> Some some
        | None -> aux (succ i)
      else None
  in aux 0

let sum (mapfun : 'a -> int) (a:'a array) : int =
  let sum = ref 0 in
  for i = 0 to Array.length a - 1
  do
    sum := !sum + (mapfun(Array.unsafe_get a i))
  done;
  !sum

let assoc (x:'a) (xy:('a * 'b) array) : 'b =
  let len : int = Array.length xy in
  let rec aux (i:int) : 'b =
    if i < len then (
      let (x', y') = Array.unsafe_get xy i in
      if x = x' then y' else (aux (succ i))
    ) else (assert false)
  in aux 0

let pprint_array (sa:'a -> string) (aarray:'a array) : unit =
  print_string "[|"; print_newline();
  Array.iter (fun a ->
    print_string "\t";
    print_string (sa a);
    print_string ";";
    print_newline();
  ) aarray;
  print_string "|]";
  print_newline();
  ()

let for_alli (p:int -> 'a -> bool) (a:'a array) : bool =
  let n = Array.length a in
  let rec loop i =
    if i = n
      then true
    else if p i (Array.unsafe_get a i)
      then loop (succ i)
      else false
  in
  loop 0

let between min max a : bool =
  Array.for_all (fun v -> min <= v && v <= max) a

let is_permut_assert_bound a : bool =
  let len = Array.length a in
  let r = Array.make len (-1) in
  Array.iteri (fun i v ->
    assert(0 <= v && v < len);
    r.(v) <- i;
  ) a;
  Array.for_all (fun v -> v >= 0) r

let is_permut a : bool =
  let len = Array.length a in
  (* by default r.(i) = true
     one visited r.(i) = false
     if visited twice return false
   *)
  let r = Array.make len true in
  let rec loop i =
    if i < len
    then (
      let v = Array.unsafe_get a i in
      (0 <= v && v < len && Array.unsafe_get r v &&
       (Array.unsafe_set r v false; loop(succ i)))
    )
    else true
  in
  loop 0
  (* using pigeon hole principle,
     both [r] and [a] have [len] cell,
     each cell of [a] puts one cell of [r]
     to false, if there is no collision in [r]
     then all cells are [false] thus
     [a] is a permutation of {0, ..., len-1}
   *)
