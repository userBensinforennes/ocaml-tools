(* [TODO] rajouter de la doc *)
type test = {
  test_name : string;
  test_func : unit -> unit;
}

type test_poll = test list ref

type error = {
  error_name : string;
  error_text : string option;
  error_done : bool;
}

exception OUnitError of error

let error_string name ?text () =
  raise (OUnitError {error_name = name; error_text = text; error_done = true})

let error_poll_enable = ref false

type error_poll = string list ref
let error_poll : error_poll = ref []

let print_error (f:unit -> string) : unit =
  if !error_poll_enable
  then (error_poll := (f()) :: !error_poll)

let reset_error () : unit = error_poll := []

let print_error_poll () : unit =
  if !error_poll_enable then (
    print_endline "error report:";
    List.iter print_string (List.rev !error_poll)
  );
  reset_error();
  ()

let ouniterror name ?(func=(fun () -> None)) =
  let error =
    try
      {
        error_name = name;
        error_text = func ();
        error_done = true
      }
    with _ ->
      {
        error_name = name;
        error_text = None;
        error_done = false;
      }
  in raise (OUnitError error)

let error_assert name ?(func=(fun () -> None)) cond : unit =
  if not cond then (ouniterror name ~func)

let test_poll : test_poll = ref []

let push_test name func : unit=
  test_poll := {test_name = name; test_func = func} :: !test_poll;
  ()

exception NoGenericError

module ToS =
struct
  let int (x:int) : string = string_of_int x
  let string (s:string) : string = s
  let unary sa a : string = "("^(sa a)^")"
  let pair sa sb (a, b) : string = ("( "^(sa a)^", "^(sb b)^")")
  let trio sa sb sc (a, b, c) : string = ("( "^(sa a)^", "^(sb b)^", "^(sc c)^")")

  let ignore _ : string = " _ "
end

let generic_error : exn -> string = function
  | Exit -> "Exit"
  | Match_failure (s, i, j) -> "Match_failure"^ToS.(trio string int int (s, i, j))
  | Assert_failure (s, i, j) -> "Assert_failure"^ToS.(trio string int int (s, i, j))
  | Invalid_argument s -> "Invalid_argument"^ToS.(unary string s)
  | Failure s -> "Failure"^ToS.(unary string s)
  | Not_found -> "Not_found"
  | Out_of_memory -> "Out_of_memory"
  | Stack_overflow -> "Stack_overflow"
  | Sys_error s -> "Sys_error"^ToS.(unary string s)
  | End_of_file -> "End_of_file"
  | Division_by_zero -> "Division_by_zero"
  | Sys_blocked_io -> "Sys_blocked_io"
  | Undefined_recursive_module (s, i, j) -> "Undefined_recursive_module"^ToS.(trio string int int (s, i, j))
  | _ -> raise  NoGenericError

let ouniterror_of_generic_error name (error:exn) =
  match error with
  | OUnitError error -> raise(OUnitError error)
  | _ -> (
    ouniterror name ~func:(fun () ->
      try Some("Generic Failure: "^(generic_error error))
      with
      | NoGenericError -> Some "NoGenericFailure"
      | _ -> Some "Failure in generic_failure"
    )
  )

let catch_generic name (todo:unit -> 'b) : 'b =
  try
    todo()
  with error -> ouniterror_of_generic_error name error

let run_tests () : unit =
  List.iter
    (fun test ->
      print_string test.test_name; print_string " : "; flush stdout;
      try (
        error_poll_enable := false;
        Gc.full_major();
        test.test_func();
        Gc.full_major();
        print_string "success"; print_newline();
      )
      with
      | OUnitError error -> (
        print_endline "OUnitError";
        print_string "\tname: ";
        print_string error.error_name;
        print_string " ";
        print_endline (if error.error_done then "{ =) }" else "{ =( }");
        (match error.error_text with
        | None -> print_endline "None";
        | Some text -> print_endline "Some"; print_endline text);
        print_error_poll();
      )
      | err -> (
        try
          print_endline ("Failure: "^(generic_error err));
          print_error_poll();
        with
        | NoGenericError -> (
          print_endline "FAILURE : No Generic Error";
          print_error_poll();
          raise err
        )
        | _ -> (
          print_endline "FAILURE!!!";
          print_error_poll();
          raise err;
        )
      )
    )
    (List.rev !test_poll)

(* raises an error if Not_found is NOT raised *)
(* [eassert_*] prefix used for 'error assertion' *)
let eassert_not_found name ?(func=(fun()->None)) (todo:unit -> unit) =
  try
    todo();
    ouniterror name ~func;
  with
    | Not_found -> ()
    | error -> ouniterror_of_generic_error (name^" [failure]") error

let eassert_failure name ?fmsg ?(func=(fun()->None)) (todo:unit -> unit) =
  try
    todo();
    ouniterror name ~func;
  with
    | Failure msg ->
    ( match fmsg with
      | Some fmsg -> if msg <> fmsg then
        (error_string (name^" [bad message]") ~text:msg ())
      | None -> ()
    )
    | error -> ouniterror_of_generic_error (name^" [failure]") error

(* raises an error if Assert_failure is NOT raised *)
let eassert_assert_failure name ?(func=(fun()->None)) (todo:unit -> unit) =
  try
    todo();
    ouniterror name ~func;
  with
    | Assert_failure (s, i, j) -> ()
    | error -> ouniterror_of_generic_error (name^" [failure]") error
let catch_unop name (opa:'a option) : 'a =
  match opa with
  | Some a -> a
  | None -> error_string name ~text:"None" ()

let assert_none name opa : unit =
  match opa with
  | Some a -> error_string name ~text:"Some" ()
  | None -> ()

