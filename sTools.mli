module SUtils :
sig
  val char_of_bool : bool -> char
  val bool_of_char : char -> bool
  val string_of_bool : bool -> string
  val bool_of_string : string -> bool
  val pretty_of_bool : bool -> string
  val print_bool : bool -> unit

  val char_0 : int
  val char_9 : int
  val char_A : int
  val char_Z : int
  val char_a : int
  val char_z : int

  val explode: string -> char list
  val implode: char list -> string
  val catmap:  string -> ('a -> string) -> 'a list -> string
  val catmap_list :
    string -> ('a -> string) -> 'a list  -> string
  val catmap_array :
    string -> ('a -> string) -> 'a array -> string

  val index: string -> char -> int option
  val index_from: string -> int -> char -> int option
  val split: char -> string -> string list

  val ntimes : string -> int -> string
  val print_stream : bool list -> unit

  val bool_array_of_string : string -> bool array
  val string_of_bool_array : bool array -> string

  val bool_list_of_string : string -> bool list
  val string_of_bool_list : bool list -> string

  val naive_hexa_of_int  : int -> char
  val hexa_of_int        : int -> char
  val unsafe_hexa_of_int : int -> char

  val char_of_hexa : char -> char
  val int_of_hexa  : char -> int

  val naive_widehexa_of_int  : int -> char
  val widehexa_of_int        : int -> char
  val unsafe_widehexa_of_int : int -> char

  val naive_int_of_widehexa   : char -> int
  val char_of_widehexa        : char -> char
  val int_of_widehexa         : char -> int
  val unsafe_char_of_widehexa : char -> char
  val unsafe_int_of_widehexa  : char -> int

  (* (unsafe_)widehexa_of_sized_int x s
   * with 0 < s <= 4 and 0 <= x < 2^s
   *)
  val widehexa_of_sized_int        : int -> int -> char
  val unsafe_widehexa_of_sized_int : int -> int -> char

  val sized_char_of_widehexa        : char -> int * char
  val sized_int_of_widehexa         : char -> int * int
end

module ToS :
sig
  type 'a t = 'a -> string
  val ref : 'a t -> 'a ref t
  val string : string t
  val option : 'a t -> 'a option t
  val list : ?sep:string -> 'a t -> 'a list t
  val array : ?sep:string -> 'a t -> 'a array t
  val unit : unit t
  val bool : ?t:string -> ?f:string -> bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t
  val ignore : 'a t
    (* let ignore _ = " _ " *)
end

module OfS :
sig
  type 'a t = string -> 'a
  val string : string t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
end

module O3S :
sig
  type 'a t = ('a ToS.t) * ('a OfS.t)
  val string : string t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
end

module ToSTree :
sig
  type 'a t = 'a -> Tree.stree
  val leaf : 'a ToS.t -> 'a t
  val map : ('a -> 'b) -> 'b t -> 'a t

  val string : string t
  val option : 'a t -> 'a option t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t

  val file : string -> Tree.stree list
(** [file target]
    opens the [target] file and parses it as a list of s-tree
 **)
end

module OfSTree :
sig
  type 'a t = Tree.stree -> 'a
  val leaf : 'a OfS.t -> 'a t
  val map  : ('a -> 'b) -> 'a t -> 'b t

  val string : string t
  val option : 'a t -> 'a option t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val unit : unit t
  val bool : bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t

  val file : Tree.stree list -> string -> unit
(** [file streelist target]
    opens the [target] file and writes down the list of s-tree in [streelist]
 **)
  val print : Tree.stree list -> unit
  val pprint : Tree.stree list -> unit

  val to_bytes  : Tree.stree -> bytes
  val to_string : Tree.stree -> string
end

module SOut :
sig
  type 'a t = (string -> unit) -> 'a -> unit

  val string : string t
  val pretty_option : ?some:string -> ?none:string ->
    'a t -> 'a option t
  val option : 'a t -> 'a option t
  val pretty_list :
    ?nil:string -> ?head:string ->
    ?tail:string -> ?sep:string ->
      'a t -> 'a list t
  val list : 'a t -> 'a list t
  val pretty_array :
    ?nil:string -> ?head:string ->
    ?tail:string -> ?sep:string ->
      'a t -> 'a array t
  val array : 'a t -> 'a array t
  val unit : unit t
  val pretty_bool : ?t:string -> ?f:string -> bool t
  val bool : bool t
  val int : int t
  val float : float t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t

  val file : 'a t -> string -> 'a -> unit
  val print : 'a t -> 'a -> unit
end
