open OUnit
open GraphGenLA.Type
open GraphHFT.Type
open GraphHFT

(* compute Component.t *)
let compute_truetwins_alternatives = [|
  (compute_truetwins_naive,
  "compute_truetwins_naive");
  (compute_truetwins_naiveD,
  "compute_truetwins_naiveD");
  (compute_truetwins_naive_pseudoquad,
  "compute_truetwins_naive_pseudoquad");
  (compute_truetwins_pseudolinear,
  "compute_truetwins_pseudolinear");
|]

(* ante-normalized graphs *)
let compute_truetwins_tests = [|
  (GraphHFT_ounit_data.graph_00,
  "GraphHFT_ounit_data.graph_00");
  (GraphHFT_ounit_data.graph_01,
  "GraphHFT_ounit_data.graph_01");
|]

let test_compute_gen hg compute () : unit =
  error_poll_enable := true;
  print_error (fun()-> "hg:"^(ToS.hg hg)^"\n");
  let qt : (_, _) GGLA.Component.qt = compute hg in
  print_error (fun()->"qt.component:"^(GGLA.Component.ToS.t qt.GGLA.Component.component)^"\n");
  let hg' = hg_of_hgl qt.GGLA.Component.qgraph in
  print_error (fun()-> "hg':"^(ToS.hg hg')^"\n");
  error_assert
    "GGLA.TrueTwinsFree.compute_naive hg' vertex_alive"
    (GGLA.TrueTwinsFree.compute_naive hg' vertex_alive);
  ()

let test_compute_indexes i j : unit =
  test_compute_gen
    (compute_truetwins_tests.(i)|>fst)
    (compute_truetwins_alternatives.(j)|>fst)
    ()

(* [DEBUG] *)
(* let _ = test_compute_indexes 0 3 *)

let _ =
  Array.iteri (fun i (hg, hg_name) ->
    Array.iteri (fun j (compute, compute_name) ->
      let tag = OUnit.ToS.("["^(int i)^","^(int j)^"]") in
      push_test
        ("test_"^tag^"_"^compute_name^"_"^hg_name)
        (test_compute_gen hg compute)
      ) compute_truetwins_alternatives
    ) compute_truetwins_tests

(* UNIT TEST : [normalize : hg -> hg] *)
let test_normalize_00 () =
  let g : hg = GraphHFT_ounit_data.graph_00 in
  let ng = normalize g in
  ignore(ng);
  ()

let test_normalize_01 () =
  let g : hg = GraphHFT_ounit_data.graph_01 in
  let ng = normalize g in
  ignore(ng);
  ()

let _ = push_test "test_normalize_00" test_normalize_00

let _ = run_tests()

