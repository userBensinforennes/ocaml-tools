open OUnit
open SetList

let test_nointer_00 () =
  let l1 = [] in
  let l2 = [] in
  let b = nointer l1 l2 in
  error_assert "b ( := nointer l1 l2 ) = true" (b = true);
  ()

let _ = push_test
  "test_nointer_00"
   test_nointer_00

let test_nointer_01 () =
  let l1 = [0] in
  let l2 = [] in
  let b = nointer l1 l2 in
  error_assert "b ( := nointer l1 l2 ) = true" (b = true);
  ()

let _ = push_test
  "test_nointer_01"
   test_nointer_01

let test_nointer_02 () =
  let l1 = [] in
  let l2 = [0] in
  let b = nointer l1 l2 in
  error_assert "b ( := nointer l1 l2 ) = true" (b = true);
  ()

let _ = push_test
  "test_nointer_02"
   test_nointer_02

let test_nointer_03 () =
  let l1 = [0] in
  let l2 = [0] in
  let b = nointer l1 l2 in
  error_assert "b ( := nointer l1 l2 ) = false" (b = false);
  ()

let _ = push_test
  "test_nointer_02"
   test_nointer_02

(* #debug:2020-04-03 *)
let test_nointer_10 () =
  let l1 = [0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19] in
  let l2 = [] in
  let b = nointer l1 l2 in
  error_assert "b ( := nointer l1 l2 ) = true" (b = true);
  ()

let _ = push_test
  "test_nointer_10"
   test_nointer_10

(* #debug:2020-04-06 *)
let test_subset_of_10 () =
  let l1 = [2] in
  let l2 = [0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19] in
  let b = subset_of l1 l2 in
  error_assert
    "b = true"
    (b = true);
  ()
let _ = push_test
  "test_subset_of_10"
   test_subset_of_10

let test_subset_of_11 () =
  let l1 = [2] in
  let l2 = [0; 1; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19] in
  let b = subset_of l1 l2 in
  error_assert
    "b = false"
    (b = false);
  ()

let _ = push_test
  "test_subset_of_11"
   test_subset_of_11

(*  test_01.(i) = (A, B, C) such that : C := A - B /\ B \subset A
    hence :
    - B \subset A
    - C \subset A
    - C = A - B
    - B = A - C
    - A = B + C
    - nointer B C
 *)

let setlist_test_01 = [|
  ([1;2;3;4;5;10;11;12;14;15;16;17], [2], [1;3;4;5;10;11;12;14;15;16;17]);
  ([1;3;4;5;6;7;8;9;10;11;12;14;16;18], [7], [1;3;4;5;6;8;9;10;11;12;14;16;18]);
  ([0;4;5;6;8;9;10;12;13;14;15;16;17;18;19], [0], [4;5;6;8;9;10;12;13;14;15;16;17;18;19]);
  ([1;3;4;5;6;8;9;10;11;12;14;15;16;17;18;19], [1], [3;4;5;6;8;9;10;11;12;14;15;16;17;18;19]);
  ([3;4;5;6;8;9;10;11;12;13;14;15;16;17;18;19], [3;4;5;6;8;9;10;11;12;13;14;15;16;17;18;19], []);
|]

let gen_test_setlist_01 a b c () =
  error_assert
    "subset_of b a"
    (subset_of b a);
  error_assert
    "subset_of c a"
    (subset_of c a);
  let a_minus_b = minus a b in
  error_assert
    "c = a - b"
    (c = a_minus_b);
  let a_minus_c = minus a c in
  error_assert
    "b = a - c"
    (b = a_minus_c);
  let b_union_c = union b c in
  error_assert
    "a = b + c"
    (a = b_union_c);
  error_assert
    "nointer b c"
    (nointer b c);
  ()

let _ =
  Array.iteri (fun i (a, b, c) ->
    let tag = OUnit.ToS.("["^(int i)^"]") in
      push_test
        ("test_setlist_"^tag)
        (gen_test_setlist_01 a b c)
    ) setlist_test_01

let _ = run_tests()
