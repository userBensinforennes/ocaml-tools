open OUnit
open Bicimal

let test_add_single_00 () =
  let i = 4 in
  let b = [3; 3; 4; 4; 4] in
  let ib = add_single ~strict:false i b in
  error_assert
    "ib = [3; 3; 4; 4; 5]"
    (ib = [3; 3; 4; 4; 5]);
  ()

let _ = push_test
  "test_add_single_00"
   test_add_single_00

let test_add_single_01 () =
  let i = 4 in
  let b = [3; 3; 4; 4; 4] in
  Tools.check_print_print := false;
  eassert_assert_failure "[add_single [error:assert_failure]]"
    (fun() -> ignore(add_single i b));
  Tools.check_print_print := true;
  ()

let _ = push_test
  "test_add_single_01"
   test_add_single_01

let test_normalize_00 () =
  let b = [4; 4; 4; 3; 3; 3; 3; 3] in
  let b'_sol = [3; 4; 6] in
  let b' = normalize b in
  error_assert
    "b' = b'_sol"
    (b' = b'_sol);
  ()

let _ = push_test
  "test_normalize_00"
   test_normalize_00

let _ = run_tests()
