open OUnit
open GGLA_domination.Domination
open GraphGenLA.Type
open GraphGenLA.Type
open GraphGenLA.Utils

let test_dominated_00 () =
  let graph  = [|
  {index=0;tag=(); edges=[]};
  {index=1;tag=(); edges=[]} |] in
  let dominated_graph = dominated 0 1 graph in
  error_assert "dominated_graph = true" (dominated_graph = false);
  ()

let _ = push_test "test_dominated_00" test_dominated_00

let test_dominated_01 () =
  let graph = [|
  {index=0;tag=(); edges=[(1,())]};
  {index=1;tag=(); edges=[(0,())]} |] in
  let dominated_graph_0_1 = dominated 0 1 graph in
  let dominated_graph_1_0 = dominated 1 0 graph in
  error_assert "dominated_graph_0_1 = true" (dominated_graph_0_1 = true);
  error_assert "dominated_graph_1_0 = true" (dominated_graph_1_0 = true);
  ()

let _ = push_test "test_dominated_01" test_dominated_01

let test_list_dominated_00 () =
  let graph = [|
  {index=0;tag=(); edges=[(1,())]};
  {index=1;tag=(); edges=[(0,())]} |] in
  let list_dominated_0 = list_dominated 0 graph in
  let list_dominated_1 = list_dominated 1 graph in
  error_assert "list_dominated_0 = [(1,0)]" (list_dominated_0 = [(1,0)]);
  error_assert "list_dominated_1 = [(0,1)]" (list_dominated_1 = [(0,1)]);
  ()

let _ = push_test "test_list_dominated_00" test_list_dominated_00

let test_list_dominated_01 () =
  let graph = [|
  {index=0;tag=(); edges=[(2,())]};
  {index=1;tag=(); edges=[(2,())]};
  {index=2;tag=(); edges=[(0,()); (1,())]} |] in
  let list_dominated_0 = list_dominated 0 graph in
  let list_dominated_1 = list_dominated 1 graph in
  let list_dominated_2 = list_dominated 2 graph in
  error_assert "list_dominated_0 = []" (list_dominated_0 = []);
  error_assert "list_dominated_1 = []" (list_dominated_1 = []);
  error_assert "list_dominated_2 = [(0,2), (1,2)]" (list_dominated_2 = [(0,2); (1,2)]);
  ()

let _ = push_test "test_list_dominated_01" test_list_dominated_01

let test_list_dominated_02 () =
  let graph = [|
  {index=0;tag=(); edges=[(1,()); (2,())]};
  {index=1;tag=(); edges=[(0,()); (2,())]};
  {index=2;tag=(); edges=[(0,()); (1,())]} |] in
  let list_dominated_0 = list_dominated 0 graph in
  let list_dominated_1 = list_dominated 1 graph in
  let list_dominated_2 = list_dominated 2 graph in
  error_assert "list_dominated_0 = [(1,0), (2,0)]" (list_dominated_0 = [(1,0); (2,0)]);
  error_assert "list_dominated_1 = [(0,1), (2,1)]" (list_dominated_1 = [(0,1); (2,1)]);
  error_assert "list_dominated_2 = [(0,2), (1,2)]" (list_dominated_2 = [(0,2); (1,2)]);
  ()

let _ = push_test "test_list_dominated_02" test_list_dominated_02

let test_list_dominated_03 () =
  let graph = [|
  {index=0;tag=(); edges=[(1,()); (2,()); (3,())]};
  {index=1;tag=(); edges=[(0,()); (2,())]};
  {index=2;tag=(); edges=[(0,()); (1,())]};
  {index=3;tag=(); edges=[(0,())]} |] in
  let list_dominated_0 = list_dominated 0 graph in
  let list_dominated_1 = list_dominated 1 graph in
  let list_dominated_2 = list_dominated 2 graph in
  let list_dominated_3 = list_dominated 3 graph in
  error_assert "list_dominated_0 = [(1,0), (2,0), (3,0)]" (list_dominated_0 = [(1,0); (2,0); (3,0)]);
  error_assert "list_dominated_1 = [(2,1)]" (list_dominated_1 = [(2,1)]);
  error_assert "list_dominated_2 = [(1,2)]" (list_dominated_2 = [(1,2)]);
  error_assert "list_dominated_3 = []" (list_dominated_3 = []);
  ()

let _ = push_test "test_list_dominated_03" test_list_dominated_03

let test_give_dominated_00 () =
  let graph = [| |] in
  let list_dominated = give_dominated graph in
  error_assert "list_dominated = []" (list_dominated = []);
  ()

let _ = push_test "test_give_dominated_00" test_give_dominated_00

let test_give_dominated_01 () =
  let graph = [|
  {index=0;tag=(); edges=[(2,())]};
  {index=1;tag=(); edges=[(2,())]};
  {index=2;tag=(); edges=[(0,()); (1,())]} |] in
  let list_dominated = give_dominated graph in
  error_assert "list_dominated = [(0,2), (1,2)]" (list_dominated = [(0,2); (1,2)]);
  ()

let _ = push_test "test_give_dominated_01" test_give_dominated_01

let test_give_dominated_02 () =
  let graph = [|
  {index=0;tag=(); edges=[(1,()); (2,())]};
  {index=1;tag=(); edges=[(0,()); (2,())]};
  {index=2;tag=(); edges=[(0,()); (1,())]} |] in
  let list_dominated = give_dominated graph in
  error_assert "list_dominated = [(1,0); (2,0); (0,1), (2,1); (0,2), (1,2)]"
            (list_dominated = [(1,0); (2,0); (0,1); (2,1); (0,2); (1,2)]);
  ()

let _ = push_test "test_give_dominated_02" test_give_dominated_02

let test_give_dominated_03 () =
  let graph = [|
  {index=0;tag=(); edges=[(1,()); (2,()); (3,())]};
  {index=1;tag=(); edges=[(0,()); (2,())]};
  {index=2;tag=(); edges=[(0,()); (1,())]};
  {index=3;tag=(); edges=[(0,())]} |] in
  let list_dominated = give_dominated graph in
  error_assert "list_dominated = [(1,0), (2,0), (3,0), (2,1), (1,2)]"
                (list_dominated = [(1,0); (2,0); (3,0); (2,1); (1,2)]);
  ()

let _ = push_test "test_give_dominated_03" test_give_dominated_03

let _ = run_tests()
