type entry = {
  mutable ncall : int;
  mutable ttime : float;
}

type entry_list = {
  mutable el_ncall : int;
  mutable el_ttime : float;
  mutable el_times : float list;
}

type t = {
  prefix : string;
  table  : (string, entry) Hashtbl.t;
  el_table  : (string, entry_list) Hashtbl.t;
  mutable active : int;
  mutable total_other : float;
  mutable start_other : float;
}

let newman ?(prefix="") () : t = {
  prefix;
  table  = Hashtbl.create 128;
  el_table  = Hashtbl.create 128;
  active = 0;
  total_other = 0. ;
  start_other = Tools.utime() ;
}

let get_entry t s : entry =
  try
    Hashtbl.find t.table s
  with Not_found -> (
    let entry = {ncall = 0; ttime = 0.} in
    Hashtbl.add t.table s entry;
    entry
  )

let get_entry_list t s : entry_list =
  try
    Hashtbl.find t.el_table s
  with Not_found -> (
    let entry = {el_ncall = 0; el_ttime = 0.; el_times = []} in
    Hashtbl.add t.el_table s entry;
    entry
  )

let stop_other (t:t) : unit =
  (if t.active = 0
  then (
    (* [stop_other] procedure *)
    let delta = Tools.utime() -. t.start_other in
    t.total_other <- t.total_other +. delta
  ));
  t.active <- succ t.active;
  ()

let start_other (t:t) : unit =
  assert(t.active >= 1);
  t.active <- pred t.active;
  (if t.active = 0
  then (
    (* [start_other] procedure *)
    t.start_other <- Tools.utime()
  ));
  ()

let time_start (t:t) (s:string) =
  stop_other t;
  let entry = get_entry t s in
  let start = Tools.utime() in
  entry.ncall <- succ entry.ncall;
  (fun () ->
    let delta = Tools.utime() -. start in
    entry.ttime <- entry.ttime +. delta;
    start_other t;
  )

let time_start_el (t:t) (s:string) =
  stop_other t;
  let entry = get_entry_list t s in
  let start = Tools.utime() in
  entry.el_ncall <- succ entry.el_ncall;
  (fun () ->
    let delta = Tools.utime() -. start in
    entry.el_ttime <- entry.el_ttime +. delta;
    entry.el_times <- delta :: entry.el_times;
    start_other t;
  )

let profile (t:t) (s:string) (f:'a -> 'b) (a:'a) : 'b =
  let time_stop = time_start t s in
  let b = f a in
  time_stop();
  b

let uprofile (t:t) (s:string) (f:unit -> 'b) : 'b = profile t s f ()

let print_table (t:t) : unit =
  if Hashtbl.length t.table > 0 then (
    print_endline(t.prefix^" #table");
    Hashtbl.iter (fun k v ->
      print_string (t.prefix^k^" : ncall : "); print_int   v.ncall; print_newline();
      print_string (t.prefix^k^" : ttime : "); print_float v.ttime; print_newline();
      ()
    ) t.table
  )

let print_el_table (t:t) : unit =
  if Hashtbl.length t.el_table  > 0 then (
    print_endline(t.prefix^" #el-table");
    Hashtbl.iter (fun k v ->
      print_string (t.prefix^k^" : ncall : "); print_int   v.el_ncall; print_newline();
      print_string (t.prefix^k^" : ttime : "); print_float v.el_ttime; print_newline();
      print_string (t.prefix^k^" : times : ");
        print_string "[";
        List.iter
          (fun f -> print_float f; print_string"; ")
          (List.rev v.el_times);
        print_string "]";
        print_newline();
      ()
    ) t.el_table
  )

let print (t:t) : unit =
  print_string (t.prefix^"other"^" : ttime : "); print_float t.total_other; print_newline();
  print_table t;
  print_el_table t;
  ()

let default : t = newman ~prefix:"[OProfile] " ()
