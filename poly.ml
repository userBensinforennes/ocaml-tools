type 'a v0 = unit
type 'a v1 = 'a
type 'a v2 = 'a * 'a
type 'a v3 = 'a * 'a * 'a
type 'a v4 = 'a * 'a * 'a * 'a

let v1_max x = x
let v2_max (x0, x1) = max x0 x1
let v3_max (x0, x1, x2) = max x0 (max x1 x2)
let v4_max (x0, x1, x2, x3) = max (max x0 x1) (max x2 x3)

let v0_rev (():'a v0) : 'a v0 = ()
let v1_rev ( x:'a v1) : 'a v1 = x
let v2_rev ((x0, x1):'a v2) : 'a v2 = (x1, x0)
let v3_rev ((x0, x1, x2):'a v3) : 'a v3 = (x2, x1, x0)
let v4_rev ((x0, x1, x2, x3):'a v4) : 'a v4 = (x3, x2, x1, x0)

let v1_get = function
  | 0 -> (fun x -> x)
  | _ -> failwith "[ocaml-tools/poly:v1_get] invalid index"

let v2_get = function
  | 0 -> (fun(x0, x1) -> x0)
  | 1 -> (fun(x0, x1) -> x1)
  | _ -> failwith "[ocaml-tools/poly:v2_get] invalid index"

let v3_get = function
  | 0 -> (fun(x0, x1, x2) -> x0)
  | 1 -> (fun(x0, x1, x2) -> x1)
  | 2 -> (fun(x0, x1, x2) -> x2)
  | _ -> failwith "[ocaml-tools/poly:v3_get] invalid index"

let v4_get = function
  | 0 -> (fun(x0, x1, x2, x3) -> x0)
  | 1 -> (fun(x0, x1, x2, x3) -> x1)
  | 2 -> (fun(x0, x1, x2, x3) -> x2)
  | 3 -> (fun(x0, x1, x2, x3) -> x3)
  | _ -> failwith "[ocaml-tools/poly:v4_get] invalid index"

let v1_get' x i = v1_get i x
let v2_get' x i = v2_get i x
let v3_get' x i = v3_get i x
let v4_get' x i = v4_get i x

let v1_argmax x = 0
let v2_argmax (x0, x1) =
  if x0 >= x1 then 0 else 1
let v3_argmax (x0, x1, x2) =
  if x0 >= x1
  then (if x0 >= x2 then 0 else 2)
  else (if x1 >= x2 then 1 else 2)
let v4_argmax (x0, x1, x2, x3) =
  if x0 >= x1
  then if x0 >= x2
    then if x0 >= x3 then 0 else 3
    else if x2 >= x3 then 2 else 3
  else if x1 >= x2
    then if x1 >= x3 then 1 else 3
    else if x2 >= x3 then 2 else 3

let v1_argmin x = 0
let v2_argmin (x0, x1) =
  if x0 <= x1 then 0 else 1
let v3_argmin (x0, x1, x2) =
  if x0 <= x1
  then (if x0 <= x2 then 0 else 2)
  else (if x1 <= x2 then 1 else 2)
let v4_argmin (x0, x1, x2, x3) =
  if x0 <= x1
  then if x0 <= x2
    then if x0 <= x3 then 0 else 3
    else if x2 <= x3 then 2 else 3
  else if x1 <= x2
    then if x1 <= x3 then 1 else 3
    else if x2 <= x3 then 2 else 3
let v1_0 x = x

let v2_0 (x0, x1) = x0
let v2_1 (x0, x1) = x1

let v3_0 (x0, x1, x2) = x0
let v3_1 (x0, x1, x2) = x1
let v3_2 (x0, x1, x2) = x2

let v4_0 (x0, x1, x2, x3) = x0
let v4_1 (x0, x1, x2, x3) = x1
let v4_2 (x0, x1, x2, x3) = x2
let v4_3 (x0, x1, x2, x3) = x3

let v0_map  (f: 'a -> 'b)       (():'a v0)            : 'b v0 = ()
let v0_map2 (f: 'a -> 'b -> 'c) (():'a v0) (():'b v0) : 'c v0 = ()

let v1_map  (f: 'a -> 'b)       ( a:'a v1)            : 'b v1 = f a
let v1_map2 (f: 'a -> 'b -> 'c) ( a:'a v1) ( b:'b v1) : 'c v1 = f a b

let v2_map  (f: 'a -> 'b)       ((a0, a1):'a v2)                  : 'b v2 =
  (f a0   , f a1   )
let v2_map2 (f: 'a -> 'b -> 'c) ((a0, a1):'a v2) ((b0, b1):'b v2) : 'c v2 =
  (f a0 b0, f a1 b1)

let v3_map  (f: 'a -> 'b)       ((a0, a1, a2):'a v3)                      : 'b v3 =
  (f a0   , f a1   , f a2   )
let v3_map2 (f: 'a -> 'b -> 'c) ((a0, a1, a2):'a v3) ((b0, b1, b2):'b v3) : 'c v3 =
  (f a0 b0, f a1 b1, f a2 b2)

let v4_map  (f: 'a -> 'b)       ((a0, a1, a2, a3):'a v4)                          : 'b v4 =
  (f a0   , f a1   , f a2   , f a3   )
let v4_map2 (f: 'a -> 'b -> 'c) ((a0, a1, a2, a3):'a v4) ((b0, b1, b2, b3):'b v4) : 'c v4 =
  (f a0 b0, f a1 b1, f a2 b2, f a3 b3)

let v0_map_X_y f x y = v0_map (f x) y
let v1_map_X_y f x y = v1_map (f x) y
let v2_map_X_y f x y = v2_map (f x) y
let v3_map_X_y f x y = v3_map (f x) y
let v4_map_X_y f x y = v4_map (f x) y

let v0_map_x_Y f x y = v0_map (fun x' -> f x' y) x
let v1_map_x_Y f x y = v1_map (fun x' -> f x' y) x
let v2_map_x_Y f x y = v2_map (fun x' -> f x' y) x
let v3_map_x_Y f x y = v3_map (fun x' -> f x' y) x
let v4_map_x_Y f x y = v4_map (fun x' -> f x' y) x

let v0_perm (():'a v0) (():int v0) : 'a v0 = ()
let v1_perm ( x:'a v1) ( i:int v1) : 'a v1 = assert(i=0); x
let v2_perm ( x:'a v2) ( i:int v2) : 'a v2 = v2_map (v2_get' x) i
let v3_perm ( x:'a v3) ( i:int v3) : 'a v3 = v3_map (v3_get' x) i
let v4_perm ( x:'a v4) ( i:int v4) : 'a v4 = v4_map (v4_get' x) i

let v0_sort (():'a v0) : 'a v0 = ()
let v1_sort ( x:'a v1) : 'a v1 = x
let v2_sort ((x0, x1):'a v2) : 'a v2 =
  if x0 >= x1
  then (x0, x1)
  else (x1, x0)
let v3_sort ((x0, x1, x2): 'a v3) : 'a v3 =
  if x0 >= x1
  then if x0 >= x2
    then if x1 >= x2
      then (x0, x1, x2)
      else (x0, x2, x1)
    else (x2, x0, x1)
  else if x0 >= x2
    then (x1, x0, x2)
    else if x1 >= x2
      then (x1, x2, x0)
      else (x2, x1, x0)

let v0_cosort ((): _ v0) (():'a v0) : 'a v0 = ()
let v1_cosort ( _: _ v1) ( y:'a v1) : 'a v1 = y
let v2_cosort ((x0, x1):_ v2) ((y0, y1): 'a v2) : 'a v2 =
  if x0 >= x1
  then (y0, y1)
  else (y1, y0)
let v3_cosort ((x0, x1, x2): _ v3) ((y0, y1, y2): 'a v3) : 'a v3 =
  if x0 >= x1
  then if x0 >= x2
    then if x1 >= x2
      then (y0, y1, y2)
      else (y0, y2, y1)
    else (y2, y0, y1)
  else if x0 >= x2
    then (y1, y0, y2)
    else if x1 >= x2
      then (y1, y2, y0)
      else (y2, y1, y0)

type i0 = int v0
type i1 = int v1
type i2 = int v2
type i3 = int v3
type i4 = int v4

let i0_add_x_y = v0_map2 (+)
let i1_add_x_y = v1_map2 (+)
let i2_add_x_y = v2_map2 (+)
let i3_add_x_y = v3_map2 (+)
let i4_add_x_y = v4_map2 (+)

let i0_add_X_y = v0_map_X_y (+)
let i1_add_X_y = v1_map_X_y (+)
let i2_add_X_y = v2_map_X_y (+)
let i3_add_X_y = v3_map_X_y (+)
let i4_add_X_y = v4_map_X_y (+)

let i0_add_x_Y = v0_map_x_Y (+)
let i1_add_x_Y = v1_map_x_Y (+)
let i2_add_x_Y = v2_map_x_Y (+)
let i3_add_x_Y = v3_map_x_Y (+)
let i4_add_x_Y = v4_map_x_Y (+)

let i0_sub_x_y = v0_map2 (-)
let i1_sub_x_y = v1_map2 (-)
let i2_sub_x_y = v2_map2 (-)
let i3_sub_x_y = v3_map2 (-)
let i4_sub_x_y = v4_map2 (-)

let i0_sub_X_y = v0_map_X_y (-)
let i1_sub_X_y = v1_map_X_y (-)
let i2_sub_X_y = v2_map_X_y (-)
let i3_sub_X_y = v3_map_X_y (-)
let i4_sub_X_y = v4_map_X_y (-)

let i0_sub_x_Y = v0_map_x_Y (-)
let i1_sub_x_Y = v1_map_x_Y (-)
let i2_sub_x_Y = v2_map_x_Y (-)
let i3_sub_x_Y = v3_map_x_Y (-)
let i4_sub_x_Y = v4_map_x_Y (-)

let i0_mul_x_y = v0_map2 ( * )
let i1_mul_x_y = v1_map2 ( * )
let i2_mul_x_y = v2_map2 ( * )
let i3_mul_x_y = v3_map2 ( * )
let i4_mul_x_y = v4_map2 ( * )

let i0_mul_X_y = v0_map_X_y ( * )
let i1_mul_X_y = v1_map_X_y ( * )
let i2_mul_X_y = v2_map_X_y ( * )
let i3_mul_X_y = v3_map_X_y ( * )
let i4_mul_X_y = v4_map_X_y ( * )

let i0_mul_x_Y = v0_map_x_Y ( * )
let i1_mul_x_Y = v1_map_x_Y ( * )
let i2_mul_x_Y = v2_map_x_Y ( * )
let i3_mul_x_Y = v3_map_x_Y ( * )
let i4_mul_x_Y = v4_map_x_Y ( * )

let i0_div_x_y = v0_map2 (/)
let i1_div_x_y = v1_map2 (/)
let i2_div_x_y = v2_map2 (/)
let i3_div_x_y = v3_map2 (/)
let i4_div_x_y = v4_map2 (/)

let i0_div_X_y = v0_map_X_y (/)
let i1_div_X_y = v1_map_X_y (/)
let i2_div_X_y = v2_map_X_y (/)
let i3_div_X_y = v3_map_X_y (/)
let i4_div_X_y = v4_map_X_y (/)

let i0_div_x_Y = v0_map_x_Y (/)
let i1_div_x_Y = v1_map_x_Y (/)
let i2_div_x_Y = v2_map_x_Y (/)
let i3_div_x_Y = v3_map_x_Y (/)
let i4_div_x_Y = v4_map_x_Y (/)

type f0 = float v0
type f1 = float v1
type f2 = float v2
type f3 = float v3
type f4 = float v4

let f0_add_x_y = v0_map2 (+.)
let f1_add_x_y = v1_map2 (+.)
let f2_add_x_y = v2_map2 (+.)
let f3_add_x_y = v3_map2 (+.)
let f4_add_x_y = v4_map2 (+.)

let f0_add_X_y = v0_map_X_y (+.)
let f1_add_X_y = v1_map_X_y (+.)
let f2_add_X_y = v2_map_X_y (+.)
let f3_add_X_y = v3_map_X_y (+.)
let f4_add_X_y = v4_map_X_y (+.)

let f0_add_x_Y = v0_map_x_Y (+.)
let f1_add_x_Y = v1_map_x_Y (+.)
let f2_add_x_Y = v2_map_x_Y (+.)
let f3_add_x_Y = v3_map_x_Y (+.)
let f4_add_x_Y = v4_map_x_Y (+.)

let f0_sub_x_y = v0_map2 (-.)
let f1_sub_x_y = v1_map2 (-.)
let f2_sub_x_y = v2_map2 (-.)
let f3_sub_x_y = v3_map2 (-.)
let f4_sub_x_y = v4_map2 (-.)

let f0_sub_X_y = v0_map_X_y (-.)
let f1_sub_X_y = v1_map_X_y (-.)
let f2_sub_X_y = v2_map_X_y (-.)
let f3_sub_X_y = v3_map_X_y (-.)
let f4_sub_X_y = v4_map_X_y (-.)

let f0_sub_x_Y = v0_map_x_Y (-.)
let f1_sub_x_Y = v1_map_x_Y (-.)
let f2_sub_x_Y = v2_map_x_Y (-.)
let f3_sub_x_Y = v3_map_x_Y (-.)
let f4_sub_x_Y = v4_map_x_Y (-.)

let f0_mul_x_y = v0_map2 ( *. )
let f1_mul_x_y = v1_map2 ( *. )
let f2_mul_x_y = v2_map2 ( *. )
let f3_mul_x_y = v3_map2 ( *. )
let f4_mul_x_y = v4_map2 ( *. )

(* float / fX *)
let f0_mul_X_y = v0_map_X_y ( *. )
let f1_mul_X_y = v1_map_X_y ( *. )
let f2_mul_X_y = v2_map_X_y ( *. )
let f3_mul_X_y = v3_map_X_y ( *. )
let f4_mul_X_y = v4_map_X_y ( *. )

(* fX / float *)
let f0_mul_x_Y = v0_map_x_Y ( *. )
let f1_mul_x_Y = v1_map_x_Y ( *. )
let f2_mul_x_Y = v2_map_x_Y ( *. )
let f3_mul_x_Y = v3_map_x_Y ( *. )
let f4_mul_x_Y = v4_map_x_Y ( *. )

(* fX / fX *)
let f0_div_x_y = v0_map2 (/.)
let f1_div_x_y = v1_map2 (/.)
let f2_div_x_y = v2_map2 (/.)
let f3_div_x_y = v3_map2 (/.)
let f4_div_x_y = v4_map2 (/.)

(* float / fX *)
let f0_div_X_y = v0_map_X_y (/.)
let f1_div_X_y = v1_map_X_y (/.)
let f2_div_X_y = v2_map_X_y (/.)
let f3_div_X_y = v3_map_X_y (/.)
let f4_div_X_y = v4_map_X_y (/.)

(* fX / float *)
let f0_div_x_Y = v0_map_x_Y (/.)
let f1_div_x_Y = v1_map_x_Y (/.)
let f2_div_x_Y = v2_map_x_Y (/.)
let f3_div_x_Y = v3_map_x_Y (/.)
let f4_div_x_Y = v4_map_x_Y (/.)

(* int_of_float *)
let i0_of_f0 = v0_map int_of_float
let i1_of_f1 = v1_map int_of_float
let i2_of_f2 = v2_map int_of_float
let i3_of_f3 = v3_map int_of_float
let i4_of_f4 = v4_map int_of_float

(* float_of_int *)
let f0_of_i0 = v0_map float_of_int
let f1_of_i1 = v1_map float_of_int
let f2_of_i2 = v2_map float_of_int
let f3_of_i3 = v3_map float_of_int
let f4_of_i4 = v4_map float_of_int

(* reduction *)
let v0_fold_left ((+):'a->'b->'a) (x0:'a) ((): 'b v0) :'a =
  x0
let v1_fold_left ((+):'a->'b->'a) (x0:'a) ( y0: 'b v1) :'a =
  x0+y0
let v2_fold_left ((+):'a->'b->'a) (x0:'a) ((y0, y1): 'b v2) :'a =
  x0+y0+y1
let v3_fold_left ((+):'a->'b->'a) (x0:'a) ((y0, y1, y2): 'b v3) :'a =
  x0+y0+y1+y2
let v4_fold_left ((+):'a->'b->'a) (x0:'a) ((y0, y1, y2, y3): 'b v4) :'a =
  x0+y0+y1+y2+y3

let i0_fold_add = v0_fold_left (+) 0
let i1_fold_add = v1_fold_left (+) 0
let i2_fold_add = v2_fold_left (+) 0
let i3_fold_add = v3_fold_left (+) 0
let i4_fold_add = v4_fold_left (+) 0

let i0_fold_mul = v0_fold_left ( * ) 0
let i1_fold_mul = v1_fold_left ( * ) 0
let i2_fold_mul = v2_fold_left ( * ) 0
let i3_fold_mul = v3_fold_left ( * ) 0
let i4_fold_mul = v4_fold_left ( * ) 0

let f0_fold_add = v0_fold_left (+.) 0.
let f1_fold_add = v1_fold_left (+.) 0.
let f2_fold_add = v2_fold_left (+.) 0.
let f3_fold_add = v3_fold_left (+.) 0.
let f4_fold_add = v4_fold_left (+.) 0.

let f0_fold_mul = v0_fold_left ( *. ) 0.
let f1_fold_mul = v1_fold_left ( *. ) 0.
let f2_fold_mul = v2_fold_left ( *. ) 0.
let f3_fold_mul = v3_fold_left ( *. ) 0.
let f4_fold_mul = v4_fold_left ( *. ) 0.

(* integer scalar product *)
let i0_sp x y = i0_add_x_y x y |> i0_fold_add
let i1_sp x y = i1_add_x_y x y |> i1_fold_add
let i2_sp x y = i2_add_x_y x y |> i2_fold_add
let i3_sp x y = i3_add_x_y x y |> i3_fold_add
let i4_sp x y = i4_add_x_y x y |> i4_fold_add

(* float scalar product *)
let f0_sp x y = f0_add_x_y x y |> f0_fold_add
let f1_sp x y = f1_add_x_y x y |> f1_fold_add
let f2_sp x y = f2_add_x_y x y |> f2_fold_add
let f3_sp x y = f3_add_x_y x y |> f3_fold_add
let f4_sp x y = f4_add_x_y x y |> f4_fold_add

(* vectorial product *)
let i2_vp (x0, x1) (y0, y1) =
  (x0*y1)-(x1*y0)
let i3_vp (x0, x1, x2) (y0, y1, y2) =
  ((x1*y2)-(x2*y1), (x2*y0)-(x0*y2), (x0*y1)-(x1*y0))
let f2_vp (x0, x1) (y0, y1) =
  (x0*.y1)-.(x1*.y0)
let f3_vp (x0, x1, x2) (y0, y1, y2) =
  ((x1*.y2)-.(x2*.y1), (x2*.y0)-.(x0*.y2), (x0*.y1)-.(x1*.y0))

type ('t0, 't1) c2 =
  | C2_0 of 't0
  | C2_1 of 't1

type ('t0, 't1, 't2) c3 =
  | C3_0 of 't0
  | C3_1 of 't1
  | C3_2 of 't2

type ('t0, 't1, 't2, 't3) c4 =
  | C4_0 of 't0
  | C4_1 of 't1
  | C4_2 of 't2
  | C4_3 of 't3

type 'a o1 =
  | O1_0
  | O1_1 of 'a

type 'a o2 =
  | O2_0
  | O2_1 of 'a
  | O2_2 of 'a * 'a

