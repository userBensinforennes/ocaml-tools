open Tree

module ToS =
struct
  open STools.ToS
  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree) : string =
    let rec aux = function
    | ALeaf  b -> "ALeaf ("^(fb b)^")"
    | ANode (a, l) -> "ANode "^((fa * (list aux)) (a, l))
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl

  let gnext (fa:'a t) (fb:'b t) : ('a, 'b) gnext -> string =
    function
    | GLeaf a -> "GLeaf ("^(fa a)^")"
    | GLink b -> "GLink ("^(fb b)^")"
end

module ToSTree =
struct
  open STools.ToSTree
  let atree (fa:'a t) (fb:'b t) (t:('a, 'b) atree)  =
    let rec aux : ('a, 'b) atree t = function
    | ALeaf  b ->
      Node [Leaf "ALeaf"; fb b]
    | ANode (a, l) ->
      Node [Leaf "ANode"; (fa * (list aux)) (a, l)]
    in aux t

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl
end

module OfSTree =
struct
  open STools.OfSTree
  let atree (fa:'a t) (fb:'b t) stree =
    let rec aux = function
    | Node[Leaf "ALeaf"; data] ->
      let b = fb data in
      ALeaf b
    | Node[Leaf "ANode"; data] ->
      let a, l = (fa * (list aux)) data in
      ANode (a, l)
    | _ -> assert false
    in aux stree

  let atrees fa fb tl = list (atree fa fb) tl

  let aatree fa t = atree fa fa t
  let aatrees fa tl = atrees fa fa tl
end

module ToBStream =
struct
  open BTools.ToBStream

  let gnext (aa:'a t) (bb:'b t)
    (cha:Channel.t) (next:('a, 'b) gnext) : unit =
    match next with
    | GLeaf a ->
    (
      bool cha false;
      aa cha a;
    )
    | GLink b ->
    (
      bool cha true;
      bb cha b;
    )
end

module OfBStream =
struct
  open BTools.OfBStream

  let gnext (aa:'a t) (bb:'b t)
    (cha:Channel.t) : ('a, 'b) gnext =
    match bool cha with
    | false -> GLeaf (aa cha)
    | true  -> GLink (bb cha)
end

let rec aatree_normalized = function
  | ALeaf _ -> true
  | ANode (_, []) -> false
  | ANode (_, tl) -> aatrees_normalized tl
and    aatrees_normalized (tl:_ aatrees) =
  List.for_all aatree_normalized tl

let rec aatree_normalize = function
  | ALeaf x
  | ANode (x, []) -> ALeaf x
  | ANode (x, tl) -> ANode (x, aatrees_normalize tl)
and    aatrees_normalize (tl: _ aatrees) =
  Tools.map aatree_normalize tl

(* @santiago.bautista *)
let aatree_of_ialist_insert ((i0, e0): int * 'a) (l: (int*('a aatree)) list) : (int*('a aatree)) list =
  let heads, tails =
    MyList.find_onehash_prefix ~p:(fun (i, _) -> i > i0) fst l
  in
  (i0, ANode(e0, Tools.map snd heads))::tails

(* @santiago.bautista *)
let aatree_of_ialist (l:(int * 'a) list) : 'a aatrees =
  let rec aux (left: (int * 'a) list) (l: (int*('a aatree)) list) : _ aatrees =
    match left with
    | [] ->
    ( (* finalizer *)
      assert(MyList.onehash fst l);
      Tools.map snd l
    )
    | line::left -> aux left (aatree_of_ialist_insert line l)
  in
  aatrees_normalize (aux (List.rev l) [])

let unGLeaf = function
  | GLeaf lf -> lf
  | GLink _  -> assert false
let unGLink = function
  | GLeaf _  -> assert false
  | GLink lk -> lk

let ungnext = function GLeaf x | GLink x -> x
