(*
  All Right Reserved

  Copyright (c) 2020
    Joan Thibault
      - joan.thibault@irisa.fr
    Clara Begue
      - clara.begue@ens-rennes.fr
*)

open GraphGenLA.Type
open GraphGenLA.Utils
open GGLA_utils

module Domination =
struct
  (* @JoanThibault #alt [dominated]
     Time Complexity : O(|V|)
   *)

  (*[dominated node1 node2 graph] tests if the node [node1] is dominated by
  [node2] in [graph]
      Time  : O(D) where D is the max of node degree in [graph]
      Stack : O(1)
      Heap  : O(D)
  *)
  let dominated (node1:int) (node2:int) (graph:('v, 'e) graph) : bool =
    let neib1 = vertex_list_neighbors graph [node1] in
    let neib2 = vertex_list_neighbors graph [node2] in
    SetList.subset_of neib1 neib2

  (*[list_dominated node graph] returns the int list of nodes dominated by
  [node] in [graph]
      Time  : O(D^2) where D is the max of node degree in [graph]
      Stack : O(1)
      Heap  : O(D)
  *)
  let list_dominated (node:int) (graph:('v, 'e) graph) : (int*int) list =
    let rec aux (couples:(int*int) list) (graph:('v, 'e) graph) (neib:int list) =
      match neib with
        | [] -> List.rev couples
        | h::t -> if dominated h node graph then
                    aux ((h, node)::couples) graph t
                  else aux couples graph t
    in aux [] graph (vertex_list_adjacent graph [node])

  (*[give_dominated graph] returns the list of domination couples (n1, n2)
    (n1, n2) means n1 is dominated by n2 in [graph]
      Time  : O(|V|^D^2) where D is the max of node degree in [graph]
      Stack : O(1)
      Heap  : O(|V|)
  *)
  let give_dominated (graph:('v, 'e) graph) : (int*int) list =
    let rec aux (couples:(int*int) list list) (graph:('v, 'e) graph) (i:int) =
      if (i = Array.length graph) then List.flatten (List.rev couples)
      else (aux ((list_dominated i graph)::couples) graph (i+1))
    in aux [] graph 0
end
