(** signature imported from : https://github.com/harnold/ocaml-base.git]
    the signature is meant to be integrated into ocaml-tools
    the implementation is based on the the Map module of the standard library
 **)

(* [definition of Map.OrderedType]
module type OrderedType =
  sig
    type t
    val compare : t -> t -> int
    (* a total order over keys, such as Stdlib.compares *)
  end
 *)

(* TODO : add a Stdlib-friendly version of the interface with type ('k, 'v) t *)
(* TODO : add a module-less version of the interface, very handy in some cases *)

module type S =
(* read the .mli file for signature description *)
sig
  type key

  type 'v t (* 'v being the type of values *)

  val empty : unit -> 'v t
  val length: 'v t -> int
  val is_empty: 'v t -> bool
  val add: 'v t -> key -> 'v -> unit
(*
  val mem: 'v t -> 'v -> bool
  val get_key: 'v t -> 'v -> key
 *)
  val first: 'v t -> key * 'v
  val remove_first: 'v t -> unit
  val pull_first : 'v t -> key * 'v
  val pull_first_opt : 'v t -> (key * 'v) option
  val pull_firsts : 'v t -> key * ('v list)
(*
  val remove: 'v t -> 'v -> unit
 *)
  val clear: 'v t -> unit
  val shrink : 'v t -> key -> unit

(*
  val reorder_up: 'a t -> 'a -> unit
  val reorder_down: 'a t -> 'a -> unit
*)
  val to_stree :
    (key -> Tree.stree) ->
    ('v  -> Tree.stree) ->
    'v t -> Tree.stree
  val of_stree :
    (Tree.stree -> key) ->
    (Tree.stree -> 'v ) ->
    Tree.stree -> 'v t

  val iter: (key -> 'a -> unit) -> 'a t -> unit
  (** [iter f m] applies [f] to all bindings in map [m].
       [f] receives the key as first argument, and the associated value
       as second argument.  The bindings are passed to [f] in increasing
       order with respect to the ordering over the type of the keys.
   **)

(* TODO : add serialization/conversion interface *)
end

module MakeMax(Ord:Map.OrderedType) : S
  with type key = Ord.t =
struct
  type key = Ord.t

  module MAP = Map.Make(Ord)

  type 'v t = 'v list MAP.t ref (* 'v being the type of letues *)
  let empty () = ref MAP.empty

  let length pq : int =
    let total = ref 0 in
    MAP.iter (fun k v -> total := List.length v + !total) !pq;
    !total

  let nb_bag pq : int = MAP.cardinal !pq

  let is_empty q : bool = MAP.is_empty !q
  let add_aux v = function
    | None -> Some [v]
    | Some l -> Some (v::l)
  let add q k v : unit =
    q := MAP.update k (add_aux v) !q
(*
  let mem : 'v t -> 'v -> bool
  let get_key: 'v t -> 'v -> key
 *)
  let first (q:'v t) : key * 'v =
    match MAP.max_binding !q with
    | _, [] -> assert false
    | k, (h::_) -> (k, h)

  let remove_first (q:'v t) : unit =
    let k, l = MAP.max_binding !q in
    let q' = match l with
      | [] -> assert false
      | _::[] -> MAP.remove k !q
      | _::l' -> MAP.add k l' !q
    in q := q'; ()

  let pull_first (q:'v t) : key * 'v =
    let k, l = MAP.max_binding !q in
    let v, q' = match l with
      | [] -> assert false
      | v::[] -> (v, MAP.remove k !q)
      | v::l' -> (v, MAP.add k l' !q)
    in
    q := q'; (k, v)

  let pull_first_opt (q:'v t) : (key * 'v) option =
    try
      Some(pull_first q)
    with
      | Not_found -> None

  let pull_firsts (q:'v t) : key * ('v list) =
    let k, l as res = MAP.max_binding !q in
    q := MAP.remove k !q; res
(*
  let remove: 'v t -> 'v -> unit
 *)
  let clear q : unit =
    q := MAP.empty
  let shrink q k : unit=
    let _, _, high = MAP.split k !q in
    q := high

(*
  let reorder_up: 'a t -> 'a -> unit
  let reorder_down: 'a t -> 'a -> unit
*)

(* TODO : add serialization/conversion interface *)
(* type 'v t = 'v list MAP.t ref (* 'v being the type of letues *) *)
  let to_stree
    (kdump:key -> Tree.stree)
    (vdump:'v -> Tree.stree)
    (pq : 'v t) : Tree.stree=
    let l = ref [] in
    MAP.iter (fun k vl ->
      l := (Tree.Node ((kdump k)::(Tools.map vdump vl))):: !l
    ) !pq;
    Tree.Node !l

  let of_stree kload vload stree =
    let pq = empty() in
    match stree with
    | Tree.Leaf _ -> assert false
    | Tree.Node l -> (
      List.iter (function
        | Tree.Node (k'::vl') -> (
          let k = kload k' in
          let vl = Tools.map vload vl' in
          pq := MAP.add k vl !pq;
        )
        | _ -> assert false) l
    );
    pq

  let iter (p:(key -> 'a -> unit)) (t:'a t) : unit =
    MAP.iter (fun k vl -> List.iter (p k) vl) !t
end
(* version where piority is defined according to OT.compare, i.e. :
   - values with higher keys have higher priority
   - *first* return value(s) with maximal key
 *)

module MakeMin(Ord:Map.OrderedType) : S
  with type key = Ord.t =
struct
  include MakeMax
    (struct
      type t = Ord.t
      let compare x y = -(Ord.compare x y)
     end)
end
(* version where piority is defined according to OT.compare, i.e. :
   - values with lower keys have higher priority
   - *first* return value(s) with minimal key
 *)

(* TODO : add a version with user-transparent compression of elements of values *)
