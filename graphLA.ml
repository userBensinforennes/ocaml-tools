(* All Right Reserved

   Copyright (c) 2020 Joan Thibault
*)
open STools
open BTools

(* Data Structure  *)
(* G = (V, E) *)
type graph = {
  nodes : int list;
  (* list of currently alive vertices *)
  (* nodes is [SetList.sorted] *)
  edges : int list array;
  (* array of adjacency list *)
  (* each [int list] is [SetList.sorted] *)
  (* if i not in graph.nodes then edges.(i) = [] *)
  (* i.e., if [graph.edges.(i) <> []] then [List.mem i graph.nodes] *)
}

(* Dump/Load *)

let o3_graph =
  let dump graph = (graph.nodes, graph.edges)
  and load (nodes, edges) = {nodes; edges} in
  (dump, load)

let stree_of_graph = ToSTree.(map (fst o3_graph) (pair (list int) (array (list int))))
let stree_to_graph = OfSTree.(map (snd o3_graph) (pair (list int) (array (list int))))

(* Tools *)

let check graph =
(* returns true iff the graph [graph] is properly formed *)
(* Time Complexity O(n^2) *)
  let check_nodes nodes =
    (SetList.sorted nodes)&&(SetList.subset_of nodes graph.nodes)
  in
  (SetList.sorted graph.nodes)&&
  (
    let seq = Array.map (fun nodes -> nodes = []) graph.edges in
    List.iter (fun node -> seq.(node) <- true) graph.nodes;
    Array.for_all (fun x -> x) seq
  )&&
  (Array.for_all check_nodes graph.edges)

let reduce graph =
(* remove replicates *)
(* Time Complexity O(n^2.log(n)) *)
{
  nodes = SetList.sort graph.nodes;
  edges = Array.map SetList.sort graph.edges
}

(* neighbourhood related tools *)

let voisins graph set =
(* returns the neighbourhood of [set] in [graph] *)
(* Time Complexity O( #edges( set - * ) + n ) = O(n^2)
     with #edges( set - * ) is the number of edges linked to set
 *)
  let vect = BArray.make (Array.length graph.edges) false in
  List.iter (fun x ->
    List.iter (fun y -> BArray.set vect y true) graph.edges.(x)) set;
  vect |> BArray.to_list

let voisins_strict graph set =
(* returns the strict neighbourhood of [set] in [graph] *)
(* Time Complexity O(n^2) *)
  SetList.minus (voisins graph set) set

let print_graph graph : unit =
  print_string ToS.("{nodes = "^((list int) graph.nodes)^"; edges = "^(array(list int) graph.edges)^" }"); print_newline();
  ()
