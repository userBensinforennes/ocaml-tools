open STools

let show_debug = ref false

module REGULAR =
struct
  let get_byte s i = Char.code (Bytes.unsafe_get s i)
  let set_byte s i x = Bytes.unsafe_set s i (Char.unsafe_chr x)

  let array_unsafe_get a i = Array.unsafe_get a i
  let array_unsafe_set a i x = Array.unsafe_set a i x

  let bytes_unsafe_get a i = Bytes.unsafe_get a i
  let bytes_unsafe_set a i x = Bytes.unsafe_set a i x

  let string_unsafe_get a i = String.unsafe_get a i

  let ( %+ ) a b =
    a lsl b
  let mask2n n = (1 %+ n) - 1
  let ( %% ) a b =
    a land (mask2n b)
  let ( %- ) a b =
    a lsr b
end

module DEBUG =
struct
  let get_byte s i = Char.code (Bytes.get s i)
  let set_byte s i x = Bytes.set s i (Char.chr x)

  let array_unsafe_get a i = Array.get a i
  let array_unsafe_set a i x = Array.set a i x

  let bytes_unsafe_get a i = Bytes.get a i
  let bytes_unsafe_set a i x = Bytes.set a i x

  let string_unsafe_get a i = String.get a i

  let ( %+ ) a b =
    assert(0<=b && b < 64);
    a lsl b
  let mask2n n = (1 %+ n) - 1
  let ( %% ) a b =
    a land (mask2n b)
  let ( %- ) a b =
    assert(0<=b && b < 64);
    a lsr b
end
include REGULAR

type t = {
  length : int;
  bytes  : Bytes.t
}

let length ba = ba.length
let size ba = Bytes.length ba.bytes

let max_length = Sys.max_string_length * 8

let low_mask_array = Array.init 9 mask2n
let low_mask x = array_unsafe_get low_mask_array x
let high_mask x = (array_unsafe_get low_mask_array (8-x)) lxor 255

let keep_lowest_bits x i = x land (low_mask i)
let ( %% ) = keep_lowest_bits
let keep_highest_bits x i = x land (high_mask i)

let bytes_create text length =
  if length < 0 || length > max_length then invalid_arg text;
  let len = (length+7)/8 in
  (len, {length; bytes = Bytes.create len})

let create length =
  let len, ba = bytes_create "BArray.create" length in
  (* NOTE : if length > 0 is enough *)
  if length mod 8 > 0 then (
    set_byte ba.bytes (len-1) 0;
  );
  ba

let normalize ba =
  let r = ba.length mod 8 in
  if r > 0 then (
    let b = ba.bytes in
    let s = Bytes.length b in
    set_byte b (s-1) ((get_byte b (s-1)) land (low_mask r))
  )

let checked ba =
  let r = ba.length mod 8 in
  ((ba.length+7)/8 = Bytes.length ba.bytes) &&
  r = 0 || (
    let len = Bytes.length ba.bytes in
    let byte = get_byte ba.bytes (len-1) in
    byte = byte land (low_mask r)
  )

let make length b =
  let len, ba = bytes_create "BArray.make" length in
  if b
  then ( (* b = true  *)
    Bytes.unsafe_fill ba.bytes 0 len '\255';
    let r = length mod 8 in (* r = len mod 8 *)
    if r > 0
    then (set_byte ba.bytes (len-1) (low_mask r))
  )
  else ( (* b = false *)
    Bytes.unsafe_fill ba.bytes 0 len '\000'
  );
  ba

let copy ba = { length = ba.length; bytes = Bytes.copy ba.bytes }

let unsafe_get ba n =
  let i = n lsr 3 in
  ((get_byte ba.bytes i) lsr (n land 7)) land 1 <> 0

let get ba n =
  if n < 0 || n >= ba.length then invalid_arg "BArray.get";
  unsafe_get ba n

let unsafe_set ba n b =
  let i = n lsr 3 in
  let c = get_byte ba.bytes i in
  let mask = 1 lsl (n land 7) in
  set_byte ba.bytes i (if b then c lor mask else c land (lnot mask))

let set ba n b =
  if n < 0 || n >= ba.length then invalid_arg "BArray.set";
  unsafe_set ba n b

(* Conversions {to, of} {bool {array, list}}|string *)

let to_bool_array (ba:t) : bool array =
  let n = ba.length in
  let s = Array.make n false in
  for i = 0 to n-1
  do
    array_unsafe_set s i (unsafe_get ba i)
  done;
  s

let to_bool_list (ba:t) : bool list =
  let n = ba.length in
  let l = ref [] in
  for i = n-1 downto 0
  do
    l:=(unsafe_get ba i)::(!l);
  done;
  !l

let of_bool_array (s:bool array) : t =
  let n = Array.length s in
  let ba = make n false in
  for i = 0 to n-1 do
    unsafe_set ba i (array_unsafe_get s i)
  done;
  ba

let of_bool_list (l:bool list) : t =
  let n = List.length l in
  let ba = make n false in (* OPTME? use bytes_create instead of make *)
  let rec aux i = function
    | [] -> ()
    | h::t -> unsafe_set ba i h; aux (i+1) t in
  aux 0 l;
  ba

let to_bool_string (ba:t) : string =
  let n = ba.length in
  (* OPTME use Bytes.create instead of Bytes.make *)
  let s = Bytes.make n '0' in
  for i = 0 to n-1
  do
    bytes_unsafe_set s i
      (SUtils.char_of_bool (unsafe_get ba i))
  done;
  Bytes.unsafe_to_string s

let of_bool_string (s:string) : t =
  let n = String.length s in
  let ba = make n false in (* OPTME? use bytes_create instead of make *)
  for i = 0 to n-1 do
    unsafe_set ba i
      (SUtils.bool_of_char(string_unsafe_get s i))
  done;
  ba

let to_string : t -> string = to_bool_string
let of_string : string -> t = of_bool_string

let print fmt ba = Format.pp_print_string fmt (to_string ba)

(* (reverse) Conversions {to, of} {bool {array, list}}|string *)

let to_rev_bool_array (ba:t) : bool array =
  let n = ba.length in
  let s = Array.make n false in
  for i = 0 to n-1
  do
    array_unsafe_set s (n-1-i) (unsafe_get ba i)
  done;
  s

let to_rev_bool_list (ba:t) : bool list =
  let n = ba.length in
  let l = ref [] in
  for i = 0 to n-1
  do
    l:=(unsafe_get ba i)::(!l);
  done;
  !l

let of_rev_bool_array (s:bool array) : t =
  let n = Array.length s in
  let ba = make n false in (* OPTME? use bytes_create instead of make *)
  for i = 0 to n-1 do
    unsafe_set ba i (array_unsafe_get s (n-1-i))
  done;
  ba

let of_rev_bool_list (l:bool list) : t =
  let n = List.length l in
  let ba = make n false in (* OPTME? use bytes_create instead of make *)
  let rec aux i = function
    | [] -> ()
    | h::t -> unsafe_set ba i h; aux (i-1) t in
  aux (n-1) l;
  ba

let to_rev_bool_string (ba:t) : string =
  let n = ba.length in
  (* OPTME use Bytes.create instead of Bytes.make *)
  let s = Bytes.make n '0' in
  for i = 0 to n-1
  do
    bytes_unsafe_set s (n-1-i)
      (SUtils.char_of_bool (unsafe_get ba i))
  done;
  Bytes.unsafe_to_string s

let of_rev_bool_string (s:string) : t =
  let n = String.length s in
  let ba = make n false in (* OPTME? use bytes_create instead of make *)
  for i = 0 to n-1 do
    unsafe_set ba i
      (SUtils.bool_of_char(string_unsafe_get s (n-1-i)))
  done;
  ba

let to_rev_string = to_rev_bool_string
let of_rev_string = of_rev_bool_string

let print_rev fmt ba = Format.pp_print_string fmt (to_rev_string ba)

let sub_is_zero ba fst len =
  assert(0 <= fst && fst+len <= ba.length);
  let top = fst+len in
  let rec aux i = if i < top
    then if unsafe_get ba i
      then aux (i+1)
      else false
    else true
  in aux 0

let sub_is_one ba fst len =
  assert(0 <= fst && fst+len <= ba.length);
  let top = fst+len in
  let rec aux i = if i < top
    then if unsafe_get ba i
      then false
      else aux (i+1)
    else true
  in aux fst

(* WIP
unsafe_append_ones_first' is required but not provided
The value `append_ones_first' is required but not provided
The value `append_zeros_first' is required but not provided
The value `drop_last' is required but not provided
The value `drop_first' is required but not provided
 *)

let init length f =
  let _, ba = bytes_create "Btools.BArray.init" length in
  let r = length land 7 in
  let q = length lsr  3 in
  for i = 0 to q - 1
  do
    let i' = i lsl 3 in
    let c0 = if f  i'        then 1                  else 0  in
    let c1 = if f (i' lor 1) then (c0 lor (1 lsl 1)) else c0 in
    let c2 = if f (i' lor 2) then (c1 lor (1 lsl 2)) else c1 in
    let c3 = if f (i' lor 3) then (c2 lor (1 lsl 3)) else c2 in
    let c4 = if f (i' lor 4) then (c3 lor (1 lsl 4)) else c3 in
    let c5 = if f (i' lor 5) then (c4 lor (1 lsl 5)) else c4 in
    let c6 = if f (i' lor 6) then (c5 lor (1 lsl 6)) else c5 in
    let c7 = if f (i' lor 7) then (c6 lor (1 lsl 7)) else c6 in
    set_byte ba.bytes i c7
  done;
  if r > 0 then (
    let c = ref 0 in
    let i' = q lsl 3 in
    for j = 0 to r - 1
    do
      if f (i' lor j) then (c := !c lor (1 lsl j))
    done;
    set_byte ba.bytes q !c
  );
  ba

let random length =
  let _, ba = bytes_create "Btools.BArray.random" length in
  let q = length / 16 in
  let r = length mod 16 in
  for i = 0 to q -1
  do
    let rand = Random.int (1 %+ 16) in
    set_byte ba.bytes (2*i+0) (rand %% 8);
    set_byte ba.bytes (2*i+1) (rand %- 8)
  done;
  if r > 0 then (
    let rand = Random.int (1 %+ r) in
    if r > 8 then (
      set_byte ba.bytes (2*q+0) (rand %% 8);
      set_byte ba.bytes (2*q+1) (rand %- 8)
    ) else (
      set_byte ba.bytes (2*q+0) rand
    )
  );
  ba

let unsafe_fill_ones ba fst len : unit =
  let lst = fst + len in
  let rfst = fst mod 8
  and rlst = lst mod 8
  and qfst = fst / 8
  and qlst = lst / 8 in
  if qfst = qlst then (
    let fst_mask = high_mask rfst
    and lst_mask = low_mask  rlst in
    let mask = fst_mask land lst_mask in
    set_byte ba.bytes qfst (mask lor (get_byte ba.bytes qfst))
  ) else (
    let qfst' = if rfst = 0 then qfst else (qfst+1) in
    let qlst' = if rlst = 0 then qlst else (qlst-1) in
    if rfst <> 0 then (
      let mask = high_mask rfst in
      set_byte ba.bytes qfst (mask lor (get_byte ba.bytes qfst))
    );
    Bytes.unsafe_fill ba.bytes qfst' (qlst'-qfst'+1) '\255';
    if rlst <> 0 then (
      let mask = low_mask rlst in
      set_byte ba.bytes qlst (mask lor (get_byte ba.bytes qlst))
    )
  )

let unsafe_fill_zeros ba fst len : unit =
  let lst = fst + len in
  let rfst = fst mod 8
  and rlst = lst mod 8
  and qfst = fst / 8
  and qlst = lst / 8 in
  (if qfst = qlst then (
    let fst_mask = low_mask  rfst
    and lst_mask = high_mask rlst in
    let mask = fst_mask lor lst_mask in
    set_byte ba.bytes qfst (mask land (get_byte ba.bytes qfst))
  ) else (
    let qfst' = if rfst = 0 then qfst else (qfst+1) in
    let qlst' = if rlst = 0 then qlst else (qlst-1) in
    (if rfst <> 0 then (
      let cell_byte = get_byte ba.bytes qfst in
      let masked_cell = keep_lowest_bits cell_byte rfst in
      set_byte ba.bytes qfst masked_cell
    ));
    Bytes.unsafe_fill ba.bytes qfst' (qlst'-qfst'+1) '\000';
    (if rlst <> 0 then (
      let cell_byte = get_byte ba.bytes qlst in
      let masked_cell = keep_highest_bits cell_byte (8-rlst) in
      set_byte ba.bytes qlst masked_cell
    ));
  ));
  ()

let naive_fill ba ofs len b =
  if ofs < 0 || len < 0 || ofs + len > ba.length then invalid_arg "BArray.naive_fill";
  for i = ofs to ofs + len - 1 do unsafe_set ba i b done

let fill ba fst len b =
  if fst < 0 || len < 0 || fst + len > ba.length then invalid_arg "BArray.fill";
  if b
    then unsafe_fill_ones  ba fst len
    else unsafe_fill_zeros ba fst len

(*s All the iterators are implemented as for traditional arrays, using
  [unsafe_get]. For [iter] and [map], we do not precompute [(f
  true)] and [(f false)] since [f] may have side-effects. *)

let iter f ba =
  for i = 0 to ba.length - 1 do f (unsafe_get ba i) done

let map f ba =
  let l = ba.length in
  (* OPTME use bytes_create instead of make *)
  let r = make l false in
  (* OPTME unroll the loop as the user cannot access the returned object *)
  for i = 0 to l - 1 do
    unsafe_set r i (f(unsafe_get ba i))
  done;
  r

let iteri f ba =
  for i = 0 to ba.length - 1 do f i (unsafe_get ba i) done

let mapi f ba =
  let l = ba.length in
  (* OPTME use bytes_create instead of make *)
  let r = make l false in
  (* OPTME unroll the loop as the user cannot access the returned object *)
  for i = 0 to l - 1 do
    unsafe_set r i (f i (unsafe_get ba i))
  done;
  r

let fold_left f x ba =
  let r = ref x in
  (* OPTME unroll the loop as the user cannot access the returned object *)
  for i = 0 to ba.length - 1 do
    r := f !r (unsafe_get ba i)
  done;
  !r

let fold_right f ba x =
  let r = ref x in
  (* OPTME unroll the loop as the user cannot access the returned object *)
  for i = ba.length - 1 downto 0 do
  r := f (unsafe_get ba i) !r
  done;
  !r

let foldi_left f x ba =
  let r = ref x in
  (* OPTME unroll the loop as the user cannot access the returned object *)
  for i = 0 to ba.length - 1 do
  r := f !r i (unsafe_get ba i)
  done;
  !r

let foldi_right f ba x =
  let r = ref x in
  (* OPTME unroll the loop as the user cannot access the returned object *)
  for i = ba.length - 1 downto 0 do
  r := f i (unsafe_get ba i) !r
  done;
  !r

let iteri_true f ba =
  (* OPTME unroll the loop as the user cannot access the returned object *)
  (* OPTME replace Bytes.iteri by an actuall loop *)
  Bytes.iteri
  (fun i x -> let x = Char.code x in if x != 0 then begin
    let i = i %+ 3 in
    for j = 0 to 7 do if x land (1 %+ j) > 0 then f (i + j) done
  end)
  ba.bytes

(*s Population count (return the number of bits set to true) *)

let pop ba =
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  let rec loop acc i = if i < len
    then loop (acc + Internal_BChar.pop8 (bytes_unsafe_get bytes i)) (i + 1)
    else acc
  in
  loop 0 0

(*s First true index *)
let fst_true (ba:t) : int option =
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  let rec loop i = if i < len
    then (
      let c = bytes_unsafe_get bytes i in
      if c = '\000'
        then (loop(i+1))
        else Some (i*8+Internal_BChar.unsafe_fst8_true c)
    ) else None
  in loop 0

(*s First false index *)
let fst_false (ba:t) : int option =
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  let rec loop i = if i < len
    then (
      let c = bytes_unsafe_get bytes i in
      if c = '\255'
        then (loop(i+1))
        else (
          let p = i*8+Internal_BChar.unsafe_fst8_false c in
          if p < ba.length then Some p else None
        )
    ) else None
  in loop 0

(*s Last true index *)
let lst_true (ba:t) : int option =
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  let rec loop i = if i >= 0
    then (
      let c = bytes_unsafe_get bytes i in
      if c = '\000'
        then (loop(i-1))
        else Some (i*8+Internal_BChar.unsafe_lst8_true c)
    ) else None
  in loop (len-1)

(*s Last false index *)

let lst_false (ba:t) : int option =
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  let rec loop i = if i >= 0
    then (
      let c = bytes_unsafe_get bytes i in
      if c = '\255'
        then (loop(i-1))
        else Some (i*8+Internal_BChar.unsafe_lst8_false c)
    ) else None
  in
  let r = ba.length mod 8 in
  if r = 0
  then (loop(len-1))
  else (
    let c = bytes_unsafe_get bytes (len-1) in
    let c' = Char.unsafe_chr ((Char.code c) lor (high_mask r)) in
    if c = '\255'
      then (loop(len-2))
      else Some ((len-1)*8+Internal_BChar.unsafe_lst8_false c')
  )

(*s Bitwise operations. It is straigthforward, since bitwise operations
  can be realized by the corresponding bitwise operations over integers.
  However, one has to take care of normalizing the result of [bwnot]
  which introduces ones in highest significant positions. *)

let bw_and v1 v2 =
  let l = v1.length in
  if l <> v2.length then invalid_arg "BArray.bw_and";
  let b1 = v1.bytes
  and b2 = v2.bytes in
  let n = Bytes.length b1 in
  (* OPT : copying first improves perfs *)
  let bytes = Bytes.copy b1 in
  for i = 0 to n - 1 do
    set_byte bytes i
      ((get_byte bytes i) land (get_byte b2 i))
  done;
  { length = l; bytes }

(* v1 = v1 && v2 *)
let bw_and' v1 v2 =
  let l = v1.length in
  if l <> v2.length then invalid_arg "BArray.bw_and (in place)";
  let b1 = v1.bytes
  and b2 = v2.bytes in
  let n = Bytes.length b1 in
  for i = 0 to n - 1 do
    set_byte b1 i ((get_byte b1 i) land (get_byte b2 i))
  done;
  v1

let bw_or v1 v2 =
  let l = v1.length in
  if l <> v2.length then invalid_arg "BArray.bw_or";
  let b1 = v1.bytes
  and b2 = v2.bytes in
  let n = Bytes.length b1 in
  (* OPT : copying first improves perfs *)
  let bytes = Bytes.copy b1 in
  for i = 0 to n - 1 do
    set_byte bytes i
      ((get_byte bytes i) lor (get_byte b2 i))
  done;
  { length = l; bytes }

(* v1 = v1 || v2 *)
let bw_or' v1 v2 =
  let l = v1.length in
  if l <> v2.length then invalid_arg "BArray.bw_or (in place)";
  let b1 = v1.bytes
  and b2 = v2.bytes in
  let n = Bytes.length b1 in
  for i = 0 to n - 1 do
    set_byte b1 i ((get_byte b1 i) lor (get_byte b2 i))
  done;
  v1

let bw_xor v1 v2 =
  let l = v1.length in
  if l <> v2.length then invalid_arg "BArray.bw_or";
  let b1 = v1.bytes
  and b2 = v2.bytes in
  let n = Bytes.length b1 in
  (* OPT : copying first improves perfs *)
  let bytes = Bytes.copy b1 in
  for i = 0 to n - 1 do
    set_byte bytes i
      ((get_byte bytes i) lxor (get_byte b2 i))
  done;
  { length = l; bytes }

(* v1 = v1 <> v2 *)
let bw_xor' v1 v2 =
  let l = v1.length in
  if l <> v2.length then invalid_arg "BArray.bw_xor (in place)";
  let b1 = v1.bytes
  and b2 = v2.bytes in
  let n = Bytes.length b1 in
  for i = 0 to n - 1 do
    set_byte b1 i ((get_byte b1 i) lxor (get_byte b2 i))
  done;
  v1

let bw_not ba =
  let bytes = ba.bytes in
  let r = ba.length mod 8 in
  let n = Bytes.length bytes in
  let bytes' = Bytes.create n in
  let top = if r = 0
    then (n-1)
    else (n-2)
  in
  for i = 0 to top do
    set_byte bytes' i ((mask2n 8) lxor (get_byte bytes i))
  done;
  if r > 0 then (
    set_byte bytes' (n-1)
      ((low_mask r) lxor get_byte bytes (n-1))
  );
  {length = ba.length; bytes = bytes'}

(* ba = not ba *)
let bw_not' ba =
  let bytes = ba.bytes in
  let r = ba.length mod 8 in
  let n = Bytes.length bytes in
  let top = if r = 0
    then (n-1)
    else (n-2)
  in
  for i = 0 to top do
    set_byte bytes i ((mask2n 8) lxor (get_byte bytes i))
  done;
  if r > 0 then (
    set_byte bytes (n-1)
      ((low_mask r) lxor get_byte bytes (n-1))
  );
  ba

(*s Coercions to/from lists of integers *)

let of_list l =
  match l with
  | [] -> (make 0 false)
  | _ -> (
    let n = List.fold_left max 0 l in
    let ba = make (succ n) false in
    let add_element i =
      (* negative numbers are invalid *)
      if i < 0 then invalid_arg "BArray.of_list";
      unsafe_set ba i true
    in
    List.iter add_element l;
    ba
  )

let of_list_with_length l len =
  let ba = make len false in
  let add_element i =
    if i < 0 || i >= len then invalid_arg "BArray.of_list_with_length";
    unsafe_set ba i true
  in
  List.iter add_element l;
  ba

let to_list ba =
  let n = ba.length in
  let rec make i acc =
    if i < 0 then acc
    else make (pred i) (if unsafe_get ba i then i :: acc else acc)
  in
  make (pred n) []

let to_hexa_string (ba:t) : string =
  let q = ba.length / 8 in
  let r = ba.length mod 8 in
  let n = ((ba.length+3)/4) in (* roundup (ba.length / 4) *)
  assert(0 <= n && n <= Sys.max_string_length);
  let s = Bytes.create n in
  for i = 0 to q - 1
  do
    let x = get_byte ba.bytes i in
    let char0 = SUtils.unsafe_hexa_of_int (x %% 4)
    and char1 = SUtils.unsafe_hexa_of_int (x %- 4) in
    bytes_unsafe_set s (2*i+0) char0;
    bytes_unsafe_set s (2*i+1) char1;
  done;
  if r = 0 then () else (
    let x = get_byte ba.bytes q in
    assert(x < (1 %+ r));
      if r = 4 then (
        bytes_unsafe_set s (2*q+0) (SUtils.widehexa_of_int x)
    ) else (
      let inc = match (r mod 4) with
        | 3 -> 16 | 2 -> 24 | 1 -> 28 | _ -> assert false
      in
      if r > 4 then (
        bytes_unsafe_set s (2*q+0) (SUtils.widehexa_of_int (x %% 4));
        bytes_unsafe_set s (2*q+1) (SUtils.widehexa_of_int (inc + (x %- 4)))
      ) else (
        bytes_unsafe_set s (2*q+0) (SUtils.widehexa_of_int (inc + (x %% 4)))
      )
    )
  );
  Bytes.unsafe_to_string s

let of_hexa_string (s:string) : t =
  let n = String.length s in
  if n = 0 then {length = 0; bytes = Bytes.create 0} else (
    let r = n land 1 (* r = n mod 2 *)
    (* let q = if r = 0 then (n/2) - 1 else (n/2) *)
    and b_len = (n+1)/2 in
    let bytes = Bytes.create b_len in (* Bytes.length bytes = roundup (n / 2) *)
    let last_char = string_unsafe_get s (n-1) in
    (* NOTE : do not make hexa-conversion unsafe *)
    let delta, last_half = SUtils.sized_int_of_widehexa last_char in
    let length = delta+(4*(n-1)) in
    for i = 0 to b_len - 2
    do
      let char0 = string_unsafe_get s (2*i+0)
      and char1 = string_unsafe_get s (2*i+1) in
      let byte0 = SUtils.int_of_hexa char0
      and byte1 = SUtils.int_of_hexa char1 in
      set_byte bytes i (byte0 lor (byte1 %+ 4));
    done;
    let last_byte = if r = 0 then (
        let char0 = string_unsafe_get s (n-2) in
        let byte0 = SUtils.int_of_hexa char0 in
        byte0 lor (last_half %+ 4)
      ) else (
        last_half
      ) in
    set_byte bytes (b_len-1) last_byte;
    {bytes; length}
  )

(*s (* adapted from [BArray.blit] *)
    Handling bits by packets is the key for efficiency of functions
    [append], [concat], [sub] and [blit].
    We start by a very general function [blit_bits a i m v n] which blits
    the bits [i] to [i+m-1] of a native integer [a]
    onto the bit vector [v] at index [n]. It assumes that [i..i+m-1] and
    [n..n+m-1] are respectively valid subparts of [a] and [v].
    It is optimized when the bits fit the lowest boundary of an integer
    (case [j == 0]). *)

let pos n = (n lsr 3, n land 7)

let blit_bits a i m v n =
  let (i', j) = pos n in
  if j == 0 then
    set_byte v i'
      ((keep_lowest_bits (a lsr i) m) lor
       (keep_highest_bits (get_byte v i') (8-m)))
  else
    let d = m+j-8 in
    if d > 0 then (
      set_byte v i'
        (((keep_lowest_bits (a lsr i) (8-j)) lsl j) lor
         (keep_lowest_bits (get_byte v i') j));
      set_byte v (succ i')
        ((keep_lowest_bits (a lsr (i+8-j)) d) lor
         (keep_highest_bits (get_byte v (succ i')) (8 - d)))
    ) else
      set_byte v i'
        (((keep_lowest_bits (a lsr i) m) lsl j) lor
         ((get_byte v i') land ((low_mask j) lor (high_mask (-d)))))

(*s [blit_int] implements [blit_bits] in the particular case when
    [i=0] and [m=8] i.e. when we blit all the bits of [a]. *)

let blit_int a v n =
  let i, j = pos n in
  if j == 0 then
    set_byte v i a
  else (
    set_byte v i
      ((keep_lowest_bits (get_byte v i) j) lor
       ((keep_lowest_bits a (8-j)) lsl j));
    set_byte v (succ i)
      ((keep_highest_bits (get_byte v (succ i)) (8-j)) lor
       (a lsr (8-j)))
  )

(*s When blitting a subpart of a bit vector into another bit vector, there
    are two possible cases: (1) all the bits are contained in a single integer
    of the first bit vector, and a single call to [blit_bits] is the
    only thing to do, || (2) the source bits overlap on several integers of
    the source array, and then we do a loop of [blit_int], with two calls
    to [blit_bits] for the two bounds. *)

let internal_blit v1 ofs1 v2 ofs2 len =
  if len > 0 then
    let bi, bj = pos ofs1 in
    let ei, ej = pos (ofs1+len-1) in
    if bi == ei
    then (blit_bits (get_byte v1 bi) bj len v2 ofs2)
    else (
      blit_bits (get_byte v1 bi) bj (8-bj) v2 ofs2;
      let n = ref (ofs2+8-bj) in
      for i = succ bi to pred ei do
        blit_int (get_byte v1 i) v2 !n;
        n := !n + 8
      done;
      blit_bits (get_byte v1 ei) 0 (succ ej) v2 !n
    )

let unsafe_blit ba1 fst1 ba2 fst2 len =
  internal_blit ba1.bytes fst1 ba2.bytes fst2 len

let blit ba1 fst1 ba2 fst2 len =
  if len < 0 || fst1 < 0 || fst1+len > ba1.length
             || fst2 < 0 || fst2+len > ba2.length
  then invalid_arg "BArray.blit";
  unsafe_blit ba1 fst1 ba2 fst2 len

(* we assume [ length ba >= shrink ]
   then we denote DROPED:=unsafe_drop_first ba shrink
   then [ length(DROPED) = (length ba) - shrink ]
   the \forall i, [0 <= i < length DROPED] -> [DROPED.(i) = ba.(i+shrink)]
 *)
let unsafe_drop_first (ba:t) (shrink:int) : t =
  let q = shrink  /  8 in
  let r = shrink mod 8 in
  let len', ba' = bytes_create "BArray.unsafe_drop_first" (ba.length-shrink) in
  let bytes' = ba'.bytes in
  let bytes  = ba.bytes in
  if r = 0 then (
    Bytes.blit bytes q bytes' 0 len';
  ) else (
    blit ba shrink ba 0 ba'.length
  );
  ba'

let drop_first ba shrink =
  if shrink > ba.length then invalid_arg "BArray.drop_first";
  unsafe_drop_first ba shrink

(* we assume [ length ba >= shrink ]
   then, we denote DROPED:=unsafe_drop_last ba shrink
   then [ length(DROPED) = (length ba) - shrink ]
   then \forall i, [0<=i< length DROPED] -> [DROPED.(i) = ba.(i)]
 *)
let unsafe_drop_last (ba:t) (shrink:int) : t =
  let len', ba' = bytes_create "BArray.unsafe_drop_last" (ba.length-shrink) in
  Bytes.blit ba.bytes 0 ba'.bytes 0 len';
  let r = ba'.length mod 8 in
  if r > 0 then (
    set_byte ba.bytes (len'-1)
      ((get_byte ba.bytes (len'-1)) land (mask2n r))
  );
  ba'

let drop_last ba shrink =
  if shrink > ba.length then invalid_arg "BArray.drop_last";
  unsafe_drop_last ba shrink

(* (* [FIXME] *)
let append_zeros_first (ba:t) (shift:int) : t =
  let q = shift  /  8 in
  let r = shift mod 8 in
  let len', ba' = bytes_create "BArray.unsafe_append_zeros_first" (ba.length+shift) in
  let bytes' = ba'.bytes in
  let bytes  = ba.bytes in
  let len    = Bytes.length bytes in
  if r = 0 then (
    Bytes.fill bytes' 0 q '\000';
    Bytes.blit bytes 0 bytes' q len
  ) else (
    unsafe_fill_zeros ba' 0 shift;
    unsafe_blit ba 0 ba' shift ba.length
  );
  ba'
 *)

let append_zeros_first (ba:t) (shift:int) : t =
  let ba' = create (ba.length+shift) in
  unsafe_fill_zeros ba' 0 shift;
  unsafe_blit ba 0 ba' shift ba.length;
  assert (checked ba');
  ba'

let append_zeros_last (ba:t) (shift:int) : t =
  let len', ba' = bytes_create "BArray.unsafe_append_zeros_last" (ba.length+shift) in
  let bytes' = ba'.bytes in
  let bytes = ba.bytes in
  let len = Bytes.length bytes in
  Bytes.blit bytes 0 bytes' 0 len;
  if len' > len then (
    Bytes.fill bytes' len (len'-len) '\000';
  );
  ba'

let internal_meta_shift_first
    (get_char:int->char) (fst:int)
    (set_char:int->char->unit)
    (shift:int) (len:int) : char * char =
  let cshift = 8-shift in
  let byte0 = Char.code(get_char fst) in
  let fst_byte = byte0 %% shift in
  let top = fst + len in
  let rec aux carry index = if index+1 < top then (
      let byte   = Char.code(get_char (index+1)) in
      let carry' = byte %- shift in
      let byte'  = carry lor ((byte%%shift)%+cshift) in
      set_char index (Char.unsafe_chr byte');
      aux carry' (index+1)
    ) else carry
  in
  let lst_byte = aux (byte0 %- shift) fst in
  (Char.unsafe_chr fst_byte, Char.unsafe_chr lst_byte)

let rev (ba:t) : t =
  let len, ba' = bytes_create "BArray.rev" ba.length in
  let r = ba.length mod 8 in
  if r = 0 then (
    for i=0 to len-1
    do
      let byte = Internal_BChar.rev8(bytes_unsafe_get ba.bytes i) in
      bytes_unsafe_set ba'.bytes (len-i-1) byte
    done
  ) else (
    let rev_get i =
      Internal_BChar.rev8(bytes_unsafe_get ba.bytes(len-i-1))
    in
    let _, lst_byte = internal_meta_shift_first
      rev_get 0 (bytes_unsafe_set ba'.bytes) (8-r) len in
    bytes_unsafe_set ba'.bytes (len-1) lst_byte

  );
  ba'

let to_stree x = Tree.Leaf (to_hexa_string x)
let of_stree = function
  | Tree.Leaf leaf -> of_hexa_string leaf
  | _ -> assert false
let o3_stree = (to_stree, of_stree)

let to_rev_hexa_string ba = ba |> rev |> to_hexa_string
let of_rev_hexa_string s  = s  |> of_hexa_string |> rev

(* assume [0 <= fst <= ba.length - len] *)
let unsafe_sub ba fst len =
  let ba' = make len true in
  (* let _, ba' = bytes_create "BArray.unsafe_sub" len in *)
  unsafe_blit ba fst ba' 0 len;
  ba'

let sub ba fst len =
  assert(0 <= fst && 0 <= len && fst+len <= ba.length);
  unsafe_sub ba fst len

let append ba1 ba2 =
  let len, ba' = bytes_create "BArray.append" (ba1.length+ba2.length) in
  Bytes.blit ba1.bytes 0 ba'.bytes 0 (Bytes.length ba1.bytes);
  unsafe_blit ba2 0 ba' ba1.length ba2.length;
  ba'

let concat_array ba_array =
  let length' = Array.fold_left (fun sum ba -> sum + (length ba)) 0 ba_array in
  let _, ba' = bytes_create "BArray.concat_array" length' in
  let rec aux pos i = if i < Array.length ba_array then (
      let ba = ba_array.(i) in
      (* OPTME if pos mod 8 = 0 then use Blit.unsafe_blit instead of unsafe_blit
         NOTE : this capture the [pos = 0] case
       *)
      unsafe_blit ba' pos ba 0 ba.length;
      aux (pos+ba.length) (i+1)
    ) else (assert(pos=length'))
  in
  aux 0 0;
  ba'

let concat_list ba_list =
  let length' = List.fold_left (fun sum ba -> sum + (length ba)) 0 ba_list in
  let _, ba' = bytes_create "BArray.concat_array" length' in
  let rec aux pos = function
    | [] -> (assert(pos = length'))
    | ba::tail -> (
      (* OPTME if pos mod 8 = 0 then use Blit.unsafe_blit instead of unsafe_blit
         NOTE : this capture the [pos = 0] case
       *)
      unsafe_blit ba 0 ba' pos ba.length;
      aux (pos+ba.length) tail
    )
  in
  aux 0 ba_list;
  ba'

(*s Input/output in a machine-independent format. *)

let output_bin out_ch ba =
  let len = length ba in
  let rec loop i pow byte =
    let byte = if unsafe_get ba i then byte lor pow else byte in
    if i = len - 1 then
      output_byte out_ch byte
    else if i mod 8 = 7 then begin
      output_byte out_ch byte;
      loop (i + 1) 1 0
    end else
      loop (i + 1) (pow * 2) byte
  in
  output_binary_int out_ch len;
  if len > 0 then loop 0 1 0

let input_bin in_ch =
  let len = input_binary_int in_ch in
  let bytes = make len false in
  let rec loop i byte =
    if i < len then begin
      let byte = if i mod 8 = 0 then input_byte in_ch else byte in
      if byte land 1 = 1 then unsafe_set bytes i true;
      loop (i+1) (byte / 2)
    end
  in
  if len > 0 then loop 0 0;
  bytes
