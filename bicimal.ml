type t = int list
(* positive bicimal *)
(* sorted by increasing order *)
(* [[t]] = sum_{x\in t} 2^x *)

let add_single ?(strict=true) (i:int) (b:t) : t =
  let rec aux carry i = function
  | [] -> List.rev (i::carry)
  | h::t as l ->
         if i < h
    then (List.rev_append carry (i::l))
    else if i > h
    then (aux (h::carry) i t)
    else (aux carry (succ i) t)
    (* assert(i = h) *)
  in aux [] i b
  |> Tools.check_print (SetList.sorted ~strict)
    (fun out ->
      print_newline();
      print_string ("[Bicimal.add_single] i:"^STools.ToS.(int i)); print_newline();
      print_string ("[Bicimal.add_single] b:"^STools.ToS.(list int b)); print_newline();
      print_string ("[Bicimal.add_single] SetList.sorted b:"^STools.ToS.(bool(SetList.sorted ~strict b))); print_newline();
      print_string ("[Bicimal.add_single] out:"^STools.ToS.(list int out)); print_newline())

let add ?(strict=true) (b1:t) (b2:t) : t =
  let rec aux carry = function
    | [], bc
    | bc, [] -> (List.rev carry)@bc
    | h1::t1, h2::t2 ->
           if h1 < h2
      then aux (h1::carry) (t1, h2::t2)
      else if h1 > h2
      then aux (h2::carry) (h1::t1, t2)
      else aux carry (add_single ~strict (succ h1) t1, t2)
  in
  aux [] (b1, b2)
  |> Tools.check_print (SetList.sorted ~strict)
    (fun out ->
      print_newline();
      print_string ("[Bicimal.add] b1:"^STools.ToS.(list int b1)); print_newline();
      print_string ("[Bicimal.add] SetList.sorted b1:"^STools.ToS.(bool(SetList.sorted ~strict b1))); print_newline();
      print_string ("[Bicimal.add] b2:"^STools.ToS.(list int b1)); print_newline();
      print_string ("[Bicimal.add] SetList.sorted b2:"^STools.ToS.(bool(SetList.sorted ~strict b2))); print_newline();
      print_string ("[Bicimal.add] out:"^STools.ToS.(list int out)); print_newline())

let normalize ?(sorted=false) (il:int list) : t =
  (* print_endline ("[Bicimal.normalize] il:"^STools.ToS.(list int il)); *)
  let rec aux carry h1 = function
    | [] -> (List.rev (h1::carry))
    | h2::t2 ->
           if h1 < h2
      then aux (h1::carry) h2 t2
      else if h1 = h2
      then aux carry (succ h1) t2
      else (* assert(h1 > h2) *)
           aux carry h2 (add_single ~strict:false h1 t2)
  in
  let il0 = if sorted then il else List.sort Stdlib.compare il in
  match il0 with
  | [] -> []
  | h::t -> (
    aux [] h t
    |> Tools.check_print SetList.sorted
        (fun out ->
          print_newline();
          print_string ("[Bicimal.normalize] sorted:"^STools.ToS.(bool sorted)); print_newline();
          print_string ("[Bicimal.normalize] il:"^STools.ToS.(list int il)); print_newline();
          print_string ("[Bicimal.normalize] SetList.sorted il:"^STools.ToS.(bool(SetList.sorted il))); print_newline();
          if not sorted then (
            print_string ("[Bicimal.normalize] il0:"^STools.ToS.(list int il0)); print_newline();
            print_string ("[Bicimal.normalize] SetList.sorted il0:"^STools.ToS.(bool(SetList.sorted il0))); print_newline();
          );
          print_string ("[Bicimal.normalize] out:"^STools.ToS.(list int out)); print_newline())
  )
