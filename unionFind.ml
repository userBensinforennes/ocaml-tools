(* a cell with a negative number is interpreted a un-classed cell
 *)

type uf_table = int array

let init n = Array.init n (fun x -> x)

let rec find uf_table x =
  if uf_table.(x) = x
  then x
  else
  (
    let x' = find uf_table (uf_table.(x)) in
    uf_table.(x) <- x';
    x'
  )

let union uf_table x y =
  let x' = find uf_table x
  and y' = find uf_table y in
  if x' < y'
  then uf_table.(y') <- x'
  else if y' < x'
  then uf_table.(x') <- y'
  else ()

(* a cell with a negative number is interpreted a un-classed cell
 *)
let flatten uf_table : unit =
  let n = Array.length uf_table in
  for i = 0 to n-1
  do
    let x = uf_table.(i) in
    if x >= 0 && i <> x then (
        uf_table.(i) <- uf_table.(x)
    )
  done

(* return the number of component
 *)
let count uf_table : int =
  let n = Array.length uf_table in
  let cnt = ref 0 in
  for i = 0 to n-1
  do
    if uf_table.(i) = i then (incr cnt)
  done;
  !cnt

(* rename each component with an index between 0 and #C-1 (with #C being the number of component)
 *)
let remap uf_table : int array =
  let n = Array.length uf_table in
  let remap = Array.copy uf_table in
  let cnt = ref 0 in
  for i = 0 to n-1
  do
    let x = uf_table.(i) in
    if x = i then (
      remap.(i) <- !cnt;
      incr cnt
    ) else if x >= 0 then (
      remap.(i) <- remap.(x)
    )
  done;
  remap

(* rename each component with an index between 0 and #C-1 (with #C being the number of component)
 *)

let partition uf_table : (int array) * (int list array) * ((int * int) list) =
  let n = Array.length uf_table in
  let remap = Array.copy uf_table in
  let cnt = ref 0 in
  for i = 0 to n-1
  do
    let x = uf_table.(i) in
    if x = i then (
      remap.(i) <- !cnt;
      incr cnt
    ) else if x >= 0 then (
      remap.(i) <- remap.(x)
    ) else (
      remap.(i) <- (-1);
    )
  done;
  let ncomp = !cnt in
  let comps = Array.make ncomp [] in
  let other = ref [] in
  for i = n-1 downto 0
  do
    let x = uf_table.(i) in
    if x < 0 then (
      other := (i, x) :: !other
    ) else (
      let j = remap.(i) in
      comps.(j) <- i :: comps.(j)
    )
  done;
  (remap, comps, !other)

let ends uf_table =
  Array.iteri (fun i _ -> ignore(find uf_table i)) uf_table;
(*  Array.iter (fun x -> print_int x; print_string " ") uf_table;
  print_newline(); *)
  Array.iteri (fun i x -> assert((x=i)||(uf_table.(x)=x))) uf_table;
  let remap = remap uf_table in
  let parts = Array.make (Array.length remap) [] in
  Array.iteri (fun i x -> parts.(x) <- i::(parts.(x))) remap;
  (remap, parts)
