open STools

let debug = false

let unsafe_get (c:char) (x:int) : bool =
  ((Char.code c) lsr x) land 1 <> 0

let print (c:char) =
  for i = 0 to 7
  do
    print_char(SUtils.char_of_bool(unsafe_get c i))
  done

let get (c:char) (x:int) : bool =
  assert(0<=x && x<8);
  unsafe_get c x

let unsafe_set (c:char) (x:int) (b:bool) : char =
  let mask = 1 lsl x in
  Char.unsafe_chr (if b
    then ((Char.code c) lor mask)
    else ((Char.code c) land (lnot mask)))

let set (c:char) (x:int) (b:bool) : char =
  assert(0<=x && x<8);
  unsafe_set c x b

let to_bool_array (c:char) : bool array =
  Array.init 8 (unsafe_get c)
let of_bool_array (a:bool array) : char =
  assert(Array.length a = 8);
  let y = ref '\000' in
  for i = 0 to 7
  do
    y := set !y i (Array.unsafe_get a i)
  done;
  !y

let proto_rev8 (x:int) : char =
  let rec aux y i = if i < 8 then (
      let ib = (x lsr i) land 1 in
      aux (y lor (ib lsl (7-i))) (i+1)
    ) else y
  in
  Char.chr(aux 0 0)
let naive_rev8 (c:char) : char =
  proto_rev8(Char.code c)

let rev8_string = String.init 256 proto_rev8
let rev8 (x:char) : char = String.unsafe_get rev8_string (Char.code x)

let () = if false && debug then (
  for i = 0 to 255
  do
    let c = Char.chr i in
    print c;
    print_string " => ";
    print (rev8 c);
    print_newline();
  done
)

let rec proto_pop (x:int) : int =
  assert (x < 256);
  if x = 0 then 0 else 1 + proto_pop (x - (x land -x))

let naive_pop (c:char) : int =
  proto_pop(Char.code c)

let pop8_array = Array.init 256 proto_pop
let pop8 (n:char) = Array.unsafe_get pop8_array (Char.code n)

(* we assume [0<=n<8] *)
let unsafe_low_mask (n:int) : char =
  Char.unsafe_chr((1 lsl n)-1)
let low_mask (n:int) : char =
  assert(0<=n && n<8);
  unsafe_low_mask n

(* we assume [0<=n<8] *)
let unsafe_high_mask (n:int) : char  =
  Char.unsafe_chr((1 lsl (8-n))-1)
let high_mask (n:int) : char =
  assert(0<=n && n<8);
  unsafe_high_mask n

let unsafe_fill_ones (c:char) (fst:int) (len:int) : char =
  let fst_mask = Char.code(unsafe_high_mask fst)
  and lst_mask = Char.code(unsafe_low_mask (fst+len))
  in
  let mask = fst_mask land lst_mask in
  Char.unsafe_chr((Char.code c) lor mask)

let fill_ones (c:char) (fst:int) (len:int) : char =
  assert(0<=fst && 0<=len && fst+len<8);
  unsafe_fill_ones c fst len

(* [TODO] check for bugs *)
let unsafe_fill_zeros (c:char) (fst:int) (len:int) : char =
  let fst_mask = Char.code(unsafe_low_mask fst)
  and lst_mask = Char.code(unsafe_high_mask (fst+len))
  in
  let mask = fst_mask lor lst_mask in
  Char.unsafe_chr((Char.code c) land mask)

let fill_zeros (c:char) (fst:int) (len:int) : char =
  assert(0<=fst && 0<=len && fst+len<8);
  unsafe_fill_zeros c fst len

let fill (c:char) (fst:int) (len:int) (b:bool) : char =
  assert(0<=fst && 0<=len && fst+len<8);
  if b
  then (unsafe_fill_ones  c fst len)
  else (unsafe_fill_zeros c fst len)

let proto_fst8_true (x:int) : char =
  let c = Char.chr x in
  let rec aux i = if i < 8
    then if (unsafe_get c i)
      then (Char.chr i)
      else aux (i+1)
    else '\255'
  in aux 0

let proto_lst8_true (x:int) : char =
  let c = Char.chr x in
  let rec aux i : char = if i >= 0
    then if (unsafe_get c i)
      then (Char.chr i)
      else aux (i-1)
    else '\255'
  in aux 7

let fst8_true_string = String.init 256 proto_fst8_true
let lst8_true_string = String.init 256 proto_lst8_true

let unsafe_fst8_true (c:char) : int =
  Char.code(String.unsafe_get fst8_true_string (Char.code c))

let unsafe_lst8_true (c:char) : int =
  Char.code(String.unsafe_get lst8_true_string (Char.code c))

let fst8_true_opt (c:char) : int option =
  if c = '\000'
  then None
  else Some(unsafe_fst8_true c)

let lst8_true_opt (c:char) : int option =
  if c = '\000'
  then None
  else Some(unsafe_lst8_true c)

let fst8_true (c:char) : int =
  if c = '\000' then invalid_arg "BChar.fst8_true('\000')";
  unsafe_fst8_true c

let lst8_true (c:char) : int =
  if c = '\000' then invalid_arg "BChar.lst8_true('\000')";
  unsafe_lst8_true c

let proto_fst8_false (x:int) : char =
  let c = Char.chr x in
  let rec aux i = if i < 8
    then if (unsafe_get c i)
      then aux (i+1)
      else (Char.chr i)
    else '\255'
  in aux 0

let proto_lst8_false (x:int) : char =
  let c = Char.chr x in
  let rec aux i : char = if i >= 0
    then if (unsafe_get c i)
      then aux (i-1)
      else (Char.chr i)
    else '\255'
  in aux 7

let fst8_false_string = String.init 256 proto_fst8_false
let lst8_false_string = String.init 256 proto_lst8_false

let unsafe_fst8_false (c:char) : int =
  Char.code(String.unsafe_get fst8_false_string (Char.code c))

let unsafe_lst8_false (c:char) : int =
  Char.code(String.unsafe_get lst8_false_string (Char.code c))

let fst8_false_opt (c:char) : int option =
  if c = '\255'
  then None
  else Some(unsafe_fst8_false c)

let lst8_false_opt (c:char) : int option =
  if c = '\255'
  then None
  else Some(unsafe_lst8_false c)

let fst8_false (c:char) : int =
  if c = '\255' then invalid_arg "BChar.fst8_false('\255')";
  unsafe_fst8_false c

let lst8_false (c:char) : int =
  if c = '\255' then invalid_arg "BChar.lst8_false('\255')";
  unsafe_lst8_false c
