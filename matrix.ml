open STools

type 'a matrix = {
  n : int;
  m : int;
  a : 'a array array;
}

let dim mat = (mat.n, mat.m)

let map (f : 'a -> 'b) (mat : 'a matrix) : 'b matrix =
{
  n = mat.n;
  m = mat.m;
  a = Array.map (Array.map f) mat.a;
}

let mapi (f : Poly.i2 -> 'a -> 'b) (mat : 'a matrix) : 'b matrix =
{
  n = mat.n;
  m = mat.m;
  a = Array.mapi (fun i line -> Array.mapi (fun j e -> f(i, j) e) line) mat.a;
}

let matrix_of_array2 n m a =
  assert(Array.length a = n);
  Array.iter (fun l -> assert(Array.length l = m)) a;
  {n; m; a}

let array2_of_matrix matrix = matrix.a

let pretty_bool_matrix matrix =
  let pretty_line line = SUtils.catmap "" (function true -> "X" | _ -> "-") (Array.to_list line) in
  SUtils.catmap "\n" pretty_line (Array.to_list matrix.a)

let make (n, m) x = {n; m; a = Array.make_matrix n m x}
let init (n, m) f = {n; m; a = Array.init n (fun i -> Array.init m (fun j -> f(i, j)))}

let matrix_of_array n m a =
  assert(Array.length a = n * m);
  init (n, m) (fun (i, j) -> a.(i+n*j))

let array2_of_array n m a = (matrix_of_array n m a).a

let array_of_matrix a = Array.init (a.n*a.m) (fun k -> a.a.(k mod a.n).(k / a.n))

let set mat (i, j) x = mat.a.(i).(j) <- x
let get mat (i, j)   = mat.a.(i).(j)

let sumi ( + ) (zero : 'a) n (f : int -> 'a) : 'a = Array.fold_left ( + ) zero (Array.init n f)

let multmat ( + ) ( * ) (zero : 'a) (matA : 'a matrix) (matB : 'a matrix) : 'a matrix=
  assert(matA.m = matB.n);
  init (matA.n, matB.m) (fun (i, j) -> sumi ( + ) zero matA.m (fun k -> matA.a.(i).(k) * matB.a.(k).(j)))

let expmat (( + ) : 'a -> 'a -> 'a) (( * ) : 'a -> 'a -> 'a) (zero : 'a) (unit : 'a) (mat : 'a matrix) (k : int) : 'a matrix=
  assert(mat.n = mat.m);
  let n = mat.n in
  let mult : 'a matrix -> 'a matrix -> 'a matrix = multmat ( + ) ( * ) zero in
  let mat1 = init (n, n) (fun(i, j) -> if i = j then unit else zero) in
  Tools.quick_pow mat1 mult mat k

let transpose mat = init (mat.m, mat.n) (fun(i, j) -> get mat (j, i))
