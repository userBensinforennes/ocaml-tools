rm -rf ounits_outs &> /dev/null || true
mkdir ounits_outs
for file in $(ls ounits_bin/ | grep "_ounit.native$")
do
	echo "test : $file"
	logfile="ounits_outs/$file.log"
	(time ./ounits_bin/$file &> $logfile) &> time.log
	grep -iv success $logfile
	echo "total : " $(grep -i success $logfile | wc -l) "success(es)"
	echo "TIME" >> $logfile
	cat time.log >> $logfile
	grep real time.log
	rm time.log
done
