module SUtils =
struct
  let char_of_bool = function
    | true  -> '1'
    | false  -> '0'
  let bool_of_char = function
    | '1'  -> true
    | '0'  -> false
    | _    -> failwith "[ocaml-tools/strUtils:bool_of_char] bool parsing failure"
  let string_of_bool = function
    | true   -> "1"
    | false  -> "0"
  let bool_of_string = function
    | "1"  -> true
    | "0"  -> false
    | _    -> failwith "[ocaml-tools/strUtils:bool_of_string] bool parsing failure"
  let pretty_of_bool = function
    | true  -> "1"
    | false  -> "."
  let print_bool = function
    | true  -> print_string "1"
    | false  -> print_string "0"

  let char_0 = Char.code '0'
  let char_9 = Char.code '9'
  let char_A = Char.code 'A'
  let char_Z = Char.code 'Z'
  let char_a = Char.code 'a'
  let char_z = Char.code 'z'

  let explode (s : string) : char list =
    let rec exp i l =
      if i < 0 then l else exp (i - 1) (s.[i] :: l)
    in exp (String.length s - 1) []

  let array_implode (l:char array) : string =
    String.init (Array.length l) (fun i -> l.(i))

  let list_implode (l:char list) : string =
    array_implode (Array.of_list l)

  let implode = list_implode

  let catmap s f l = String.concat s (Extra.(l ||> f))
  let catmap_list = catmap
  let catmap_array s f l =
    catmap s f (Array.to_list l)

  let index s c =
    try               Some (String.index s c)
    with Not_found -> None

  let index_from s i c =
    try               Some(String.index_from s i c)
    with Not_found -> None

  let split = String.split_on_char

  let ntimes s n =
    let len = String.length s in
    let bytes = Bytes.create (n * len) in
    for i = 0 to n-1
    do
      Bytes.blit_string s 0 bytes (len * i) len
    done;
    Bytes.unsafe_to_string bytes

  let print_stream stream = print_string (Tools.map char_of_bool stream |> implode)

  let bool_array_of_string s =
    Array.init (String.length s) (fun i -> bool_of_char s.[i])

  let string_of_bool_array a =
    String.init (Array.length a) (fun i -> char_of_bool a.(i))

  let bool_list_of_string s =
    let rec aux carry = function
      | 0 -> carry
      | n -> (
        aux ((bool_of_char s.[n-1])::carry) (n-1)
      )
    in
    aux [] (String.length s)

  let string_of_bool_list l =
    let rl = ref l in
    String.init (List.length l) (fun _ -> match !rl with
      | [] -> assert false
      | b::l -> rl:=l; char_of_bool b)

  let naive_hexa_of_int x : char =
    assert(0 <= x && x <= 0b1111);
    Char.chr(if x < 10 then (char_0+x) else (char_A+(x-10)))

  let hexa_of_int_string = String.init 16 (fun i -> naive_hexa_of_int i)
  (* char array -> string *)
  let hexa_of_int x = hexa_of_int_string.[x]
  let unsafe_hexa_of_int x = String.unsafe_get hexa_of_int_string x

  (* 0 <= x < 52 *)
  let naive_widehexa_of_int x =
    assert(0 <= x && x < 62);
    Char.chr (
           if x < 10 then (char_0+x)
      else if x < 36 then (char_A+(x-10)) (* 10 + 26 *)
      else                (char_a+(x-36))
    )
  let widehexa_of_int_string =
    String.init 62 (fun i -> naive_widehexa_of_int i)

  (* char array -> string *)
  let unsafe_widehexa_of_int x =
    String.unsafe_get widehexa_of_int_string x
  let widehexa_of_int x =
    if 0 <= x && x < 62
    then (unsafe_widehexa_of_int x)
    else (
      invalid_arg (
        "[ocaml-tools:SUtils:widehexa_of_int] 'x:int = "^
        (string_of_int x)^"' outside the range [0; 62["
      )
    )

  (* 0 < s <= 4, 0 <= x < 2^s *)
  let unsafe_widehexa_of_sized_int x s =
    let t = [|0; 28; 24; 16; 0|] in
    unsafe_widehexa_of_int (x+t.(s))

  let widehexa_of_sized_int x s =
    assert (0 < s && s <= 4 && 0 <= x && x < (1 lsl s));
    unsafe_widehexa_of_sized_int x s

  let proto_int_of_widehexa x =
    if      (x >= char_0)&&(x <= char_9)
    then (x - char_0)
    else if (x >= char_A)&&(x <= char_Z)
    then (x - char_A + 10)
    else if (x >= char_a)&&(x <= char_z)
    then (x - char_a + 36)
    else 255

  let naive_int_of_widehexa c = proto_int_of_widehexa (Char.code c)

  let char_of_widehexa_string =
    String.init 256 (fun i -> Char.chr (proto_int_of_widehexa i))
  let unsafe_char_of_widehexa c =
    (* remarq : 0 <= Char.code c <= 255, thus its not unsafe *)
    String.unsafe_get char_of_widehexa_string (Char.code c)
  let char_of_widehexa c =
    let x = unsafe_char_of_widehexa c in
    assert(Char.code x < 255);
    x
  let unsafe_int_of_widehexa c =
    Char.code (unsafe_char_of_widehexa c)
  let int_of_widehexa c =
    let x = unsafe_int_of_widehexa c in
    assert(x<255);
    x

  let unsafe_int_of_hexa = unsafe_int_of_widehexa
  let int_of_hexa c =
    let x = unsafe_int_of_hexa c in
    assert(x<16);
    x

  let unsafe_char_of_hexa = unsafe_char_of_widehexa
  let char_of_hexa c =
    let x = unsafe_char_of_hexa c in
    assert(Char.code x < 16);
    x

  let sized_int_of_widehexa c =
    let x = proto_int_of_widehexa (Char.code c) in
    assert(0<= x && x < 30);
         if x < 16 then (4, x   )
    else if x < 24 then (3, x-16)
    else if x < 28 then (2, x-24)
    else                (1, x-28)

  let sized_char_of_widehexa c =
    let s, x = sized_int_of_widehexa c in
    assert(s>0);
    (s, Char.unsafe_chr x)
end

module ToS =
struct
  type 'a t = 'a -> string
  let ref dump x = "ref("^(dump !x)^")"
  let string item : string = "\""^item^"\""
  let option dump = function
    | None -> "None"
    | Some x -> "Some ("^(dump x)^")"
  let list ?(sep="; ") dump list =
    "["^(SUtils.catmap sep dump list)^"]"
  let array ?(sep="; ") dump array =
    "[|"^(SUtils.catmap sep dump (Array.to_list array))^"|]"
  let unit () = "()"
  let bool ?(t="true") ?(f="false") =
    function true -> t | false -> f
  let int = string_of_int
  let float = string_of_float
  let pair dumpA dumpB (a, b) =
    "("^(dumpA a)^", "^(dumpB b)^")"
  let ( * ) = pair
  let trio dumpA dumpB dumpC (a, b, c) =
    "("^(dumpA a)^", "^(dumpB b)^", "^(dumpC c)^")"
  let quad dumpA dumpB dumpC dumpD (a, b, c, d) =
    "("^(dumpA a)^", "^(dumpB b)^", "^(dumpC c)^", "^(dumpD d)^")"
  let ignore _ = " _ "
end

module OfS =
struct
  type 'a t = string -> 'a
  let string item : string = item
  let unit = function
    | "()" -> ()
    | _ -> assert false
  let bool = function
    | "true"  -> true
    | "false" -> false
    | _ -> assert false
  let int = int_of_string
  let float = float_of_string
end

module O3S =
struct
  type 'a t = ('a ToS.t) * ('a OfS.t)
  let string = (ToS.string, OfS.string)
  let unit = (ToS.unit, OfS.unit)
  let bool = ((fun b -> ToS.bool b), OfS.bool)
  let int = (ToS.int, OfS.int)
  let float = (ToS.float, OfS.float)
end

module OfFile =
struct
  open Extra
  open Tree

  let unpack_text text =
    let x = String.get text 0 in
    let y = String.sub text 1 (String.length text - 2) in
    let z = String.get text (String.length text - 1) in
    assert(x = '"');
    assert(z = '"');
    Scanf.unescaped y

  let load_leaf strlist =
    let rec aux carry = function
      | []  -> assert false
      | head::tail -> match head with
        | "]"  -> Leaf (String.concat " " (List.rev carry)), tail
        | text  -> aux ((unpack_text text)::carry) tail
    in
    aux [] strlist

  let rec load_tree strlist =
    let rec aux carry = function
      | []  -> assert false
      | head::tail -> match head with
        | ")"  -> Node (List.rev carry), tail
        | "("  -> let tree, tail' = load_tree tail in aux (tree::carry) tail'
        | "["  -> let leaf, tail' = load_leaf tail in aux (leaf::carry) tail'
        | text  -> aux ((Leaf (unpack_text text))::carry) tail
    in aux [] strlist

  let load text =
    let strlist = List.filter (function "" -> false | _ -> true) (SUtils.split ' ' (SUtils.explode text ||> (function ' ' | '\n' | '\t' -> ' ' | x -> x) |> SUtils.implode)) in
    let rec aux carry = function
      | [] -> List.rev carry
      | head::tail -> match head with
        | "("  -> let tree, tail' = load_tree tail in aux (tree::carry) tail'
        | "["  -> let leaf, tail' = load_leaf tail in aux (leaf::carry) tail'
        | text  -> aux ((Leaf (unpack_text text))::carry) tail
    in aux [] strlist

  let stream_to_stree stream =
    let pull () =
      try        Some(Stream.next stream)
      with _ -> None
    in
    let blank = function ' ' | '\t' | '\n' -> true | _ -> false in
    let parse_word () =
      let rec aux carry = match pull () with
        | None -> failwith "[ocaml-tools/STools/OfFile:stream_to_stree:parse_word] parsing error"
        | Some head -> match head with
          | '\\' -> let c = pull () |> Tools.unop in aux ('\\'::c::carry)
          | '"'  -> Scanf.unescaped (SUtils.implode (List.rev carry))
          |  c   -> aux (c::carry)
      in aux []
    in
    let parse_leaf () =
      let rec aux carry = match pull () with
        | None -> failwith "[ocaml-tools/STools/OfFile:stream_to_stree:parse_leaf] parsing error : 0"
        | Some head -> match head with
          | '"' -> let word = parse_word () in aux (word::carry)
          | ']' -> String.concat " " (List.rev carry)
          |  c when blank c -> aux carry
          | _ -> failwith "[ocaml-tools/STools/OfFile:stream_to_stree:parse_leaf] parsing error : 1"
      in aux []
    in
    let rec parse_node () =
      let rec aux carry = match pull () with
        | None -> List.rev carry
        | Some head ->
        ( match head with
          | '(' -> let anode = parse_node () in aux ((Node anode)::carry)
          | '[' -> let aleaf = parse_leaf () in aux ((Leaf aleaf)::carry)
          | '"' -> let aword = parse_word () in aux ((Leaf aword)::carry)
          | ')' -> List.rev carry
          |  c when blank c -> aux carry
          | _ -> failwith "[ocaml-tools/STools/OfFile:stream_to_stree:parse_node] parsing error"
        )
      in aux []
    in
    parse_node ()

  let of_file target =
    let file = open_in target in
    let stree = file |> Stream.of_channel |> stream_to_stree in
    close_in file;
    stree
end

module ToSTree =
struct
  type 'a t = 'a -> Tree.stree
  let leaf map item = Tree.Leaf(map item)

  let map f dump item = dump(f item)

  let string = leaf ToS.string
  let bool   = leaf ToS.bool
  let int    = leaf ToS.int
  let float  = leaf ToS.float
  let option dump = function
    | None      -> Tree.Node []
    | Some some -> Tree.Node [dump some]
  let list dump list = Tree.Node (Tools.map dump list)
  let array dump array = list dump (Array.to_list array)
  let unit () = Tree.Node []
  let pair dumpA dumpB (a, b) = Tree.Node [dumpA a; dumpB b]
  let ( * ) = pair
  let trio dumpA dumpB dumpC (a, b, c) = Tree.Node [dumpA a; dumpB b; dumpC c]

  let file target = OfFile.of_file target
end

module ToFile =
struct
  open Extra
  open Tree

  let dump_leaf text =
    match SUtils.split ' ' (String.escaped text) with
      | []  -> "\"\""
      | [x]  -> "\""^x^"\""
      | lx  -> "[ \""^(String.concat "\" \"" lx)^"\" ]"

  let rec dump_tree = function
    | Leaf text -> dump_leaf text
    | Node treelist -> String.concat " " ("("::(treelist ||> dump_tree)@[")"])

  let output_tree output_string =
    let rec aux = function
      | Leaf text -> output_string(dump_leaf text)
      | Node treelist -> output_string "( "; List.iter (fun tree -> aux tree; output_string " ") treelist; output_string " )";
    in aux

  let to_bytes_size (t:stree) : int =
    let rec tnode = function
      | Leaf text -> 2 + String.length text (* '"text"' *)
      | Node treelist -> 1 + tlist treelist
    and     tlist = function
      | [] -> 2 (* ' )' *)
      | x::tl -> 1 + tnode x + tlist tl (* ' x.tl' *)
    in tnode t

  let to_bytes_put_char (s:bytes) (src:char) (pos:int) : int =
      Bytes.set s pos src;
      (pos + 1)

  let to_bytes_put_string (s:bytes) (src:string) (pos:int) : int =
      let len = String.length src in
      Bytes.blit_string src 0 s pos len;
      (pos + len)

  let rec to_bytes_put_stree (s:bytes) (t:stree) (pos:int) : int =
    let rec to_bytes_put_tnode (s:bytes) (node:stree) (pos:int) =
      match node with
      | Leaf text -> (
        pos
        |> to_bytes_put_char s '"'
        |> to_bytes_put_string s text
        |> to_bytes_put_char s '"'
      )
      | Node treelist -> (
        pos
        |> to_bytes_put_char s '('
        |> to_bytes_put_tlist s treelist
      )
    and     to_bytes_put_tlist (s:bytes) (tl:stree list) (pos:int) =
      match tl with
      | [] -> (
        pos
        |> to_bytes_put_string s " )"
      )
      | hd::tl -> (
        pos
        |> to_bytes_put_char s ' '
        |> to_bytes_put_tnode s hd
        |> to_bytes_put_tlist s tl
      )
    in to_bytes_put_tnode s t pos

  let to_bytes (t:stree) : bytes =
    let len = to_bytes_size t in
    let s = Bytes.make len '\000' in
    let pos = to_bytes_put_stree s t 0 in
    assert(pos = len);
    s

  let to_string (t:stree) : string =
    to_bytes t |> Bytes.unsafe_to_string

  let output_treelist output_string treelist =
    List.iter (fun tree -> output_tree output_string tree; output_string "\n") treelist

  let dump treelist = SUtils.catmap "\n" dump_tree treelist

  let to_file treelist target =
    let file = open_out target in
    output_treelist (output_string file) treelist;
    close_out file;
    ()

  let print_tree = output_tree print_string
  let print_treelist = output_treelist print_string

  let pretty_output_tree output_string =
    let lvlstr lvl text = output_string ((SUtils.ntimes " " lvl)^text^"\n") in
    let rec aux lvl = function
      | Leaf text -> lvlstr lvl text
      | Node treelist -> lvlstr lvl "("; List.iter (aux(lvl+1)) treelist; lvlstr lvl ")"
    in aux 0

  let pprint_v1 treelist =
    List.iter (fun tree -> pretty_output_tree print_string tree; print_newline()) treelist

  (* get colored version of str *)
  let colorize color str =
    if color > 0
    then "\027[" ^ (string_of_int color) ^ "m" ^ str ^ "\027[0m"
    else str

  type enum =
    | T000
    | T001
    | T010
    | T011
    | T100
    | T101
    | T110
    | T111

  (* print colored tree *)
  let pretty_output_treelist_v2 output_string =
    (* draw UTF-8 tree line *)
    let conv = function
      | T000 -> "  "
      | T001 -> "┌ "
      | T010 -> "──"
      | T011 -> "┌─"
      | T100 -> "└ "
      | T101 -> "│ "
      | T110 -> "└─"
      | T111 -> "├─"
    in
    let output_row row =
      output_string (SUtils.catmap""conv(List.rev row));
    in
    let rec tree row0 rows = function
      | Leaf leaf  -> output_row row0; output_string " "; output_string leaf; output_string "\n";
      | Node liste  -> match liste with
        | []         -> output_row row0; output_string "|\n";
        | [head]     -> tree (T010::row0) (T000::rows) head
        | head::tail -> tree (T011::row0) (T101::rows) head; treelist rows tail
    and treelist row = function
      | head::[]     -> tree (T110::row) (T000::row) head
      | head::tail   -> tree (T111::row) (T101::row) head; treelist row tail
      | []           -> ()
    in List.iter (tree [] [])

  let pprint_v2 treelist = pretty_output_treelist_v2 print_string treelist; print_newline()
end

module OfSTree =
struct
  open Tree
  type 'a t = Tree.stree -> 'a

  let map f load item = f(load item)

  let leaf map = function
    | Leaf leaf -> map leaf
    | _ -> failwith "[ocaml-tools/sTL:leaf] - parsing error"

  let string = leaf OfS.string
  let bool   = leaf OfS.bool
  let int    = leaf OfS.int
  let float  = leaf OfS.float
  let option load = function
    | Node [] -> None
    | Node [some] -> Some(load some)
    | _ -> failwith "[ocaml-tools/sTL:option] - parsing error"
  let list load = function
    | Node list -> Tools.map load list
    | Leaf _ -> failwith "[ocaml-tools/sTL:list] - parsing error"
  let array load stree = list load stree |> Array.of_list
  let unit = function
    | Node [] -> ()
    | _ -> failwith "[ocaml-tools/sTL:unit] - parsing error"
  let pair loadA loadB = function
    | Node [a; b] -> (loadA a, loadB b)
    | _ -> failwith "[ocaml-tools/sTL:pair] - parsing error"
  let ( * ) = pair
  let trio loadA loadB loadC = function
    | Node [a; b; c] -> (loadA a, loadB b, loadC c)
    | _ -> failwith "[ocaml-tools/sTL:trio] - parsing error"

  let file treelist target = ToFile.to_file treelist target
  let print treelist = ToFile.print_treelist treelist
  let pprint treelist =  ToFile.pprint_v2 treelist

  let to_bytes t = ToFile.to_bytes t
  let to_string t = ToFile.to_string t
end

module SOut =
struct
  type 'a t = (string -> unit) -> 'a -> unit

  let string p s = p s
  let pretty_option ?(some="Some ") ?(none="None") pa p = function
    | Some a -> p some; pa p a
    | None   -> p none

  let option p s = pretty_option p s
  let pretty_list ?(nil="[]") ?(head="[ ") ?(tail=" ]") ?(sep="; ") (pa:'a t) p l =
    match l with
    | [] -> p nil
    | h::t -> p head; pa p h; List.iter (fun x -> p sep; pa p x) t; p tail
  let list pa p la = pretty_list pa p la
  let pretty_array ?(nil="[| |]") ?(head="[| ") ?(tail=" |]") ?(sep="; ") (a:'a t) p l =
    pretty_list ~nil ~head ~tail ~sep a p (Array.to_list l)
  let array pa p aa = pretty_array pa p aa
  let unit p l = p "()"
  let pretty_bool ?(t="true") ?(f="false") p =
    function true -> p t | false -> p f
  let bool p b = pretty_bool p b
  let int p x = p (string_of_int x)
  let float p x = p (string_of_float x)
  let pair a b p (xa, xb) =
    a p xa; b p xb
  let ( * ) = pair
  let trio pa pb pc p (a, b, c) =
    pa p a; pb p b; pc p c

  let file (pa:'a t) (target:string) (a:'a) : unit =
    let file = open_out target in
    pa (output_string file) a;
    close_out file;
    ()

  let print (pa:'a t) (a:'a) : unit =
    pa print_string a;
    ()
end

