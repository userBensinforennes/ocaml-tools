let rec sorted_rec ?(strict=true) ?(next=None) x = function
  | [] -> (
    match next with
    | Some f -> f (Some x)
    | None   -> true
  )
  | y::t -> (
    (if strict then (x<y) else (x<=y)) &&
    (sorted_rec ~strict ~next y t)
  )

(* return true iff the input list is strictly increasing *)
(* Time Complexity O(n) *)
let sorted ?(strict=true) ?(next=None) = function
  | [] -> (
    match next with
    | Some f -> f None
    | None   -> true
  )
  | x::t -> sorted_rec ~strict ~next x t

(* return true iff the input list is strictly increasing *)
(* checks that the first element is greater than 0 *)
(* Time Complexity O(n) *)
let sorted_nat ?(strict=true) ?(next=None) = function
  | [] -> (
    match next with
    | Some f -> f None
    | None   -> true
  )
  | x::t -> (0<=x)&&(sorted_rec ~strict ~next x t)

let sort (l:'a list) : 'a list =
  List.sort_uniq Stdlib.compare l

let sort (l:'a list) : 'a list =
  List.sort_uniq Stdlib.compare l

(* merges two already sorted list and returns a sorted list *)
(* removes duplicates *)
(* Time Complexity O(nX + nY) *)
(* Tail-Recursive *)
let union lX lY : _ list =
  let rec aux carry = function
  | ([], l) | (l, []) ->
    List.rev_append carry l
  | (((x::x') as lx), ((y::y') as ly)) -> if x = y
    then aux (x::carry) (x', y')
    else if x < y
    then aux (x::carry) (x', ly)
    else aux (y::carry) (lx, y')
  in
  aux [] (lX, lY)

(* merges a list of already sorted list and returns a sorted list *)
(* removes duplicates *)
(* Time Complexity O(\sum_i |l_i|) *)
(* Tail-Recursive *)
let union_list ll = List.fold_left union [] ll

let minus (lX:'a list) (lY:'a list) : 'a list =
(* returns [lX] - [lY] *)
(* Time Complexity O(nX+nY) *)
  let rec aux carry = function
    | ([], _) -> (List.rev carry)
    | (x, []) -> (List.rev_append carry x)
    | (x::x', y::y') -> if x = y
      then (aux carry (x', y'))
      else if x < y
      then (aux (x::carry) (x', y::y'))
      else (aux carry (x', y'))
  in aux [] (lX, lY)

let inter lX lY =
(* return [lX] inter [lY] *)
(* Time Complexity O(nX+nY) *)
  let rec aux carry = function
    | ([], _) | (_, []) ->
      (List.rev carry)
    | (x::x', y::y') -> if x = y
      then (aux (x::carry) (x', y'))
      else (aux carry (if x < y
      then (x', y::y')
      else (x::x', y')))
  in aux [] (lX, lY)

(* returns (lX inter lY) = emptyset *)
(* Time Complexity O(nX+nY) *)
let rec nointer lX lY =
  match lX, lY with
  | [], _ | _, [] -> true
  | x::lX', y::lY' ->
         if x = y then false
    else if x < y
      then nointer lX' lY
      else nointer lX  lY'

(* return true iff [lX] is a subset of [lY] *)
(* Time Complexity O(nX+nY) *)
let rec subset_of lX lY =
  match lX, lY with
  | [], _ -> true
  | _, [] -> false
  | x::lX', y::lY' -> (
         if x = y
      then subset_of lX' lY'
    else if x < y
      then false
      else subset_of lX  lY'
  )

(* if fst-equal, elements of [l2] replace elements of [l1]
 *)
(* Section. Fst Cmp *)
let union_fst_replace
    ?(carry=[])
    (l1:('a*'b)list) (l2:('a*'b)list) : ('a*'b)list =
  let rec loop carry l1 l2 =
    match l1, l2 with
    | [], l
    | l, [] -> List.rev_append carry l
    | (((a1, _)as x1)::l1'), (((a2, _) as x2)::l2') ->
           if a1 = a2
        then loop carry l1' l2
      else if a1 < a2
        then loop (x1::carry) l1' l2
        else loop (x2::carry) l1  l2'
  in loop carry l1 l2

(* if fst-equal, elements of [l2] are put after
   elements of [l1]
 *)
let union_fst_stable
    ?(carry=[])
    (l1:('a*'b)list) (l2:('a*'b)list) : ('a*'b)list =
  let rec loop carry l1 l2 =
    match l1, l2 with
    | [], l
    | l, [] -> List.rev_append carry l
    | (((a1, _)as x1)::l1'), (((a2, _) as x2)::l2') ->
           if a1 <= a2
        then loop (x1::carry) l1' l2
        else loop (x2::carry) l1  l2'
  in loop carry l1 l2

let union_fst ?(replace=true) ?(carry=[]) l1 l2 =
  if replace
  then union_fst_replace ~carry l1 l2
  else union_fst_stable  ~carry l1 l2
