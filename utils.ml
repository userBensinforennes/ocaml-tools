open STools
open BTools

exception StrError of string

type ('edge, 'node) merge =
  | MEdge of 'edge
  | MNode of 'node

let dump_merge dump_medge dump_mnode merge stream = match merge with
  | MEdge medge -> false::(dump_medge medge stream)
  | MNode mnode -> true ::(dump_mnode mnode stream)

let load_merge load_medge load_mnode merge = function
  | false::stream ->
  (
    let medge, stream = load_medge stream in
    (MEdge medge, stream)
  )
  | true ::stream ->
  (
    let mnode, stream = load_mnode stream in
    (MNode mnode, stream)
  )
  | _ -> assert false

let o3s_merge (dump_medge, load_medge) (dump_mnode, load_mnode) =
(
  dump_merge dump_medge dump_mnode,
  load_merge load_medge load_mnode
)

type ('return, 'node) pull_request = ('return, 'node -> 'return) merge

type ('edge, 'node) unmerge = ('edge * 'edge, 'node) pull_request

type ('tag, 'edge, 'node) unmerge_tagged = ('tag * 'edge * 'edge, 'node) pull_request

let string_of_gnode leaf node = function
  | Tree.GLeaf lf -> "Leaf"^(leaf lf)
  | Tree.GLink nd -> "Node"^(node nd)

let bw_gnode bw_leaf bw_node cha =
ToBStream.(function
  | Tree.GLeaf lf -> (
    bool cha false;
    bw_leaf cha lf
  )
  | Tree.GLink nd -> (
    bool cha true;
    bw_node cha nd
  )
)

let br_gnode br_leaf br_node cha =
OfBStream.(
  match bool cha with
  | false -> Tree.GLeaf(br_leaf cha)
  | true  -> Tree.GLink(br_node cha)
)

let gnode_leaf = function
  | Tree.GLeaf leaf -> leaf
  | Tree.GLink _ -> assert false

let gnode_node = function
  | Tree.GLeaf _ -> assert false
  | Tree.GLink node -> node

let dump_gnode dump_leaf dump_node gnode stream = match gnode with
  | Tree.GLeaf leaf -> false::(dump_leaf leaf stream)
  | Tree.GLink node -> true ::(dump_node node stream)

let load_gnode load_leaf load_node = function
  | false::stream ->
  (
    let leaf, stream = load_leaf stream in
    (Tree.GLeaf leaf, stream)
  )
  | true ::stream ->
  (
    let node, stream = load_node stream in
    (Tree.GLink node, stream)
  )
  | _ -> assert false

let o3s_gnode
  ((dump_leaf, load_leaf) : 'leaf IoB.t)
  ((dump_node, load_node) : 'node IoB.t) : ('leaf, 'node) Tree.gnext IoB.t =
(
  dump_gnode dump_leaf dump_node,
  load_gnode load_leaf load_node
)

let kldump_gnode dump_leaf dump_node gnode stream = match gnode with
  | Tree.GLeaf leaf -> let (k, l) = dump_leaf leaf stream in (false::k, l)
  | Tree.GLink node -> let (k, l) = dump_node node stream in (true ::k, l)

let klload_gnode load_leaf load_node (k, l) = match k with
  | false::k -> let leaf, stream = load_leaf (k, l) in Tree.GLeaf leaf
  | true ::k -> let node, stream = load_node (k, l) in Tree.GLink node
  | [] -> assert false

let klo3s_gnode (dump_leaf, load_leaf) (dump_node, load_node) =
(
  kldump_gnode dump_leaf dump_node,
  klload_gnode load_leaf load_node
)

let gnode_is_leaf = function
  | Tree.GLeaf _ -> true
  | Tree.GLink _ -> false

type ('stop, 'left, 'right, 'both) binpull =
  | MStop of 'stop
  | Go0   of 'left
  | Go1   of 'right
  | MPull  of 'both

type ('return, 'edge, 'node) binpath = ('return, 'edge -> 'return, 'edge -> 'return, 'node -> 'return) binpull

type ('edge, 'cons, 'node) merge3 =
  | M3Edge of 'edge
  | M3Cons of 'cons
  | M3Node of 'node

type ('edge, 'node, 'leaf, 'link) node =
  | TNode of ('node * (('edge, 'node, 'leaf, 'link) edge * ('edge, 'node, 'leaf, 'link) edge))
  | TLeaf of 'leaf
  | TLink of 'link
and  ('edge, 'node, 'leaf, 'link) edge = 'edge * ('edge, 'node, 'leaf, 'link) node

type ('pnode, 'tnode) pt_node =
  | PTree of 'pnode
  | TTree of 'tnode

let pnext_of_next = Tree.(function
  | GLeaf leaf -> GLeaf leaf
  | GLink node -> GLink (None, node)
)

let pedge_of_edge (edge, next) =
  (edge, pnext_of_next next)

let pnode_of_node (node, edge0, edge1) =
  (node, pedge_of_edge edge0, pedge_of_edge edge1)

let merge_of_edge (edge, next) = (edge, MEdge next)
let merge3_of_edge (edge, next) = (edge, M3Edge next)

let gnode leaf node = Tree.(function
  | Tree.GLeaf leaf' -> "Leaf ("^(leaf leaf')^")"
  | Tree.GLink node' -> "Node ("^(node node')^")"
)

let merge next node = function
  | MEdge next' -> "MEdge ("^(next next')^")"
  | MNode node' -> "MNode ("^(node node')^")"
let emerge block merge emerge' = ToS.pair block merge emerge'

let merge3 next node = function
  | M3Edge edge' -> "M3Edge ("^(next edge')^")"
  | M3Cons node' -> "M3Cons ("^(node node')^")"
  | M3Node node' -> "M3Node ("^(node node')^")"
let emerge3 block merge3 emerge3' = ToS.pair block merge3 emerge3'

let o3s_gnode
  ((dump_leaf, load_leaf) : 'leaf IoB.t)
  ((dump_node, load_node) : 'node IoB.t) : ('leaf, 'node) Tree.gnext IoB.t =
(
  dump_gnode dump_leaf dump_node,
  load_gnode load_leaf load_node
)

let mp_of_bool = function
  | true  -> "-"
  | false  -> "+"
let mp_char_of_bool = function
  | true  -> '-'
  | false -> '+'
let pm_of_bool = function
  | true  -> "+"
  | false  -> "-"
let pm_char_of_bool = function
  | true  -> '+'
  | false -> '-'
let bool_of_pm = function
  | "+" -> true
  | "-" -> false
  | _ -> assert false
let bool_of_mp = function
  | "+" -> false
  | "-" -> true
  | _ -> assert false
let bool_of_pm_char = function
  | '+' -> true
  | '-' -> false
  | _ -> assert false
let bool_of_mp_char = function
  | '+' -> false
  | '-' -> true
  | _ -> assert false

let consensus (f : 'a * 'b -> 'c * (('d * 'e) option)) (x : 'a list) (y : 'b list) : (('c list) * (('d list) * ('e list))) =
  let rec aux carryC carry0 carry1 = function
    | ([], []) -> (List.rev carryC, (List.rev carry0, List.rev carry1))
    | (x::x', y::y') ->
    (
      let xC, opx01 = f (x, y) in
      let carry0, carry1 = match opx01 with
      | Some (x0, x1) -> (x0::carry0, x1::carry1)
      | None -> (carry0, carry1) in
      aux (xC::carryC) carry0 carry1 (x', y')
    )
    | [], _ | _, [] -> assert false
  in aux [] [] [] (x, y)

let consensus0 f x y =
  let rec aux carryC carry0 = function
    | ([], []) -> (List.rev carryC, List.rev carry0)
    | (x::x', y::y') ->
    (
      let xC, opx0 = f (x, y) in
      let carry0 = match opx0 with
      | Some x0 -> x0::carry0
      | None    ->     carry0 in
      aux (xC::carryC) carry0 (x', y')
    )
    | [], _ | _, [] -> assert false
  in aux [] [] (x, y)

let consensus2 f x y =
  let xy, xy' = List.split(Tools.map f (List.combine x y)) in
  let x', y' = List.split xy' in
  xy, (MyList.unop x'), (MyList.unop y')

let consensus3 f x y =
  let x', y' = List.split(Tools.map f (List.combine x y)) in
  (MyList.unop x', MyList.unop y')

let compose s lC lc =
  let rec aux carry = function
    | [] -> (function
      | [] -> (List.rev carry)
      | _ -> assert false
    )
    | x::x' -> if x = s
      then (function
        | []     -> assert false
        | y::y'  -> aux (y::carry) x' y'
      )
      else (fun y' -> aux (x::carry) x' y')
  in aux [] lC lc

type peval  = bool option list
type opeval = peval option

type quant = bool list
type opquant = quant option

let stree_of_peval = ToS.(list (option bool))
let stree_of_opeval = ToS.option stree_of_peval

let reduce_opeval : opeval -> opeval = function
  | Some peval when List.exists Tools.isSome peval -> Some peval
  | _ -> None

let preduce_pnode = function
  | Tree.GLeaf leaf -> Tree.GLeaf leaf
  | Tree.GLink (opeval, node) -> Tree.GLink(reduce_opeval opeval, node)

let preduce_pedge (edge, pnode) = (edge, preduce_pnode pnode)

open IterExtra

let gen_peval n =  (Iter.of_list [None; Some false; Some true]) $^ n;;

let compose_peval : peval -> peval -> peval = fun pevalC pevalc -> compose None pevalc pevalC

let compose_opeval opevalC opevalc = match (opevalC, opevalc) with
  | Some pevalC, Some pevalc -> Some(compose_peval pevalC pevalc)
  | Some peval, None
  | None, Some peval -> Some peval
  | None, None -> None

let default_eval_node eval_edge = function
  | [] -> assert false
  | head::peval -> match head with
    | None       -> fun ((), edge0, edge1) ->
      MNode((), eval_edge peval edge0, eval_edge peval edge1)
    | Some false -> fun((), edge0, _) ->
      MEdge(eval_edge peval edge0)
    | Some true  -> fun((), _, edge1) ->
      MEdge(eval_edge peval edge1)

