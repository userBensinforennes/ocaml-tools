(*
All Right Reserved

Copyright (c) 2020
  Joan Thibault
    - joan.thibault@ens-rennes.fr
*)

(** Simple Extension to OCaml's Standard Librairy's
   [Array]
 **)

(*  [rev a = ra]

    semantic :
      [Array.to_list(rev a) = List.rev(Array.to_list a)]

    ensures [rev a != a] (at physical level)
      except for a = [| |]
    no side-effect
*)
val map_of_list : ('a -> 'b) -> 'a list -> 'b array
val map_of_rev_list : ('a -> 'b) -> 'a list -> 'b array
val rev : 'a array -> 'a array
val count : ('a -> bool) -> 'a array -> int
val counti : (int -> 'a -> bool) -> 'a array -> int
val count_true  : bool array -> int
val count_false : bool array -> int
val count_Some  : 'a option array -> int
val count_None  : 'a option array -> int
val count_Ok    : ('a, 'b) result array -> int
val count_Error : ('a, 'b) result array -> int
val list_of_indexes : ('a -> bool) -> 'a array -> int list
val list_of_indexes_true : bool array -> int list
val list_of_indexesi : (int -> 'a -> bool) -> 'a array -> int list
val indexes : ('a -> bool) -> 'a array -> int array
val indexes_true : bool array -> int array
val indexesi : (int -> 'a -> bool) -> 'a array -> int array
(*  [of_indexes len il = ba]
      where [il] is unsorted list of positive integers
      require \forall i. 0 <= il_i < len
 *)
val of_indexes : int -> int list -> bool array
val flatten : 'a array array -> 'a array
val unop : 'a option array -> 'a array
(*  [map_unop opa = a]
    apply [Tools.unop] to each elements
    raise AssertError if there is a None in [opa]
 *)
val map_unop : 'a option array -> 'a array
(*  [unop_rename opa = (a, rename)]
    where :
    - [a] is the sub-array of [opa] containing [Some _] elements
    - [rename] if a vector which associates to each index
      its newposition in [a], -1 otherwise
 *)
val unop_rename : 'a option array -> ('a array) * int array
val find_prev : ('a -> bool) -> 'a array -> int -> int option
val find_prev_Some : 'a option array -> int -> (int * 'a) option
val bool_rename : ('a -> bool) -> 'a array -> int option array * int
val cumul_bool : ('a -> bool) -> 'a array -> int array * int
val cumul_bool_list : ('a -> bool) -> 'a list -> int array * int
val for_all2 : ('a -> 'b -> bool) -> 'a array -> 'b array -> bool
val for_all_true : bool array -> bool
val map_find_first2 : ('a -> 'b -> 'c option) -> 'a array -> 'b array -> 'c option
val sum : ('a -> int) -> 'a array -> int
val assoc : 'a -> ('a * 'b) array -> 'b
val pprint_array : ('a -> string) -> 'a array -> unit
val for_alli : (int -> 'a -> bool) -> 'a array -> bool
val between : 'a -> 'a -> 'a array -> bool
val is_permut_assert_bound : int array -> bool

(*  [is_permut a]
    returns true iff [a] is a permutation of {0, ..., n-1}
    with n = Array.length a
    remarq : [is_permut [| |] = true]

    linear time (single pass),
    linear memory overhead
    no side-effect
*)
val is_permut : int array -> bool

