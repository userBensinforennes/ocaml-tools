(* All Right Reserved

   Copyright (c) 2020 Joan Thibault
*)
open Extra
open STools
open BTools

type vertex = {
  index : int;
  edges : int list;
}

type graph = vertex list

let get_vertices (graph:graph) : int list =
  graph
  ||> (fun v -> v.index)
  |> Tools.check SetList.sorted_nat

(* [LATER]
let to_GraphLA (graph:graph) : GraphLA.graph =
  let vertices = get_vertices graph in
  let
 *)

