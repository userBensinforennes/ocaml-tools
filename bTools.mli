module BChar : sig
  val get : char -> int -> bool
(** [get c i] with [0 <= i < 8] returns the bit located at the [i]th position of [c]
 **)
  val unsafe_get : char -> int -> bool
(** [unsafe_get c i = get c i] but does not check for [i]'s value
 **)

  val set : char -> int -> bool -> char
(** [set c i b = c'] with [0 <= i < 8]
    ensures [forall j\in[0, 8[, j <> i -> get c' j = get c j]
    ensures [get c' i = b]
 **)
  val unsafe_set : char -> int -> bool -> char
(** [unsafe_set c i b = set c i b] but does not check for [i]'s value
 **)

  val print : char -> unit
(** [print c > s]
    ensures [String.length s = 8]
    ensures  [forall i\in[0, 8[,
      s.[i] = if get c i then '1' else '0']
 **)
  val to_bool_array : char -> bool array
(** [to_bool_array c = ba]
    ensures [Array.length ba = 8]
    ensures [forall i\in[0, 8[,
      ba.(i) = get c i
 **)
  val of_bool_array : bool array -> char
(** [of_bool_array ba = c] with [Array.length ba = c]
    ensures [forall i\in[0, 8[,
      get c i = ba.(i)
    ensures [of_bool_array(to_bool_array c) = c]
    ensures [Array.length ba = 8 ->
      to_bool_array(of_bool_array ba) = ba]
 **)

  val rev8 : char -> char
(** [rev8 c = c']
    ensures [forall i\in[0, 8[, get c' i = get c (7-i)]
    ensures [rev8(rev8 c) = c]
 **)
  val pop8 : char -> int
(** [pop8 c = n]
    ensures \sum(1_{get c i} | i\in[0, 8[)
 **)

  val low_mask : int -> char
(** [low_mask i = c] with [i\in[0, 8[]
    ensures [forall j\in[0, 8[, get c j = j < i]
 **)
  val unsafe_low_mask : int -> char
(** [unsafe_low_mask i = low_mask i] but does not check [i]'s value
 **)

  val fill : char -> int -> int -> bool -> char
(** [fill_ones c fst len cst = c'] with [0 <= fst <= 8-len]
    ensures [forall i\in[0, 8[,
      get c' i = if i\in[fst, fst+len[
        then cst else (get c i)]
 **)

  val fill_ones : char -> int -> int -> char
(** [fill_ones c fst len = fill c fst len true]
 **)
  val unsafe_fill_ones : char -> int -> int -> char
(** [unsafe_fill_ones c fst len = fill_ones c fst len] bot does not check for [fst]'s and [len]'s consistency
 **)

  val fill_zeros : char -> int -> int -> char
(** [fill_ones c fst len = fill c fst len false]
 **)
  val unsafe_fill_zeros : char -> int -> int -> char
(** [unsafe_fill_zeros c fst len = fill_zeros c fst len] bot does not check for [fst]'s and [len]'s consistency
 **)

  val fst8_true_opt  : char -> int option
(** [fst8_true_opt c = op]
    ensures [c = '\0' <-> op = None]
    ensures [op = Some i <->
      get c i = true /\ forall j < i, get c j = false]
 **)
  val fst8_false_opt : char -> int option
(** [fst8_false_opt c = op]
    ensures [c = '\255' <-> op = None]
    ensures [op = Some i <->
      get c i = false /\ forall j < i, get c j = true]
 **)
  val lst8_true_opt  : char -> int option
(** [lst8_true_opt c = op]
    ensures [c = '\0' <-> op = None]
    ensures [op = Some i <->
      get c i = true /\ forall j > i, get c j = false]
 **)
  val lst8_false_opt : char -> int option
(** [lst8_false_opt c = op]
    ensures [c = '\255' <-> op = None]
    ensures [op = Some i <->
      get c i = false /\ forall j > i, get c j = true]
 **)

  val fst8_true  : char -> int
(** [fst8_true c = match fst8_true_opt c with
      | Some n -> n
      | None   -> raise Invalid_argument
 **)
  val fst8_false : char -> int
(** [fst8_false c = match fst8_false_opt c with
      | Some n -> n
      | None   -> raise Invalid_argument
 **)
  val lst8_true  : char -> int
(** [lst8_true c = match lst8_true_opt c with
      | Some n -> n
      | None   -> raise Invalid_argument
 **)
  val lst8_false : char -> int
(** [lst8_false c = match lst8_false_opt c with
      | Some n -> n
      | None   -> raise Invalid_argument
 **)

  val unsafe_fst8_true  : char -> int
(** [unsafe_fst8_true c = fst8_true c] with [c <> '\0']
 **)
  val unsafe_fst8_false : char -> int
(** [unsafe_fst8_false c = fst8_false c] with [c <> '\255']
 **)
  val unsafe_lst8_true  : char -> int
(** [unsafe_lst8_true c = lst8_true c] with [c <> '\0']
 **)
  val unsafe_lst8_false : char -> int
(** [unsafe_lst8_false c = lst8_false c] with [c <> '\255']
 **)
end

module BArray : sig

  type t
(** type of a BArray
 **)

  val checked : t -> bool
(** [checked t] returns true iff the BArray is properly formed, using only safe operations conserves this property, however, it can become handy if you're debugging code which uses unsafe operations
 **)

  val length : t -> int
(** [length t] returns the length of [t] in bits, we denode [I(t) = [0, length t]] the set of valid indexes
 **)
  val size : t -> int
(** [size t] returns the length of [t] in bytes (i.e. round_up ([length t]/8) ) **)
  val max_length : int
(** [max_length] is the maximal length for a BArray
 **)
  val create : int -> t
(** [create n] returns a new BArray of length [n].
    The sequence is uninitialized and contains arbitrary values.

    Raise [Invalid_argument] if [n < 0] or [n > ]{!max_length}.
 **)

  val make : int -> bool -> t
(** [ba = make n b] return a BArray of length [n]
    ensures [forall i, get ba i = b]
 **)
  val copy : t -> t
(** [copy t = t'] return a copy of [t]
    ensures [length t' = length t]
    ensures [forall i\in I(t'), get t' i = get t i]
 **)
  val get : t -> int -> bool
(** [get t i] with [i\in I(t)] returns the bit located at the [i]th position of [t]
 **)
  val set : t -> int -> bool -> unit
(** [set t i b] sets the bit located at the [i]th position of [t] to the value [b]
    ensures [forall j\inI(t), i <> j -> (set t i b; get t j) = get t j]
    ensures [(set t i b; get i) = b]
 **)

  (* unsafe_* means that no consistency check is performed,
     this may lead to silent errors, core dump, etc. *)

  val unsafe_get : t -> int -> bool
  val unsafe_set : t -> int -> bool -> unit

  val init : int -> (int -> bool) -> t
(** [init n f = t] returns the BArray [t] initialized with [f(0); f(1); ...; f(n-1)] in this order
    ensures [(init n f) = (of_bool_array(Array.init n f))]
 **)
  val random : int -> t
(** [random n = t] return a random (uniform distribution) BArray of length [n]
    ensures [length t = n]
 **)

  val unsafe_fill_ones : t -> int -> int -> unit
(** [unsafe_fill_ones t fst len]
     in-place
 **)
  val unsafe_fill_zeros : t -> int -> int -> unit
(** [unsafe_fill_zeros t fst len]
     in-place
 **)
  val naive_fill : t -> int -> int -> bool -> unit
(** [naive_fill t fst len]
     in-place
 **)
  val fill : t -> int -> int -> bool -> unit
(** [fill t fst len b]
     in-place
 **)

  val iter : (bool -> unit) -> t -> unit
(** [iter f t] is semantically equivalent to [Array.iter f (to_bool_array t)]
 **)
  val map : (bool -> bool) -> t -> t
(** [map f t is semantically equivalent to [of_bool_array(Array.map f (to_bool_array t))]
 **)
  val iteri : (int -> bool -> unit) -> t -> unit
(** [iteri f t] is semantically equivalent to [Array.iteri f (to_bool_array t)]
 **)
  val mapi : (int -> bool -> bool) -> t -> t
(** [mapi f t] is semantically equivalent to [of_bool_array(Array.mapi f (to_bool_array t))]
 **)
  val fold_left : ('a -> bool -> 'a) -> 'a -> t -> 'a
(** [fold_left f x t] is semantically equivalent to [Array.fold_left f x (to_bool_array t)]
 **)
  val fold_right : (bool -> 'a -> 'a) -> t -> 'a -> 'a
(** [fold_right f x t] is semantically equivalent to [Array.fold_right f x (to_bool_array t)]
 **)
  val foldi_left : ('a -> int -> bool -> 'a) -> 'a -> t -> 'a
(** [foldi_left f x t] is semantically equivalent to [Array.foldi_left f x (to_bool_array t)]
 **)
  val foldi_right : (int -> bool -> 'a -> 'a) -> t -> 'a -> 'a
(** [fold_right f x t] is semantically equivalent to [Array.fold_right f x (to_bool_array t)]
 **)
  val iteri_true : (int -> unit) -> t -> unit
(** [iteri_true f t] is semantically equivalent to [List.iter f (to_list t)]
 **)

  val blit : t -> int -> t -> int -> int -> unit
(** [blit t0 fst0 t1 fst1 len] with [fst0, fst0+len[ C I(t0) and [fst1, fst1+len[ C I(t1)
    ensures [forall i\in[0, len[, (get (fst1+i) t1' = get (fst0+i) t0)]
    ensures [t0' = t0]
    ensures [forall i\in I(t1)\[fst1, fst1+len[, (get i t1' = get i t1)]
    RQ : t0' (respectively t1') denotes the state of t0 (respectively t1) after applying [blit t0 fst0 t1 fst1 len].
 **)

  val drop_first : t -> int -> t
(** [drop_first t n = t'] with n\in[0, length t]
    ensures [length t' = length t - n]
    ensures [forall i\in I(t'), get i t' = get (n+i) t]
 **)
  val unsafe_drop_first : t -> int -> t
(** [unsafe_drop_first t n = drop_first t n] but does not check for [n]'s value
 **)

  val drop_last : t -> int -> t
(** [drop_last t n = t'] with n\in[0, length t]
    ensures [length t' = length t - n]
    ensures [forall i\in I(t'), get i t' = get i t]
 **)
  val unsafe_drop_last : t -> int -> t
(** [unsafe_drop_last t n = drop_last t n] but does not check for [n]'s value
 **)

  val append_zeros_first : t -> int -> t
(** [append_zeros_first t n = t'] appends [n] false at the beginning of [t]
    ensures [length t' = length t + n]
    ensures [forall i\in I(t), get t' i = get t (n+i)]
    ensures [forall i\in[0, n[, get t' i = false]
    ensures [forall i\in I(t'), get t' i = if i < n then false else (get t (i-n))]
 **)
  val append_zeros_last : t -> int -> t
(** [append_zeros_last t n = t'] appends [n] false at the end of [t]
    ensures [length t' = length t + n]
    ensures [forall i\in I(t), get t' i = get t i]
    ensures [forall i\in[0, n[, get t' (length t + i) = false]
    ensures [forall i\in I(t'), get t' i = if i >= length t then false else (get t i)]
 **)

  val append : t -> t -> t
(** [append t0 t1 = t]
    ensures [length t = length t0 + length t1]
    ensures [forall i \in I(t), get t i = if i < length t0 then (get t0 i) else (get t1 (i-(length t0)))]

    complexity : O(length t)
 **)
  val concat_array : t array -> t
(** [concat_array ta = t]
    ensures [length t = sum_i(length ta.(i))]
    ensures [t = Array.fold_left (fun acc t -> append acc t) [] ta]
    complexity : O(length t + Array.length ta)
 **)
  val concat_list  : t list  -> t
(** [concat_list tl = concat_array (Array.of_list tl)]
    complexity : O(length t + List.length tl)
 **)

  val sub : t -> int -> int -> t
(** [sub t fst len = t'] with [fst, fst+len[ C I(t)
  ensures [length t' = len]
  ensures [forall i\in I(t'), get t' i = get t (fst+i)]
  complexity : O(length t')
 **)
  val unsafe_sub : t -> int -> int -> t
(** [unsafe_sub t fst len = sub fst len] but does not check for [fst]'s and [len]'s values
 **)

  val pop : t -> int
(** [pop t] return the number of [true] in [t]
 **)

  val fst_true  : t -> int option
(** [fst_true t = op]
    ensures [t = make (length t) false <-> op = None]
    ensures [op = Some i <->
      get t i = true /\ forall j < i, get t j = false]
 **)

  val fst_false : t -> int option
(** [fst_false t = op]
    ensures [t = make (length t) true <-> op = None]
    ensures [op = Some i <->
      get t i = false /\ forall j < i, get t j = true]
 **)

  val lst_true  : t -> int option
(** [lst_true t = op]
    ensures [t = make (length t) false <-> op = None]
    ensures [op = Some i <->
      get t i = true /\ forall j > i, get t j = false]
 **)

  val lst_false : t -> int option
(** [lst_false t = op]
    ensures [t = make (length t) true <-> op = None]
    ensures [op = Some i <->
      get t i = false /\ forall j > i, get t j = true]
 **)

(** Bit-Wise Binary Operators *)

  val bw_and : t -> t -> t
(** [bw_and t0 t1 = t] with [length t0 = length t1]
    ensures [length t = length t0 = length t1]
    ensures [forall i\in I(t), get t i = (get t0 i) && (get t1 i)]
 **)
  val bw_and' : t -> t -> t
(** [bw_and' t0 t1 = t] with [length t0 = length t1]
    ensures [t0' = bw_and t0 t1]
    ensures [t == t0']
 **)
  val bw_or : t -> t -> t
(** [bw_or t0 t1] with [length t0 = length t1]
    ensures [length t = length t0 = length t1]
    ensures [forall i\in I(t), get t i = (get t0 i) || (get t1 i)]
 **)
  val bw_or' : t -> t -> t
(** [bw_or' t0 t1 = t] with [length t0 = length t1]
    ensures [t0' = bw_or t0 t1]
    ensures [t == t0']
 **)
  val bw_xor : t -> t -> t
(** [bw_xor t0 t1 = t] with [length t0 = length t1]
    ensures [length t = length t0]
    ensures [forall i\in I(t), get t i = (get t0 i) <> (get t1 i)]
 **)
  val bw_xor' : t -> t -> t
(** [bw_xor' t0 t1 = t] with [length t0 = length t1]
    ensures [t0' = bw_xor t0 t1]
    ensures [t == t0'] (i.e. t == t0)
 **)
  val bw_not : t -> t
(** [bw_node t0 = t]
    ensures [length t = length t0]
    ensures [forall i\in I(t), get t i = not(get t0 i)]
 **)
  val bw_not' : t -> t
(** [bw_not' t0 = t]
    ensures [t0' = bw_not t0]
    ensures [t == t0']
 **)

  val of_list : int list -> t
(** [of_list l = t] iff
      \forall i\in\N, (i\in l <=> t.!(i) = true)
    /\  if l = []
        then length t = 0
        else length t = 1+(max l)
    ensures [t.!(pred(length t)) = true]
 **)
  val of_list_with_length : int list -> int -> t
(** [of_list_with_length l s = t] iff
      \forall i\in\N, (i\in l <=> [t.!(i) = true])
    /\ length t = s
 **)
  val to_list : t -> int list
(** [to_list t = l]
    ensures [sorted l]
    ensures [forall i, i \in l <-> get t i = true]
    ensures [of_list_with_length (to_list t) (length t) = t]
    ensures [of_list (to_list t) = rm_trail negb t]
      (removes tailling [false] bits)
 **)
  val to_stree : t Tree.to_stree
(** [to_stree t] return s-tree representation of [t]
 **)
  val of_stree : t Tree.of_stree
(** [of_stree s = t] with [s] such that [exists t', to_stree t' = s]
    ensures [t = t']
    ensures [of_stree(to_stree t) = t]
    ensures [to_stree(of_stree s) = t]
 **)
  val o3_stree : (t, Tree.stree) O3.o3
  (* not in-place *)
  val rev : t -> t
(** [rev t0 = t]
    ensures [length t = length t0]
    ensures [forall i\in I(t), get t i = get t0 (length t - i)]
    ensures [rev(rev t) = t]
 **)

  (* Conversion to/from string/array/list *)

  val to_string : t -> string
  val of_string : string -> t
(** [of_string s = t] with [s] such that [exist t, s = to_string t]
    ensures [of_string(to_string t) = t]
    ensures [to_string(of_string s) = s] with [s] such that [exist t, s = to_string t]
 **)
  val print : Format.formatter -> t -> unit

  val to_bool_array : t -> bool array
(** [to_bool_array t = ba] with [length t < Array.max_length]
    ensures [Array.length ba = length t]
    ensures [forall i \in I(t), ba.(i) = get t i]
**)
  val of_bool_array : bool array -> t
(** [of_bool_array ba = t] with [Array.length ba < max_length] (should be OK)
    ensures [length t = Array.length ba]
    ensures [forall i \in I(t), get t i = ba.(i)]
    ensures [of_bool_array(to_bool_array t) = t]
    ensures [to_bool_array(of_bool_array a) = a]
 **)

  val to_bool_list : t -> bool list
(** [to_bool_list t = Array.to_list(to_bool_array t)]
 **)
  val of_bool_list : bool list -> t
(** [of_bool_list l = of_bool_array(Array.of_list l)]
 **)

  val to_hexa_string : t -> string
(** [to_hexa_string t = s]
    ensures [s] is the hexadecimal representation of [t] interpreted as a binary number.
 **)
  val of_hexa_string : string -> t
(** [of_hexa_string s = t]
    ensures [t] is the binary representation of [s] interpreted as an hexadecimal number.
    ensures [of_hexa_string(to_hexa_string t) = t]
    ensures [to_hexa_string(of_hexa_string s) = s]
 **)

  val to_bool_string : t -> string
(** [to_bool_string t = s]
    ensures [String.length s = length t]
    ensures [forall i \in I(t), s.[i] = if (get t i) then '1' else '0']
 **)
  val of_bool_string : string -> t
(** [of_bool_string s = t]
    ensures [length t = String.length s]
    ensures [forall i \in I(t), get t i =
      match s.[i] with '1' -> true | '0' -> false | _ -> assert false
 **)

  (* Conversion to/from string/bool array/bool (in reverse order *)

  val to_rev_string : t -> string
(** [to_rev_string t = to_string(rev t)]
 **)
  val of_rev_string : string -> t
(** [of_rev_string s = rev(of_string t)]
 **)
  val print_rev : Format.formatter -> t -> unit
(** [print_rev t = print(rev t)]
 **)

  val to_rev_bool_array : t -> bool array
(** [to_rev_bool_array t = to_bool_array(rev t)]
    ensures [Array.rev (to_bool_array t) = to_bool_array(rev t)]
 **)
  val of_rev_bool_array : bool array -> t
(** [of_rev_bool_array a = rev(of_bool_array a)]
    ensures [of_bool_array(Array.rev a) = rev(of_bool_array a)]
    ensures [to_rev_bool_array(of_rev_bool_array a) = a]
    ensures [of_rev_bool_array(to_rev_bool_array t) = t]
 **)

  val to_rev_bool_list : t -> bool list
(** [to_rev_bool_list t = List.rev (to_bool_list t)]
    ensures [List.rev(to_bool_list t) = to_bool_list(rev t)]
 **)
  val of_rev_bool_list : bool list -> t
(** [of_rev_bool_list l = rev(of_bool_list l)]
    ensures [of_bool_list(List.rev l) = rev(of_bool_list l)]
    ensures [to_rev_bool_list(of_rev_bool_list l) = l]
    ensures [of_rev_bool_list(to_rev_bool_list t) = t]
 **)

  val to_rev_hexa_string : t -> string
  val of_rev_hexa_string : string -> t

  val output_bin : out_channel -> t -> unit
  val input_bin : in_channel -> t

  module Nat :
  sig
    type nat = t

    val sized_zero : int -> t
    val zero : unit -> t
    val sized_unit : int -> t
    val unit : unit -> t

    val normalize : t -> t
    val resize : t -> int -> t
    val truncate : t -> int -> t

    val compare : t -> t -> int
    (* [TODO] reimplement Nat so that natural comparison works as expected *)

    val eq      : t -> t -> bool
    val ( =/ )  : t -> t -> bool
    val lt      : t -> t -> bool
    val ( </ )  : t -> t -> bool
    val gt      : t -> t -> bool
    val ( >/ )  : t -> t -> bool
    val le      : t -> t -> bool
    val ( <=/ ) : t -> t -> bool
    val ge      : t -> t -> bool
    val ( >=/ ) : t -> t -> bool

    (* [TODO] implement bitwise binary operators || and && *)

    (* exception raised when the result is lower than zero *)
    exception CarryOverflow of int
    exception LT0
    exception LowerThanZero of t
    val error_minus : t -> t -> t
    val minus : t -> t -> t
    val ( -/ ) :   t -> t -> t
    val minusi : t -> int -> t
    val add : t -> t -> t
    val ( +/ ) : t -> t -> t
    val addi : t -> int -> t
    val mult : t -> t -> t
    val ( */ ) : t -> t -> t
    val multi : t -> int -> t
    val quomod : t -> t -> t * t
    val quomodi : t -> int -> t * int
    val div : t -> t -> t
    val ( // ) : t -> t -> t
    val divi : t -> int -> t
    val modulo : t -> t -> t
    val ( mod ) : t -> t -> t
    val modi : t -> int -> int
    val succ : t -> t
    val pred : t -> t
    val log2_inf : t -> int
    val log2_sup : t -> int

    val shift_lefti : t -> int -> t
    (* we assume 0 <= [i]
       length(shift_lefti ba i) = (length ba) + [i]
       [shift_lefti ba i] = [ba]*(2^[i])
     *)
    val shift_righti : t -> int -> t
    (* we assume 0 <= [i]
       length(shift_righti ba i) = max((length ba) - [i], 0)
       [shift_righti ba i] = [ba]/(2^[i])
     *)
    val pow2 : int -> t
    (* we assume 0 <= [i] *)

    val to_string : t -> string
    val of_string : string -> t

    val print : t -> unit

    val max : t -> t -> t
    val min : t -> t -> t
    val mm : t -> t -> t * t

    val of_int : int -> t
    val of_sized_int : int -> int -> t
(** [of_sized_int x size]
 **)
    val is_int : t -> bool
    val to_int : t -> int

    val factorial : int -> t

    (*  [to_bicimal t = il]
        ensures  [SetList.sorted_nat il]
     *)
    val to_bicimal : t -> int list
    (*  [of_bicimal il = t]
        requires [SetList.sorted_nat il]
        ensures [to_bicimal(of_bicimal il) = il]
     *)
    val of_bicimal : int list -> t
  end
end

module BNat = BArray.Nat

module OfB :
sig
  type stream = bool list
  type 'a t = stream -> 'a * stream

  val unit : unit t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val c2 : 't0 t -> 't1 t -> ('t0, 't1) Poly.c2 t
  val c3 : 't0 t -> 't1 t -> 't2 t -> ('t0, 't1, 't2) Poly.c3 t
  val c4 : 't0 t -> 't1 t -> 't2 t -> 't3 t -> ('t0, 't1, 't2, 't3) Poly.c4 t
  val choice : 'a t -> 'a t -> 'a t
  val option : 'a t -> 'a option t
  val sized_barray : int -> BArray.t t
  val barray : BArray.t t
  val none_list : 'a option t -> 'a list t
  val while_list : ('e * ('s option)) option t -> 's -> 'e list t
  val sized_list : 'a t -> int -> 'a list t
  val bool : bool t
  val unary : int t
  val sized_int : int -> int t
  val int : int t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t

  val closure : 'a t -> BArray.t -> 'a
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val bool_option_list : bool option list t
end

(* [TODO] rename 'ToB' *)
module ToB :
sig
  type stream = bool list
  type 'a t = 'a -> stream -> stream

  val unit : unit t
  val map : ('a -> 'b) -> 'b t -> 'a t
  val c2 : 't0 t -> 't1 t -> ('t0, 't1) Poly.c2 t
  val c3 : 't0 t -> 't1 t -> 't2 t -> ('t0, 't1, 't2) Poly.c3 t
  val c4 : 't0 t -> 't1 t -> 't2 t -> 't3 t -> ('t0, 't1, 't2, 't3) Poly.c4 t
  val option : 'a t -> 'a option t
  val sized_barray : BArray.t t
  val barray : BArray.t t
  val none_list : 'a option t -> 'a list t
  val sized_list : 'a t -> 'a list t
  val bool : bool t
  val unary : int t
  val sized_int : int -> int t
  val int : int t
  val pair  : 'a t -> 'b t -> ('a*'b) t
  val ( * ) : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t
  val quad : 'a t -> 'b t -> 'c t -> 'd t -> ('a*'b*'c*'d) t

  val closure : 'a t -> 'a -> BArray.t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val bool_option_list : bool option list t
end

module IoB :
sig
  type stream = bool list
  type 'a t = 'a ToB.t * 'a OfB.t
  type 'a b = ('a, BArray.t) O3.o3

  val unit : unit t
  val option : 'a t -> 'a option t
  val bool : bool t
  val barray : BArray.t t
  val list : 'a t -> 'a list t
  val array : 'a t -> 'a array t
  val none_list : 'a option t -> 'a list t
  val int : int t
  val sized_int : int -> int t
  val closure : 'a t -> 'a b
  val bool_option_list : bool option list t
  val pair : 'a t -> 'b t -> ('a*'b) t
  val trio : 'a t -> 'b t -> 'c t -> ('a*'b*'c) t

  val c2 : 't0 t -> 't1 t -> ('t0, 't1) Poly.c2 t
  val c3 : 't0 t -> 't1 t -> 't2 t -> ('t0, 't1, 't2) Poly.c3 t
  val c4 : 't0 t -> 't1 t -> 't2 t -> 't3 t -> ('t0, 't1, 't2, 't3) Poly.c4 t
end

module OfBStream :
sig
  module Channel :
  sig
    type t

    val get_pos : t -> int
      (* returns the number of bits red from the t *)
    val ppos : ?s:string -> t -> unit
      (* prints [s ' [' (get_pos cha) ' ]\n'] on the standard output *)
    val mpos : ?s:string -> t -> unit
      (* set [s ' [' (get_pos cha) ' ]'] on the meta-data t *)

    val set_meta : t -> (string -> unit) -> unit
    val prn_meta : t -> string -> unit

    val open_file : string -> t
    val close_file : t -> unit
    val open_barray : BArray.t -> t
    val close_barray : t -> unit
    val open_spy : (bool -> unit) -> t -> t
    val close_spy : t -> unit
    val open_flush : ?cha:out_channel -> t -> t
    val close_flush : t -> unit

    exception End_of_OfBStream of string
    (* This exception is raised when a stream ends *)
  end

  type 'a t = Channel.t -> 'a
(** generic ['a] reader type **)

(** Each of these reader is designed to specificly work with the writter of the same in {ToBStream}
  Using a different reader, (even if it has the same) has an unspecified behavior and may lead to explicit or silent parsing errors.
**)
  val bool : bool t
(** **)
  val int  : int t
(** **)
  val int_le  : int -> int t
(** [int_le n] reverses a w[int_le n] writter
**)
  val unary : int t
(** **)
  val sized_int : int -> int t
(** [sized_int n] reverses a w[sized_int n] writter **)
  val char : char t
(** **)
  val float : float t (* Not Implemented Yet *)
(** **)
  val string : string t
(** **)
  val bytes : Bytes.t t
(** **)
  val barray : BArray.t t
(** **)
  val option : 'a t -> 'a option t
(** [option br'a] reverses a w[option bw'a] writter
    with [br'a] reversing [bw'a]
**)
  val int_option : int option t
(** **)
  val unit : unit t
(** **)
  val pair : 'a t -> 'b t -> ('a * 'b) t
(** [pair br'a br'b] reverses a w[pair bw'a bw'b] writter
    with [br'a] reversing [bw'a]
    and  [br'b] reversing [bw'a]
**)
  val ( * ) : 'a t -> 'b t -> ('a * 'b) t
(** ( * ) alias for pair **)
  val trio : 'a t -> 'b t -> 'c t -> ('a * 'b * 'c) t
(** [trio br'a br'b br'c] reverses a w[trio bw'a bw'b bw'c] writter
    with [br'a] reversing [bw'a]
    and  [br'b] reversing [bw'b]
    and  [br'c] reversing [bw'c]
**)
  val list : 'a t -> 'a list t
(** [list br'a] reverses a w[list bw'a] writter
    with [br'a] reversing [bw'a]
**)
  val sized_list : 'a t -> int -> 'a list t
(** [sized_list br'a n] reverses a w[sized_list bw'a] writter used on a list of length [n]
    with [br'a] reversing [bw'a]
**)
  val bool_list : bool list t
(** **)
  val bool_option_list : bool option list t
(** **)
  val sized_bool_list : int -> bool list t
(** [sized_bool_list n] reverses a w[sized_bool_list] used on list of length [n]
**)
  val none_list : 'a option t -> 'a list t
(** [none_list br'a] reverses a w[none_list bw'a] writter
    with [br'a] reversing [bw'a]
**)
  val array : 'a t -> 'a array t
(** [array br'a] reverses a w[arrray bw'a] writter
    with [br'a] reversing [bw'a]
**)

  val btree : 'a t -> 'a Tree.btree t
(** [btree br'a] reverses a w[btree bw'a] writter
    with [br'a] reversing [br'a]
**)
  val btree_code : 'a Tree.btree -> 'a t
  val huffman_btree : 'a t -> 'a Tree.btree t
  (* read a Huffman code's book from channel and returns
     a generic channel reader
     ( if no code's book is available,
         returns (fun _ -> assert false)
     )
   *)
  val huffman : 'a t -> Channel.t -> 'a t
  val huffman2 :
    ?post : (unit -> 'a t) option ->
    'a t -> Channel.t -> 'a t
  val dag_core :
    (* find *) (int -> 'link option) ->
    (* add  *) (int -> 'link -> unit) ->
    (* recfun *) ( Channel.t ->
      (int -> 'link * int) ->
       int -> 'link * int ) ->
    (* br_ident *) (int -> int t) ->
    (* Channel.t *) Channel.t -> int -> 'link * int
  val ctx_dag_core :
    (* find *) ('ctx -> int -> 'link option) ->
    (* add  *) ('ctx -> int -> 'link -> unit) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> int -> 'link * int) ->
       'ctx -> int -> 'link * int ) ->
    (* br_ident *) ('ctx -> int -> int t) ->
    (* Channel.t *) Channel.t -> 'ctx -> int -> 'link * int
  val hctx_dag_core :
    (* find *) ('ctx -> int -> 'link option) ->
    (* add  *) ('ctx -> int -> 'link -> unit) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link ) ->
       'ctx -> 'link
      ) ->
    (* br_ident *) ('ctx -> int -> int t) ->
    (* curr_nnode *) ('ctx -> int) ->
    (* incr_nnode *) ('ctx -> int) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link
  val hctx2_dag_core :
    (* find *) ('ctx -> int -> 'link option) ->
    (* add  *) ('ctx -> int -> 'link -> unit) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link ) ->
       'ctx -> 'link
      ) ->
    (* br_ident *) ('ctx -> int -> int option t) ->
    (* curr_nnode *) ('ctx -> int) ->
    (* incr_nnode *) ('ctx -> int) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link
  val hctx3_dag_core :
    (* find *) ('ctx -> int -> 'link option) ->
    (* add  *) ('ctx -> int -> 'link -> unit) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link ) ->
       'ctx -> 'link
      ) ->
    (* br_ident *) ('ctx -> int -> (int, bool) result t) ->
    (* curr_nnode *) ('ctx -> int) ->
    (* incr_nnode *) ('ctx -> int) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link
end

type 'a br = 'a OfBStream.t

module ToBStream :
sig
  module Channel :
  sig
    type t

    val get_pos : t -> int
      (* returns the number of bits written on the t *)
    val ppos : ?s:string -> t -> unit
      (* prints [s '[ ' (get_pos cha) ' ]\n'] on the standard output *)
    val set_meta : t -> (string -> unit) -> unit
    val prn_meta : t -> string -> unit
    val mpos : ?s:string -> t -> unit
      (* set [s ' [' (get_pos cha) ' ]'] on the meta-data t *)
    val get_free : t -> bool
      (* returns [true ] iff the t is available,
                 [false] otherwise
       *)

      (* WARNING : Dot not use [lock] nor [unlock] unless you are sure of what you are doing ! =) *)
    val lock   : t -> unit
    val unlock : t -> unit

    val open_null : unit -> t
    val close_null : t -> unit
    val open_file : string -> t
    (* DO NOT USE [flush_out] (at least for now) *)
    val flush : t -> unit
    val close_file : t -> unit
    val open_barray : unit -> t
    val close_barray : t -> BArray.t
    val open_tee : t -> t -> t
    val close_tee : t -> t * t
    val open_fun : (bool -> unit) -> t
    val close_fun : t -> unit
    val open_flush : ?cha:out_channel -> unit -> t
    val close_flush : t -> unit
    val open_stats : unit -> t
    val close_stats : t -> (string list, int) Hashtbl.t
    val stats_add : t -> string -> (unit -> unit)
    val stats_rem : t -> string -> unit
  end

  type 'a t = Channel.t -> 'a -> unit

  val bool : bool t
(** [bool] writter **)
  val char : char t
(** [cha] writter **)
  val unary : int t
(** [int] writter for positive integers using unary encoding *)
  val sized_int : int -> int t
(** [sized_int n] returns a [int] writter for positive integers lower than [2^n] using [n] bits *)
  val int  : int t
(** [int] writter for positive integers *)
  val int_le  : int -> int t
(** [int_le n] return an [int] writter for positive integers lower than [n] **)
  val float : float t (* Not Implemented Yet *)
(** [float] writter **)
  val string : string t
(** [string] writter *)
  val bytes : Bytes.t t
(** [Bytes] writter **)
  val barray : BArray.t t
(** [BArray] writter **)
  val option : 'a t -> 'a option t
(** [option bw'a] return a ['a option] writter using the ['a] writter [bw'a]
**)
  val int_option : int option t
(** [int option] writter using the bijection between \N+ and \N
**)
  val unit : unit t
(** [unit] writter
**)
  val pair : 'a t -> 'b t -> ('a * 'b) t
(** [pair bw'a bw'b] returns ['a * 'b] writter using the ['a] writter [bw'a] and the ['b] writter [bw'b]
**)
  val ( * ) : 'a t -> 'b t -> ('a * 'b) t
(** ( * ) alias for pair **)
  val trio : 'a t -> 'b t -> 'c t -> ('a * 'b * 'c) t
(** [trio bw'a bw'b bw'c] returns a ['a * 'b * 'c] writter using the ['a] writter [bw'a], the ['b] writter [bw'b] and the the ['c] writter [bw'c]
**)

  val list : 'a t -> 'a list t
(** [list bw'a] returns a ['a list] writter using the ['a] writter [bw'a]
**)
  val sized_list : 'a t -> 'a list t
(** same as [list] but does not write the list's size
**)
  val none_list : 'a option t -> 'a list t
(** same as [list] but uses a ['a option] writter instead
  the [None] is used to represent the end of the list being encoded
**)
  val bool_list : bool list t
(** same as [list bool]
**)
  val bool_option_list : bool option list t
(** same as [list(option bool)] but uses exactly 2 bits per element (plus 1)
**)
  val sized_bool_list : bool list t
(** same as [sized_list bool]
**)

  val array : 'a t -> 'a array t
(** [array bw'a] returns a ['a array] writter using the ['a] writter [bw'a]
**)

  val btree : 'a t -> 'a Tree.btree t
(** [btree bw'a] returns a ['a btree] writter using the ['a] writter [bw'a]
**)
  val btree_code : ?pf:('a -> bool list -> unit) option ->
    'a Tree.btree -> 'a t
  val huffman_btree : ?pf:('a -> bool list -> unit) option ->
    'a t -> 'a Tree.btree t
  val huffman : ?sofa:('a -> string) option ->
    'a t -> Channel.t -> ('a * int) list -> 'a t
(** [huffman ?sofa bw'a cha wl = bw''a]
  Computes an Huffman code's bool according to the weighted list [wl].
  Write it on Channel.t [cha] using the ['a] writter [bw'a]
  Returns a ['a] writter [bw''a] valid for all element appearing in [wl]
**)
  val huffman2 :
    ?sofa:('a -> string) option ->
    ?post:('a list -> 'a t) option ->
    'a t -> Channel.t -> ('a * int) list -> 'a t
(** [huffman2 ?sofa ?post bw'a cha wl = bw''a]
  same as [huffman], however, make a special case for elements of [wl] with a weight lower than [1]
**)
  val dag_core :
    (* find *) ('ident -> int option ) ->
    (* add  *) ('ident -> int -> unit) ->
    (* get_ident *) ('link -> 'ident) ->
    (* recfun *) ( Channel.t ->
      ('link -> int -> int) ->
       'link -> int -> int ) ->
    (* bw_ident *) (int -> int t) ->
    (* Channel.t *) Channel.t -> 'link -> int -> int
  val ctx_dag_core :
    (* find *) ('ctx -> 'ident -> int option ) ->
    (* add  *) ('ctx -> 'ident -> int -> unit) ->
    (* get_ident *) ('link -> 'ident) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link -> int -> int) ->
       'ctx -> 'link -> int -> int ) ->
    (* bw_ident *) ('ctx -> int -> int t) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link -> int -> int
  val hctx_dag_core :
    (* find *) ('ctx -> 'ident -> int option ) ->
    (* add  *) ('ctx -> 'ident -> int -> unit) ->
    (* get_ident *) ('link -> 'ident) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link -> unit ) ->
       'ctx -> 'link -> unit
      ) ->
    (* bw_ident *) ('ctx -> int -> int t) ->
    (* curr_nnode *) ('ctx -> int) ->
    (* incr_nnode *) ('ctx -> int) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link -> unit
  val hctx2_dag_core :
    (* find *) ('ctx -> 'ident -> int option ) ->
    (* add  *) ('ctx -> 'ident -> int -> unit) ->
    (* get_ident *) ('link -> 'ident) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link -> unit ) ->
       'ctx -> 'link -> unit
      ) ->
    (* bw_ident *) ('ctx -> int -> int option t) ->
    (* curr_nnode *) ('ctx -> int) ->
    (* incr_nnode *) ('ctx -> int) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link -> unit
  val hctx3_dag_core :
    (* find *) ('ctx -> 'ident -> (int, bool) result) ->
    (* add  *) ('ctx -> 'ident -> int -> unit) ->
    (* get_ident *) ('link -> 'ident) ->
    (* recfun *) ( Channel.t ->
      ('ctx -> 'link -> unit ) ->
       'ctx -> 'link -> unit
      ) ->
    (* bw_ident *) ('ctx -> int -> (int, bool) result t) ->
    (* curr_nnode *) ('ctx -> int) ->
    (* incr_nnode *) ('ctx -> int) ->
    (* Channel.t *) Channel.t -> 'ctx -> 'link -> unit
  val ofbstream : Channel.t -> OfBStream.Channel.t -> unit
end

type 'a bw = 'a ToBStream.t

val barray_of_br : 'a br -> BArray.t -> 'a
val barray_of_bw : 'a bw -> 'a -> BArray.t

type 't b3 = 't bw * 't br
val barray_of_b3 : 'a b3 -> ('a, BArray.t) O3.o3
