(*
  Mozilla Public License, v. 2.0.

  Copyright (c) 2020
    Joan Thibault
      - joan.thibault@irisa.fr
 *)

(** Simple Extension to OCaml's Standard Short Notations **)

let flip f x y = f y x
let ( <| ) x y = x y
let ( >> ) f g x = g ( f x )
let ( << ) g f x = g ( f x )
let ( ||> ) l f = List.rev_map f l |> List.rev
let ( <|| ) f l = List.rev_map f l |> List.rev
let ( |+ ) l f x = List.fold_left f x l
let ( ||>> ) f g x = List.rev_map g (f x) |> List.rev
let  (+=) x y = x:=!x+y
let  (-=) x y = x:=!x-y
let (+.=) x y = x:=!x+.y
let (-.=) x y = x:=!x-.y
let (!++) x = let x' = !x in x := succ x'; x'
let (!--) x = let x' = !x in x := pred x'; x'
(* [LATER] *)
let array_push a i x = a.(i) <- x :: a.(i)
