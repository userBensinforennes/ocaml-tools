open STools

type node = {
  index : int;
  weight : int;
  nodes : int list;
  edges : int list;
}

type graph = node option array

let string_of_int_list l : string =
  MyList.string_of_list string_of_int l

let string_of_int_list_list l : string =
  MyList.string_of_list string_of_int_list l

let print_int_list l : unit =
  print_string(string_of_int_list l)

let print_node node =
  print_string "{ index = "; print_int node.index;
  print_string "; weight = "; print_int node.weight;
  print_string "; nodes = "; print_int_list node.nodes;
  print_string "; edges = "; print_int_list node.edges;
  print_string " }"

let string_of_node node =
  ToS.("{ index="^(int node.index)^"; weight="^(int node.weight)^"; nodes="^(list int node.nodes)^"; edges="^(list int node.edges)^"}")

let print_graph graph =
  print_string "[|"; print_newline();
  Array.iter (function
    | None -> print_string "\tNone;\n";
    | Some node -> (
      print_string "\tSome ";
      print_node node;
      print_string ";";
      print_newline()
    )
  ) graph;
  print_string "|]"; print_newline()

let string_of_graph graph =
  ToS.array (ToS.option string_of_node) graph

let strict_neighbourgs (graph:graph) (node:node) : node list =
  Tools.map (fun x -> match graph.(x) with
    | None -> assert false
    | Some node -> node) node.edges
